<?php
// Heading
$_['heading_title']      = 'OpenCart';

// Text
$_['text_profile']       = 'Votre profil';
$_['text_store']         = 'Boutique';
$_['text_help']          = 'Aide';
$_['text_homepage']      = 'Page d&rsquo;accueil OpenCart';
$_['text_support']       = 'Forum d&rsquo;entraide';
$_['text_documentation'] = 'Documentation';
$_['text_logout']        = 'Déconnexion';
$_['text_order']             = 'Commandes';
$_['text_processing_status'] = 'En traitement';
$_['text_pending_status'] = 'En Attente';
$_['text_complete_status']   = 'Terminé';
$_['text_customer']          = 'Les clients';
$_['text_online']            = 'En ligne';
$_['text_approval']          = 'Approbation';
$_['text_product']           = 'Des produits';
$_['text_stock']             = 'En rupture de stock';
$_['text_review']            = 'Commentaires';
$_['text_return']            = 'Retour';
$_['text_affiliate']         = 'Affilié(e)s';
$_['text_front']             = 'Devanture de magasin';