-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 24 mai 2020 à 15:03
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `opencart`
--

-- --------------------------------------------------------

--
-- Structure de la table `oc_address`
--

DROP TABLE IF EXISTS `oc_address`;
CREATE TABLE IF NOT EXISTS `oc_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_address`
--

INSERT INTO `oc_address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`) VALUES
(2, 3, 'Miandrisoa Sandratra', 'RAKOTONARIVO', '', 'Amboditsiry', '', 'Antananarivo', '', 127, 1938, ''),
(6, 8, 'Saro', 'RAMANANTOANINA', '', 'Anosy', '', 'Antananarivo', '', 127, 1938, '');

-- --------------------------------------------------------

--
-- Structure de la table `oc_api`
--

DROP TABLE IF EXISTS `oc_api`;
CREATE TABLE IF NOT EXISTS `oc_api` (
  `api_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`api_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `username`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'U08tswFai7Fil8HelPY0kAacweranLN1DPF6ybrP0dTewg2XJiaCp62Bl4DTsD9qBQMB6R7icHmr1CJZzIHPZ78hThkAFi2jP23naHBCopU71T1aRP7As9lpOSEoVQqilBZS03GHjFk6aA0JRrUoLZ9rU2BJgd5G4wJA6USwFw6mj7N6G8Ouy5P2tKtreq7eqzU8TDTriqcz1QSjlXHtRxuNwKkLvBxKhdDGY2W8uKjLBtN6i2vxWt0A7R5p2og6', 1, '2018-03-13 11:18:03', '2018-03-13 11:18:03'),
(2, 'Default', 'V6vW2SuXbB4jDuJnCaiXjcGG3WnBLO40N1M4FGahF57vMcYFhaR6MmfxfGAS0M78lPl29zM0mspMlySiBzJ3y3qJJm6Go3aRQA2LW3d7KZ1bJpZNjtpyKS4CP8jLpliWxltfTPZFngwdOnZw0bfzf8XnIhI8CK8mYF959mbp1NUt70hwUsf6dMYmb2KIqkzMuosPrCsPYQao8T9IpCHEWlAqSHS6MT4dHEuSlsViPxYlNOjYzzndnWZIX4QpwNDH', 1, '2019-03-01 13:52:32', '2019-03-01 13:52:32'),
(3, 'Default', 'sBXtSfq1oTeiAT8DKhxOXLBqUqf99sm9WmFHSg6noy8mfSOA9uQZIZWnxHaRgPjBS7dmWGIYupk3YiwOGUhoxmxSMwSxLVDI5gHdN7ob3LjtFR2XWTOU16eq0IHdqIeUomcNG7YDCYuua2I3LfhkBgwYtePcj9BeMbtDPv9SowqGFjh0Nulr7owFKTFT6PnXlOvwnyvN1YmgDYBcmSS3PVLqw3n84yMOxh4Q4obdabNwJ8YYQS555RB9O6VIcvRr', 1, '2020-05-12 09:02:57', '2020-05-12 09:02:57');

-- --------------------------------------------------------

--
-- Structure de la table `oc_api_ip`
--

DROP TABLE IF EXISTS `oc_api_ip`;
CREATE TABLE IF NOT EXISTS `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  PRIMARY KEY (`api_ip_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_api_ip`
--

INSERT INTO `oc_api_ip` (`api_ip_id`, `api_id`, `ip`) VALUES
(1, 3, '::1');

-- --------------------------------------------------------

--
-- Structure de la table `oc_api_session`
--

DROP TABLE IF EXISTS `oc_api_session`;
CREATE TABLE IF NOT EXISTS `oc_api_session` (
  `api_session_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`api_session_id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_api_session`
--

INSERT INTO `oc_api_session` (`api_session_id`, `api_id`, `session_id`, `ip`, `date_added`, `date_modified`) VALUES
(55, 3, '350e0f1383f629b4f89887cb85', '::1', '2020-05-18 10:32:39', '2020-05-18 10:32:39'),
(54, 3, '3ab29628447b4f595ff8a47509', '::1', '2020-05-18 10:32:27', '2020-05-18 10:32:27'),
(53, 3, '7dfccdfefb607463012b381146', '::1', '2020-05-18 09:43:20', '2020-05-18 09:43:20'),
(52, 3, '5ecfcadfdb8d1804a0264d9a64', '::1', '2020-05-18 09:42:49', '2020-05-18 09:42:57'),
(51, 3, '56f568456323f45105a880f727', '::1', '2020-05-18 09:42:25', '2020-05-18 09:42:33'),
(50, 3, 'a90af826bb0832650c0856022c', '::1', '2020-05-18 09:42:05', '2020-05-18 09:42:10'),
(46, 3, '690359535dc7ff618973975211', '::1', '2020-05-18 09:08:19', '2020-05-18 09:08:19'),
(47, 3, 'b4653908684e6b5d31b25f40af', '::1', '2020-05-18 09:09:36', '2020-05-18 09:09:36'),
(48, 3, 'dafd7a6ee8240dedfc5a661bc7', '::1', '2020-05-18 09:41:22', '2020-05-18 09:41:22'),
(49, 3, 'e66899c3f77f710b74defdf510', '::1', '2020-05-18 09:41:36', '2020-05-18 09:41:48');

-- --------------------------------------------------------

--
-- Structure de la table `oc_attribute`
--

DROP TABLE IF EXISTS `oc_attribute`;
CREATE TABLE IF NOT EXISTS `oc_attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_attribute`
--

INSERT INTO `oc_attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 6, 3),
(4, 3, 1),
(5, 3, 2),
(6, 3, 3),
(7, 3, 4),
(8, 3, 5),
(9, 3, 6),
(10, 3, 7),
(11, 3, 8);

-- --------------------------------------------------------

--
-- Structure de la table `oc_attribute_description`
--

DROP TABLE IF EXISTS `oc_attribute_description`;
CREATE TABLE IF NOT EXISTS `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_attribute_description`
--

INSERT INTO `oc_attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(1, 1, 'Description'),
(2, 1, 'No. of Cores'),
(4, 1, 'test 1'),
(5, 1, 'test 2'),
(6, 1, 'test 3'),
(7, 1, 'test 4'),
(8, 1, 'test 5'),
(9, 1, 'test 6'),
(10, 1, 'test 7'),
(11, 1, 'test 8'),
(3, 1, 'Clockspeed'),
(1, 2, 'Description'),
(2, 2, 'No. of Cores'),
(4, 2, 'test 1'),
(5, 2, 'test 2'),
(6, 2, 'test 3'),
(7, 2, 'test 4'),
(8, 2, 'test 5'),
(9, 2, 'test 6'),
(10, 2, 'test 7'),
(11, 2, 'test 8'),
(3, 2, 'Clockspeed'),
(1, 3, 'Description'),
(2, 3, 'No. of Cores'),
(4, 3, 'test 1'),
(5, 3, 'test 2'),
(6, 3, 'test 3'),
(7, 3, 'test 4'),
(8, 3, 'test 5'),
(9, 3, 'test 6'),
(10, 3, 'test 7'),
(11, 3, 'test 8'),
(3, 3, 'Clockspeed');

-- --------------------------------------------------------

--
-- Structure de la table `oc_attribute_group`
--

DROP TABLE IF EXISTS `oc_attribute_group`;
CREATE TABLE IF NOT EXISTS `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_attribute_group`
--

INSERT INTO `oc_attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(3, 2),
(4, 1),
(5, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `oc_attribute_group_description`
--

DROP TABLE IF EXISTS `oc_attribute_group_description`;
CREATE TABLE IF NOT EXISTS `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`attribute_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_attribute_group_description`
--

INSERT INTO `oc_attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(3, 1, 'Memory'),
(4, 1, 'Technical'),
(5, 1, 'Motherboard'),
(6, 1, 'Processor'),
(3, 2, 'Memory'),
(4, 2, 'Technical'),
(5, 2, 'Motherboard'),
(6, 2, 'Processor'),
(3, 3, 'Memory'),
(4, 3, 'Technical'),
(5, 3, 'Motherboard'),
(6, 3, 'Processor');

-- --------------------------------------------------------

--
-- Structure de la table `oc_banner`
--

DROP TABLE IF EXISTS `oc_banner`;
CREATE TABLE IF NOT EXISTS `oc_banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_banner`
--

INSERT INTO `oc_banner` (`banner_id`, `name`, `status`) VALUES
(6, 'Promo', 1),
(7, 'Home Page Slideshow', 1),
(8, 'Manufacturers', 1),
(9, 'Information', 1),
(10, 'Flash', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_banner_image`
--

DROP TABLE IF EXISTS `oc_banner_image`;
CREATE TABLE IF NOT EXISTS `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_image_id`)
) ENGINE=MyISAM AUTO_INCREMENT=172 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`banner_image_id`, `banner_id`, `language_id`, `title`, `link`, `image`, `sort_order`) VALUES
(156, 6, 3, 'BIG', '', 'catalog/demo/product/images.jpg', 0),
(157, 6, 3, 'Dzama', '', 'catalog/demo/product/0ed6d0b931b496f8e97a013118a523d1.jpg', 0),
(94, 8, 1, 'NFL', '', 'catalog/demo/manufacturer/nfl.png', 0),
(95, 8, 1, 'RedBull', '', 'catalog/demo/manufacturer/redbull.png', 0),
(96, 8, 1, 'Sony', '', 'catalog/demo/manufacturer/sony.png', 0),
(91, 8, 1, 'Coca Cola', '', 'catalog/demo/manufacturer/cocacola.png', 0),
(92, 8, 1, 'Burger King', '', 'catalog/demo/manufacturer/burgerking.png', 0),
(93, 8, 1, 'Canon', '', 'catalog/demo/manufacturer/canon.png', 0),
(88, 8, 1, 'Harley Davidson', '', 'catalog/demo/manufacturer/harley.png', 0),
(89, 8, 1, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(90, 8, 1, 'Disney', '', 'catalog/demo/manufacturer/disney.png', 0),
(153, 6, 2, 'Dzama', '', 'catalog/demo/product/0ed6d0b931b496f8e97a013118a523d1.jpg', 0),
(97, 8, 1, 'Starbucks', '', 'catalog/demo/manufacturer/starbucks.png', 0),
(98, 8, 1, 'Nintendo', '', 'catalog/demo/manufacturer/nintendo.png', 0),
(133, 8, 3, 'NFL', '', 'catalog/demo/manufacturer/nfl.png', 0),
(134, 8, 3, 'RedBull', '', 'catalog/demo/manufacturer/redbull.png', 0),
(154, 6, 1, 'BIG', '', 'catalog/demo/product/images.jpg', 0),
(155, 6, 1, 'Dzama', '', 'catalog/demo/product/0ed6d0b931b496f8e97a013118a523d1.jpg', 0),
(101, 8, 2, 'NFL', '', 'catalog/demo/manufacturer/nfl.png', 0),
(102, 8, 2, 'RedBull', '', 'catalog/demo/manufacturer/redbull.png', 0),
(103, 8, 2, 'Sony', '', 'catalog/demo/manufacturer/sony.png', 0),
(104, 8, 2, 'Coca Cola', '', 'catalog/demo/manufacturer/cocacola.png', 0),
(105, 8, 2, 'Burger King', '', 'catalog/demo/manufacturer/burgerking.png', 0),
(106, 8, 2, 'Canon', '', 'catalog/demo/manufacturer/canon.png', 0),
(107, 8, 2, 'Harley Davidson', '', 'catalog/demo/manufacturer/harley.png', 0),
(108, 8, 2, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(109, 8, 2, 'Disney', '', 'catalog/demo/manufacturer/disney.png', 0),
(152, 6, 2, 'BIG', '', 'catalog/demo/product/images.jpg', 0),
(111, 8, 2, 'Starbucks', '', 'catalog/demo/manufacturer/starbucks.png', 0),
(112, 8, 2, 'Nintendo', '', 'catalog/demo/manufacturer/nintendo.png', 0),
(150, 7, 3, 'Habibo', '', 'catalog/demo/banners/pub produits habibo 2.jpg', 0),
(149, 7, 1, 'Dzama', '', 'catalog/demo/banners/5-rhums-dzama-zamalagasy-2018-danseuse-1.jpg', 0),
(148, 7, 1, 'Habibo', '', 'catalog/demo/banners/pub produits habibo 2.jpg', 0),
(147, 7, 2, 'Dzama', '#', 'catalog/demo/banners/5-rhums-dzama-zamalagasy-2018-danseuse-1.jpg', 0),
(135, 8, 3, 'Sony', '', 'catalog/demo/manufacturer/sony.png', 0),
(136, 8, 3, 'Coca Cola', '', 'catalog/demo/manufacturer/cocacola.png', 0),
(137, 8, 3, 'Burger King', '', 'catalog/demo/manufacturer/burgerking.png', 0),
(138, 8, 3, 'Canon', '', 'catalog/demo/manufacturer/canon.png', 0),
(139, 8, 3, 'Harley Davidson', '', 'catalog/demo/manufacturer/harley.png', 0),
(140, 8, 3, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(141, 8, 3, 'Disney', '', 'catalog/demo/manufacturer/disney.png', 0),
(142, 8, 3, 'Starbucks', '', 'catalog/demo/manufacturer/starbucks.png', 0),
(143, 8, 3, 'Nintendo', '', 'catalog/demo/manufacturer/nintendo.png', 0),
(146, 7, 2, 'Habibo', '', 'catalog/demo/banners/pub produits habibo 2.jpg', 0),
(151, 7, 3, 'Dzama', '', 'catalog/demo/banners/5-rhums-dzama-zamalagasy-2018-danseuse-1.jpg', 0),
(158, 9, 2, 'info1', 'https://www.who.int/fr/emergencies/diseases/novel-coronavirus-2019?gclid=Cj0KCQjw2PP1BRCiARIsAEqv-pS7WjXFbl3cBr6G7xYEEzxUKeA_zZ8kUWRQfp-RBxhU_cpCEahjGcUaAt6mEALw_wcB', 'catalog/demo/banners/97325407_1379390582448613_7695356521298788352_n.png', 0),
(159, 9, 2, 'info', 'https://www.who.int/fr/emergencies/diseases/novel-coronavirus-2019?gclid=Cj0KCQjw2PP1BRCiARIsAEqv-pS7WjXFbl3cBr6G7xYEEzxUKeA_zZ8kUWRQfp-RBxhU_cpCEahjGcUaAt6mEALw_wcB', 'catalog/demo/banners/97510757_948965485538978_5798978484151779328_n.png', 0),
(160, 9, 1, 'info1', 'https://www.who.int/fr/emergencies/diseases/novel-coronavirus-2019?gclid=Cj0KCQjw2PP1BRCiARIsAEqv-pS7WjXFbl3cBr6G7xYEEzxUKeA_zZ8kUWRQfp-RBxhU_cpCEahjGcUaAt6mEALw_wcB', 'catalog/demo/banners/97325407_1379390582448613_7695356521298788352_n.png', 0),
(161, 9, 1, 'info2', 'https://www.who.int/fr/emergencies/diseases/novel-coronavirus-2019?gclid=Cj0KCQjw2PP1BRCiARIsAEqv-pS7WjXFbl3cBr6G7xYEEzxUKeA_zZ8kUWRQfp-RBxhU_cpCEahjGcUaAt6mEALw_wcB', 'catalog/demo/banners/97510757_948965485538978_5798978484151779328_n.png', 0),
(162, 9, 3, 'info2', 'https://www.who.int/fr/emergencies/diseases/novel-coronavirus-2019?gclid=Cj0KCQjw2PP1BRCiARIsAEqv-pS7WjXFbl3cBr6G7xYEEzxUKeA_zZ8kUWRQfp-RBxhU_cpCEahjGcUaAt6mEALw_wcB', 'catalog/demo/banners/97325407_1379390582448613_7695356521298788352_n.png', 0),
(163, 9, 3, 'info1', 'https://www.who.int/fr/emergencies/diseases/novel-coronavirus-2019?gclid=Cj0KCQjw2PP1BRCiARIsAEqv-pS7WjXFbl3cBr6G7xYEEzxUKeA_zZ8kUWRQfp-RBxhU_cpCEahjGcUaAt6mEALw_wcB', 'catalog/demo/banners/97510757_948965485538978_5798978484151779328_n.png', 0),
(166, 10, 2, 'vente', 'http://localhost/tsenabe/upload/index.php?route=product/product&amp;product_id=73', 'catalog/demo/product/big/pub big cola.jpg', 0),
(167, 10, 2, 'vente2', 'http://localhost/tsenabe/upload/index.php?route=product/product&amp;product_id=72', 'catalog/demo/product/big/pub big orange.jpg', 0),
(168, 10, 1, 'vente', 'http://localhost/tsenabe/upload/index.php?route=product/product&amp;product_id=73', 'catalog/demo/product/big/pub big cola.jpg', 0),
(169, 10, 1, 'vente1', 'http://localhost/tsenabe/upload/index.php?route=product/product&amp;product_id=72', 'catalog/demo/product/big/pub big orange.jpg', 0),
(170, 10, 3, 'vente', 'http://localhost/tsenabe/upload/index.php?route=product/product&amp;product_id=73', 'catalog/demo/product/big/big cola 2l.jpg', 0),
(171, 10, 3, 'vente1', 'http://localhost/tsenabe/upload/index.php?route=product/product&amp;product_id=72', 'catalog/demo/product/big/pub big orange.jpg', 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_cart`
--

DROP TABLE IF EXISTS `oc_cart`;
CREATE TABLE IF NOT EXISTS `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_cart`
--

INSERT INTO `oc_cart` (`cart_id`, `api_id`, `customer_id`, `session_id`, `product_id`, `recurring_id`, `option`, `quantity`, `date_added`) VALUES
(1, 0, 1, '109069ab9463456efb8e9df07f', 34, 0, '[]', 3, '2018-03-28 15:18:04'),
(8, 0, 2, '8fa54662c262c8b069cdf91dc2', 36, 0, '[]', 1, '2019-04-08 17:17:04'),
(9, 0, 2, '8fa54662c262c8b069cdf91dc2', 41, 0, '[]', 1, '2019-04-08 17:17:04'),
(10, 0, 2, '8fa54662c262c8b069cdf91dc2', 43, 0, '[]', 1, '2019-04-08 17:17:04'),
(11, 0, 2, '8fa54662c262c8b069cdf91dc2', 32, 0, '[]', 1, '2019-04-08 17:17:04'),
(12, 0, 2, '8fa54662c262c8b069cdf91dc2', 28, 0, '[]', 1, '2019-04-08 17:17:04'),
(15, 0, 7, 'ac3d71fc8dfc3cf2a9404adc82', 79, 0, '[]', 1, '2020-05-16 10:47:28'),
(18, 0, 3, 'd72b0753ceba0297fc88a58edf', 73, 0, '[]', 1, '2020-05-18 09:39:02');

-- --------------------------------------------------------

--
-- Structure de la table `oc_category`
--

DROP TABLE IF EXISTS `oc_category`;
CREATE TABLE IF NOT EXISTS `oc_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(25, 'catalog/themevolty/categorybanner/Category-Banner.jpg', 18, 1, 1, 0, 1, '2009-01-31 01:04:25', '2020-05-13 09:19:53'),
(20, 'catalog/demo/banners/pub big 3.jpg', 0, 1, 1, 1, 1, '2009-01-05 21:49:43', '2020-05-13 20:45:22'),
(18, 'catalog/demo/banners/pub produit habibo.jpg', 0, 1, 0, 2, 1, '2009-01-05 21:49:15', '2020-05-13 20:46:11'),
(45, 'catalog/themevolty/categorybanner/Category-Banner.jpg', 18, 0, 0, 0, 1, '2010-09-24 18:29:16', '2020-05-13 08:49:58'),
(46, 'catalog/themevolty/categorybanner/Category-Banner.jpg', 18, 0, 0, 0, 1, '2010-09-24 18:29:31', '2020-05-13 08:48:59'),
(59, 'catalog/demo/product/0ed6d0b931b496f8e97a013118a523d1.jpg', 20, 0, 1, 0, 1, '2019-04-10 09:46:39', '2020-05-13 08:41:41'),
(72, 'catalog/demo/product/images.jpg', 20, 0, 1, 0, 1, '2019-04-10 09:55:49', '2020-05-13 08:44:45'),
(78, 'catalog/themevolty/categorybanner/Category-Banner.jpg', 18, 0, 1, 0, 1, '2019-04-10 09:58:09', '2020-05-13 08:56:17'),
(88, '', 18, 0, 1, 0, 1, '2020-05-13 09:19:07', '2020-05-13 09:19:07'),
(89, '', 18, 0, 1, 0, 1, '2020-05-13 09:21:00', '2020-05-13 09:21:00'),
(90, '', 18, 0, 1, 0, 1, '2020-05-13 09:21:40', '2020-05-13 09:21:40');

-- --------------------------------------------------------

--
-- Structure de la table `oc_category_description`
--

DROP TABLE IF EXISTS `oc_category_description`;
CREATE TABLE IF NOT EXISTS `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(25, 2, 'Sel', '', 'Sel', '', ''),
(20, 1, 'Boisson', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;\r\n', 'Boisson', '', ''),
(20, 3, 'Boisson', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;\r\n', 'Boisson', '', ''),
(18, 1, 'PPN', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;\r\n', 'PPN', '', ''),
(18, 3, 'PPN', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;\r\n', 'PPN', '', ''),
(45, 2, 'café', '', 'café', '', ''),
(46, 2, 'Pâte instantanée', '', 'Pâte instantanée', '', ''),
(25, 1, 'Sel', '', 'Sel', '', ''),
(89, 2, 'Sucre', '', 'Sucre', '', ''),
(89, 1, 'Sugar', '', 'Sugar', '', ''),
(90, 2, 'Epices', '', 'Epices', '', ''),
(90, 1, 'Epices', '', 'Epices', '', ''),
(20, 2, 'Boisson', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;\r\n', 'Boisson', '', ''),
(18, 2, 'PPN', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;\r\n', 'PPN', '', ''),
(25, 3, 'Sel', '', 'Sel', '', ''),
(89, 3, 'Sugar', '', 'Sugar', '', ''),
(90, 3, 'Epices', '', 'Epices', '', ''),
(45, 3, 'café', '', 'café', '', ''),
(46, 3, 'Pâte instantanée', '', 'Pâte instantanée', '', ''),
(59, 3, 'Boisson alcoolisé', '', 'Boisson alcoolisé', '', ''),
(72, 3, 'Boisson non-alcoolisés', '', 'Boisson non-alcoolisés', '', ''),
(78, 3, 'Savonnerie', '', 'Savonnerie', '', ''),
(88, 3, 'Vinaigre', '', 'Vinaigre', '', ''),
(45, 1, 'café', '', 'café', '', ''),
(46, 1, 'Pâte instantanée', '', 'Pâte instantanée', '', ''),
(59, 1, 'Boisson alcoolisé', '', 'Boisson alcoolisé', '', ''),
(59, 2, 'Boisson alcoolisé', '', 'Boisson alcoolisé', '', ''),
(72, 1, 'Boisson non-alcoolisés', '', 'Boisson non-alcoolisés', '', ''),
(72, 2, 'Boisson non-alcoolisés', '', 'Boisson non-alcoolisés', '', ''),
(78, 1, 'Savonnerie', '', 'Savonnerie', '', ''),
(78, 2, 'Savonnerie', '', 'Savonnerie', '', ''),
(88, 2, 'Vinaigre', '', 'Vinaigre', '', ''),
(88, 1, 'Vinaigre', '', 'Vinaigre', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `oc_category_filter`
--

DROP TABLE IF EXISTS `oc_category_filter`;
CREATE TABLE IF NOT EXISTS `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_category_path`
--

DROP TABLE IF EXISTS `oc_category_path`;
CREATE TABLE IF NOT EXISTS `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`path_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(25, 25, 1),
(20, 20, 0),
(18, 18, 0),
(45, 18, 0),
(45, 45, 1),
(46, 18, 0),
(46, 46, 1),
(59, 20, 0),
(59, 59, 1),
(25, 18, 0),
(72, 20, 0),
(72, 72, 1),
(78, 18, 0),
(78, 78, 1),
(90, 90, 1),
(90, 18, 0),
(89, 89, 1),
(89, 18, 0),
(88, 88, 1),
(88, 18, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_category_to_layout`
--

DROP TABLE IF EXISTS `oc_category_to_layout`;
CREATE TABLE IF NOT EXISTS `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(20, 0, 0),
(18, 0, 0),
(46, 0, 0),
(45, 0, 0),
(25, 0, 0),
(59, 0, 0),
(72, 0, 0),
(78, 0, 0),
(90, 0, 0),
(89, 0, 0),
(88, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_category_to_store`
--

DROP TABLE IF EXISTS `oc_category_to_store`;
CREATE TABLE IF NOT EXISTS `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(18, 0),
(20, 0),
(25, 0),
(45, 0),
(46, 0),
(59, 0),
(72, 0),
(78, 0),
(88, 0),
(89, 0),
(90, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_country`
--

DROP TABLE IF EXISTS `oc_country`;
CREATE TABLE IF NOT EXISTS `oc_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=258 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_coupon`
--

DROP TABLE IF EXISTS `oc_coupon`;
CREATE TABLE IF NOT EXISTS `oc_coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`coupon_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Structure de la table `oc_coupon_category`
--

DROP TABLE IF EXISTS `oc_coupon_category`;
CREATE TABLE IF NOT EXISTS `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_coupon_history`
--

DROP TABLE IF EXISTS `oc_coupon_history`;
CREATE TABLE IF NOT EXISTS `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`coupon_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_coupon_product`
--

DROP TABLE IF EXISTS `oc_coupon_product`;
CREATE TABLE IF NOT EXISTS `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_currency`
--

DROP TABLE IF EXISTS `oc_currency`;
CREATE TABLE IF NOT EXISTS `oc_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Ariary', 'MGA', '', 'Ar', '2', 1.00000000, 1, '2020-05-24 13:58:25');

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer`
--

DROP TABLE IF EXISTS `oc_customer`;
CREATE TABLE IF NOT EXISTS `oc_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_customer`
--

INSERT INTO `oc_customer` (`customer_id`, `customer_group_id`, `store_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `safe`, `token`, `code`, `date_added`) VALUES
(3, 1, 0, 1, 'Miandrisoa Sandratra', 'RAKOTONARIVO', 'syraxrakotonarivo@gmail.com', '0332848333', '', '166deee53cae68c77b89335e32ebdf507b81cab1', 'xe9p536sH', NULL, NULL, 0, 2, '', '::1', 1, 0, '', '', '2020-05-12 11:11:32'),
(8, 1, 0, 2, 'Saro', 'RAMANANTOANINA', 'rakotonarivosandratr@yahoo.fr', '0332848333', '', '8f2971a0351e9445af416926eba9a2f0095d2c5b', 'WdgW4HwvJ', NULL, NULL, 0, 6, '', '::1', 1, 0, '', '', '2020-05-18 09:40:27');

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_activity`
--

DROP TABLE IF EXISTS `oc_customer_activity`;
CREATE TABLE IF NOT EXISTS `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_affiliate`
--

DROP TABLE IF EXISTS `oc_customer_affiliate`;
CREATE TABLE IF NOT EXISTS `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_approval`
--

DROP TABLE IF EXISTS `oc_customer_approval`;
CREATE TABLE IF NOT EXISTS `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_approval_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_group`
--

DROP TABLE IF EXISTS `oc_customer_group`;
CREATE TABLE IF NOT EXISTS `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`customer_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_group_description`
--

DROP TABLE IF EXISTS `oc_customer_group_description`;
CREATE TABLE IF NOT EXISTS `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`customer_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test'),
(1, 2, 'Default', 'test'),
(1, 3, 'Default', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_history`
--

DROP TABLE IF EXISTS `oc_customer_history`;
CREATE TABLE IF NOT EXISTS `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_ip`
--

DROP TABLE IF EXISTS `oc_customer_ip`;
CREATE TABLE IF NOT EXISTS `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_ip_id`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_customer_ip`
--

INSERT INTO `oc_customer_ip` (`customer_ip_id`, `customer_id`, `ip`, `date_added`) VALUES
(4, 3, '::1', '2020-05-12 11:11:33'),
(9, 8, '::1', '2020-05-18 09:40:42');

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_login`
--

DROP TABLE IF EXISTS `oc_customer_login`;
CREATE TABLE IF NOT EXISTS `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`customer_login_id`),
  KEY `email` (`email`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_customer_login`
--

INSERT INTO `oc_customer_login` (`customer_login_id`, `email`, `ip`, `total`, `date_added`, `date_modified`) VALUES
(2, '', '192.168.1.20', 1, '2019-04-10 06:47:31', '2019-04-10 06:47:31');

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_online`
--

DROP TABLE IF EXISTS `oc_customer_online`;
CREATE TABLE IF NOT EXISTS `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_reward`
--

DROP TABLE IF EXISTS `oc_customer_reward`;
CREATE TABLE IF NOT EXISTS `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_reward_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_search`
--

DROP TABLE IF EXISTS `oc_customer_search`;
CREATE TABLE IF NOT EXISTS `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_transaction`
--

DROP TABLE IF EXISTS `oc_customer_transaction`;
CREATE TABLE IF NOT EXISTS `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_transaction_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_customer_transaction`
--

INSERT INTO `oc_customer_transaction` (`customer_transaction_id`, `customer_id`, `order_id`, `description`, `amount`, `date_added`) VALUES
(1, 3, 0, '', '0.0000', '2020-05-12 14:28:06');

-- --------------------------------------------------------

--
-- Structure de la table `oc_customer_wishlist`
--

DROP TABLE IF EXISTS `oc_customer_wishlist`;
CREATE TABLE IF NOT EXISTS `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_custom_field`
--

DROP TABLE IF EXISTS `oc_custom_field`;
CREATE TABLE IF NOT EXISTS `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`custom_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_custom_field_customer_group`
--

DROP TABLE IF EXISTS `oc_custom_field_customer_group`;
CREATE TABLE IF NOT EXISTS `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`custom_field_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_custom_field_description`
--

DROP TABLE IF EXISTS `oc_custom_field_description`;
CREATE TABLE IF NOT EXISTS `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`custom_field_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_custom_field_value`
--

DROP TABLE IF EXISTS `oc_custom_field_value`;
CREATE TABLE IF NOT EXISTS `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`custom_field_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_custom_field_value_description`
--

DROP TABLE IF EXISTS `oc_custom_field_value_description`;
CREATE TABLE IF NOT EXISTS `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`custom_field_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_download`
--

DROP TABLE IF EXISTS `oc_download`;
CREATE TABLE IF NOT EXISTS `oc_download` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_download_description`
--

DROP TABLE IF EXISTS `oc_download_description`;
CREATE TABLE IF NOT EXISTS `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`download_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_event`
--

DROP TABLE IF EXISTS `oc_event`;
CREATE TABLE IF NOT EXISTS `oc_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_extension`
--

DROP TABLE IF EXISTS `oc_extension`;
CREATE TABLE IF NOT EXISTS `oc_extension` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL,
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(94, 'module', 'tvcmsspecialproduct'),
(146, 'module', 'responsive_slideshow'),
(99, 'module', 'tvcmscategoryslider'),
(112, 'module', 'tvcmsleftproduct'),
(105, 'module', 'tvcmsbrandlist'),
(90, 'module', 'tvcmstabproducts'),
(83, 'module', 'latest'),
(107, 'module', 'tvcmsfooterlogo'),
(110, 'module', 'tvcmsnewsletterlist'),
(111, 'module', 'tvcmspaymenticon'),
(119, 'module', 'tvcmscommentlist'),
(148, 'module', 'tvcmscustomsetting'),
(121, 'module', 'tvcmssocialicon'),
(147, 'module', 'special'),
(133, 'module', 'information'),
(136, 'module', 'tvcmscategoryproduct'),
(155, 'module', 'revechat');

-- --------------------------------------------------------

--
-- Structure de la table `oc_extension_install`
--

DROP TABLE IF EXISTS `oc_extension_install`;
CREATE TABLE IF NOT EXISTS `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL AUTO_INCREMENT,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`extension_install_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_extension_install`
--

INSERT INTO `oc_extension_install` (`extension_install_id`, `extension_download_id`, `filename`, `date_added`) VALUES
(1, 0, 'tvcmsthemevolty.ocmod.zip', '2018-09-26 10:14:21'),
(2, 0, 'twigdebug.ocmod.zip', '2020-05-19 15:29:27');

-- --------------------------------------------------------

--
-- Structure de la table `oc_extension_path`
--

DROP TABLE IF EXISTS `oc_extension_path`;
CREATE TABLE IF NOT EXISTS `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL AUTO_INCREMENT,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`extension_path_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_filter`
--

DROP TABLE IF EXISTS `oc_filter`;
CREATE TABLE IF NOT EXISTS `oc_filter` (
  `filter_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`filter_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_filter`
--

INSERT INTO `oc_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(4, 1, 0),
(3, 1, 0),
(2, 1, 0),
(1, 1, 0),
(5, 2, 0),
(6, 2, 0),
(7, 2, 0),
(8, 2, 0),
(9, 2, 0),
(10, 3, 0),
(11, 3, 0),
(12, 3, 0),
(13, 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_filter_description`
--

DROP TABLE IF EXISTS `oc_filter_description`;
CREATE TABLE IF NOT EXISTS `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_filter_description`
--

INSERT INTO `oc_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(4, 2, 1, 'Accessories'),
(3, 3, 1, 'Dresses'),
(3, 1, 1, 'Dresses'),
(3, 2, 1, 'Dresses'),
(2, 3, 1, 'Handbags'),
(2, 1, 1, 'Handbags'),
(2, 2, 1, 'Handbags'),
(5, 1, 2, 'Black'),
(5, 2, 2, 'Black'),
(6, 1, 2, 'Blue'),
(6, 2, 2, 'Blue'),
(7, 1, 2, 'Green'),
(7, 2, 2, 'Green'),
(8, 1, 2, 'White'),
(8, 2, 2, 'White'),
(9, 1, 2, 'Red'),
(9, 2, 2, 'Red'),
(10, 1, 3, 'Versace'),
(10, 2, 3, 'Versace'),
(11, 1, 3, 'Tommy Hilfiger'),
(11, 2, 3, 'Tommy Hilfiger'),
(12, 1, 3, 'Diesel'),
(12, 2, 3, 'Diesel'),
(13, 1, 3, 'Calvin Klein'),
(13, 2, 3, 'Calvin Klein'),
(1, 3, 1, 'Jackets?'),
(1, 1, 1, 'Jackets?'),
(1, 2, 1, 'Jackets?'),
(5, 3, 2, 'Black'),
(6, 3, 2, 'Blue'),
(7, 3, 2, 'Green'),
(8, 3, 2, 'White'),
(9, 3, 2, 'Red'),
(10, 3, 3, 'Versace'),
(11, 3, 3, 'Tommy Hilfiger'),
(12, 3, 3, 'Diesel'),
(13, 3, 3, 'Calvin Klein'),
(4, 1, 1, 'Accessories'),
(4, 3, 1, 'Accessories');

-- --------------------------------------------------------

--
-- Structure de la table `oc_filter_group`
--

DROP TABLE IF EXISTS `oc_filter_group`;
CREATE TABLE IF NOT EXISTS `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`filter_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_filter_group`
--

INSERT INTO `oc_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 0),
(2, 0),
(3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_filter_group_description`
--

DROP TABLE IF EXISTS `oc_filter_group_description`;
CREATE TABLE IF NOT EXISTS `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_filter_group_description`
--

INSERT INTO `oc_filter_group_description` (`filter_group_id`, `language_id`, `name`) VALUES
(1, 3, 'Katalaogy'),
(1, 1, 'Category'),
(2, 1, 'Color'),
(2, 2, 'Color'),
(3, 1, 'Manufacturer'),
(3, 2, 'Manufacturer'),
(1, 2, 'Categorie'),
(2, 3, 'Color'),
(3, 3, 'Manufacturer');

-- --------------------------------------------------------

--
-- Structure de la table `oc_geo_zone`
--

DROP TABLE IF EXISTS `oc_geo_zone`;
CREATE TABLE IF NOT EXISTS `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Structure de la table `oc_googleshopping_category`
--

DROP TABLE IF EXISTS `oc_googleshopping_category`;
CREATE TABLE IF NOT EXISTS `oc_googleshopping_category` (
  `google_product_category` varchar(10) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`google_product_category`,`store_id`),
  KEY `category_id_store_id` (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_googleshopping_product`
--

DROP TABLE IF EXISTS `oc_googleshopping_product`;
CREATE TABLE IF NOT EXISTS `oc_googleshopping_product` (
  `product_advertise_google_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `has_issues` tinyint(1) DEFAULT NULL,
  `destination_status` enum('pending','approved','disapproved') NOT NULL DEFAULT 'pending',
  `impressions` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `conversions` int(11) NOT NULL DEFAULT '0',
  `cost` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `conversion_value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `google_product_category` varchar(10) DEFAULT NULL,
  `condition` enum('new','refurbished','used') DEFAULT NULL,
  `adult` tinyint(1) DEFAULT NULL,
  `multipack` int(11) DEFAULT NULL,
  `is_bundle` tinyint(1) DEFAULT NULL,
  `age_group` enum('newborn','infant','toddler','kids','adult') DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `gender` enum('male','female','unisex') DEFAULT NULL,
  `size_type` enum('regular','petite','plus','big and tall','maternity') DEFAULT NULL,
  `size_system` enum('AU','BR','CN','DE','EU','FR','IT','JP','MEX','UK','US') DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `is_modified` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_advertise_google_id`),
  UNIQUE KEY `product_id_store_id` (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_googleshopping_product_status`
--

DROP TABLE IF EXISTS `oc_googleshopping_product_status`;
CREATE TABLE IF NOT EXISTS `oc_googleshopping_product_status` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL DEFAULT '0',
  `product_variation_id` varchar(64) NOT NULL DEFAULT '',
  `destination_statuses` text NOT NULL,
  `data_quality_issues` text NOT NULL,
  `item_level_issues` text NOT NULL,
  `google_expiration_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`store_id`,`product_variation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_googleshopping_product_target`
--

DROP TABLE IF EXISTS `oc_googleshopping_product_target`;
CREATE TABLE IF NOT EXISTS `oc_googleshopping_product_target` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `advertise_google_target_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`,`advertise_google_target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_googleshopping_target`
--

DROP TABLE IF EXISTS `oc_googleshopping_target`;
CREATE TABLE IF NOT EXISTS `oc_googleshopping_target` (
  `advertise_google_target_id` int(11) UNSIGNED NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `campaign_name` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(2) NOT NULL DEFAULT '',
  `budget` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `feeds` text NOT NULL,
  `status` enum('paused','active') NOT NULL DEFAULT 'paused',
  `date_added` date DEFAULT NULL,
  `roas` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`advertise_google_target_id`),
  KEY `store_id` (`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_information`
--

DROP TABLE IF EXISTS `oc_information`;
CREATE TABLE IF NOT EXISTS `oc_information` (
  `information_id` int(11) NOT NULL AUTO_INCREMENT,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`information_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_information_description`
--

DROP TABLE IF EXISTS `oc_information_description`;
CREATE TABLE IF NOT EXISTS `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`information_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(4, 2, 'À propos de nous', '&lt;p&gt;\r\n About Us&lt;/p&gt;\r\n', 'À propos de nous', '', ''),
(5, 2, 'Termes &amp; Conditions', '&lt;p&gt;\r\n Terms &amp;amp; Conditions&lt;/p&gt;\r\n', 'Termes &amp; Conditions', '', ''),
(3, 2, 'Politique de confidentialité', '&lt;p&gt;\r\n Privacy Policy&lt;/p&gt;\r\n', 'Politique de confidentialité', '', ''),
(6, 2, 'Information de livraison', '&lt;p&gt;\r\n Delivery Information&lt;/p&gt;\r\n', 'Information de livraison', '', ''),
(4, 1, 'About Us', '&lt;p&gt;\r\n About Us&lt;/p&gt;\r\n', 'About Us', '', ''),
(5, 1, 'Terms &amp; Conditions', '&lt;p&gt;\r\n Terms &amp;amp; Conditions&lt;/p&gt;\r\n', 'Terms &amp; Conditions', '', ''),
(3, 1, 'Privacy Policy', '&lt;p&gt;\r\n Privacy Policy&lt;/p&gt;\r\n', 'Privacy Policy', '', ''),
(6, 1, 'Delivery Information', '&lt;p&gt;\r\n Delivery Information&lt;/p&gt;\r\n', 'Delivery Information', '', ''),
(4, 3, 'Momba Anay', '&lt;p&gt;\r\n About Us&lt;/p&gt;\r\n', 'Momba Anay', '', ''),
(5, 3, 'Fomba sy ny Fepetra', '&lt;p&gt;\r\n Terms &amp;amp; Conditions&lt;/p&gt;\r\n', 'Fomba sy ny Fepetra', '', ''),
(3, 3, 'Politika fiarovana fiainan\'olona', '&lt;p&gt;\r\n Privacy Policy&lt;/p&gt;\r\n', 'Politika fiarovana fiainan\'olona', '', ''),
(6, 3, 'Fampahalalana momban\'ny fandefasana', '&lt;p&gt;\r\n Delivery Information&lt;/p&gt;\r\n', 'Fampahalalana momban\'ny fandefasana', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `oc_information_to_layout`
--

DROP TABLE IF EXISTS `oc_information_to_layout`;
CREATE TABLE IF NOT EXISTS `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(4, 0, 0),
(4, 1, 0),
(4, 2, 0),
(4, 3, 0),
(6, 0, 0),
(6, 1, 0),
(6, 2, 0),
(6, 3, 0),
(3, 0, 0),
(3, 1, 0),
(3, 2, 0),
(3, 3, 0),
(5, 0, 0),
(5, 1, 0),
(5, 2, 0),
(5, 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_information_to_store`
--

DROP TABLE IF EXISTS `oc_information_to_store`;
CREATE TABLE IF NOT EXISTS `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_language`
--

DROP TABLE IF EXISTS `oc_language`;
CREATE TABLE IF NOT EXISTS `oc_language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 1, 1),
(2, 'Français', 'fr-FR', 'fr,fr-FR,fr_FR.UTF-8,french', '', '', 0, 1),
(3, 'Malagasy', 'mg-MG', 'mg,mg-MG,mg_MG.UTF-8,Malagasy', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_layout`
--

DROP TABLE IF EXISTS `oc_layout`;
CREATE TABLE IF NOT EXISTS `oc_layout` (
  `layout_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search');

-- --------------------------------------------------------

--
-- Structure de la table `oc_layout_module`
--

DROP TABLE IF EXISTS `oc_layout_module`;
CREATE TABLE IF NOT EXISTS `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`layout_module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1038 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(138, 5, 'category', 'column_left', 0),
(636, 6, 'information', 'column_left', 1),
(132, 7, 'category', 'column_left', 0),
(842, 3, 'account', 'column_left', 4),
(635, 6, 'account', 'column_left', 0),
(638, 10, 'information', 'column_left', 1),
(637, 10, 'account', 'column_left', 0),
(133, 7, 'information', 'column_left', 1),
(134, 8, 'account', 'column_left', 0),
(135, 11, 'category', 'column_left', 0),
(136, 11, 'account', 'column_left', 1),
(137, 11, 'information', 'column_left', 2),
(139, 5, 'account', 'column_left', 1),
(141, 13, 'category', 'column_left', 0),
(142, 13, 'information', 'column_left', 1),
(143, 9, 'account', 'column_left', 0),
(144, 9, 'information', 'column_left', 1),
(959, 2, 'responsive_slideshow.117', 'column_right', 1),
(1036, 1, 'responsive_slideshow.123', 'column_right', 1),
(841, 3, 'category', 'column_left', 0),
(1033, 1, 'tvcmstabproducts.65', 'content_top', 2),
(1034, 1, 'tvcmsbrandlist.80', 'content_bottom', 2),
(1031, 1, 'tvcmscategoryslider.74', 'content_top', 0),
(958, 2, 'responsive_slideshow.122', 'column_right', 0),
(1035, 1, 'responsive_slideshow.122', 'column_right', 0),
(1032, 1, 'responsive_slideshow.116', 'content_top', 1),
(1030, 1, 'responsive_slideshow.117', 'column_left', 1),
(1029, 1, 'tvcmsleftproduct.86', 'column_left', 0),
(1037, 1, 'revechat', 'content_top', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_layout_route`
--

DROP TABLE IF EXISTS `oc_layout_route`;
CREATE TABLE IF NOT EXISTS `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  PRIMARY KEY (`layout_route_id`)
) ENGINE=MyISAM AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(121, 6, 0, 'account/%'),
(122, 10, 0, 'affiliate/%'),
(133, 3, 0, 'product/category'),
(158, 1, 0, 'common/home'),
(149, 2, 0, 'product/product'),
(94, 11, 0, 'information/information'),
(93, 7, 0, 'checkout/%'),
(84, 8, 0, 'information/contact'),
(96, 9, 0, 'information/sitemap'),
(85, 4, 0, ''),
(87, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(95, 13, 0, 'product/search');

-- --------------------------------------------------------

--
-- Structure de la table `oc_length_class`
--

DROP TABLE IF EXISTS `oc_length_class`;
CREATE TABLE IF NOT EXISTS `oc_length_class` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL,
  PRIMARY KEY (`length_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Structure de la table `oc_length_class_description`
--

DROP TABLE IF EXISTS `oc_length_class_description`;
CREATE TABLE IF NOT EXISTS `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  PRIMARY KEY (`length_class_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in'),
(1, 2, 'Centimeter', 'cm'),
(2, 2, 'Millimeter', 'mm'),
(3, 2, 'Inch', 'in'),
(1, 3, 'Centimeter', 'cm'),
(2, 3, 'Millimeter', 'mm'),
(3, 3, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Structure de la table `oc_location`
--

DROP TABLE IF EXISTS `oc_location`;
CREATE TABLE IF NOT EXISTS `oc_location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_manufacturer`
--

DROP TABLE IF EXISTS `oc_manufacturer`;
CREATE TABLE IF NOT EXISTS `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_manufacturer`
--

INSERT INTO `oc_manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(5, 'HTC', 'catalog/demo/htc_logo.jpg', 0),
(11, 'habibo', '', 0),
(12, 'Mado', '', 0),
(13, 'Savonnerie tropicale', '', 0),
(8, 'Dzama', 'catalog/demo/profile_photo.jpg', 0),
(9, 'Salone', '', 0),
(14, 'Savour', '', 0),
(15, 'Big', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_manufacturer_to_store`
--

DROP TABLE IF EXISTS `oc_manufacturer_to_store`;
CREATE TABLE IF NOT EXISTS `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`manufacturer_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_manufacturer_to_store`
--

INSERT INTO `oc_manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(5, 0),
(8, 0),
(9, 0),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_marketing`
--

DROP TABLE IF EXISTS `oc_marketing`;
CREATE TABLE IF NOT EXISTS `oc_marketing` (
  `marketing_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`marketing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_mega_menu`
--

DROP TABLE IF EXISTS `oc_mega_menu`;
CREATE TABLE IF NOT EXISTS `oc_mega_menu` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL,
  `rang` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `name` text,
  `link` text,
  `description` text,
  `label` text,
  `label_text_color` text,
  `label_background_color` text,
  `new_window` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `display_on_mobile` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `custom_class` text,
  `submenu_width` text,
  `submenu_type` int(11) NOT NULL DEFAULT '0',
  `submenu_background` text,
  `submenu_background_position` text,
  `submenu_background_repeat` text,
  `content_width` int(11) NOT NULL DEFAULT '12',
  `content_type` int(11) NOT NULL DEFAULT '0',
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_mega_menu`
--

INSERT INTO `oc_mega_menu` (`id`, `module_id`, `parent_id`, `rang`, `icon`, `name`, `link`, `description`, `label`, `label_text_color`, `label_background_color`, `new_window`, `status`, `display_on_mobile`, `position`, `custom_class`, `submenu_width`, `submenu_type`, `submenu_background`, `submenu_background_position`, `submenu_background_repeat`, `content_width`, `content_type`, `content`) VALUES
(1, 0, 0, 0, '', 'a:1:{i:1;s:4:\"Home\";}', '', 'a:1:{i:1;s:0:\"\";}', 'a:1:{i:1;s:0:\"\";}', '', '', 0, 0, 0, 0, '', '100%', 0, '', 'top left', 'no-repeat', 4, 0, 'a:4:{s:4:\"html\";a:1:{s:4:\"text\";a:1:{i:1;s:0:\"\";}}s:7:\"product\";a:4:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:0:\"\";s:5:\"width\";s:3:\"400\";s:6:\"height\";s:3:\"400\";}s:10:\"categories\";a:7:{s:10:\"categories\";a:0:{}s:7:\"columns\";s:1:\"1\";s:7:\"submenu\";s:1:\"1\";s:14:\"image_position\";s:1:\"1\";s:11:\"image_width\";s:0:\"\";s:12:\"image_height\";s:0:\"\";s:15:\"submenu_columns\";s:1:\"1\";}s:8:\"products\";a:5:{s:7:\"heading\";a:1:{i:1;s:0:\"\";}s:8:\"products\";a:0:{}s:7:\"columns\";s:1:\"1\";s:11:\"image_width\";s:0:\"\";s:12:\"image_height\";s:0:\"\";}}'),
(2, 0, 0, 1, '', 'a:1:{i:1;s:5:\"About\";}', '', 'a:1:{i:1;s:0:\"\";}', 'a:1:{i:1;s:0:\"\";}', '', '', 0, 0, 0, 0, '', '100%', 0, '', 'top left', 'no-repeat', 4, 0, 'a:4:{s:4:\"html\";a:1:{s:4:\"text\";a:1:{i:1;s:0:\"\";}}s:7:\"product\";a:4:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:0:\"\";s:5:\"width\";s:3:\"400\";s:6:\"height\";s:3:\"400\";}s:10:\"categories\";a:7:{s:10:\"categories\";a:0:{}s:7:\"columns\";s:1:\"1\";s:7:\"submenu\";s:1:\"1\";s:14:\"image_position\";s:1:\"1\";s:11:\"image_width\";s:0:\"\";s:12:\"image_height\";s:0:\"\";s:15:\"submenu_columns\";s:1:\"1\";}s:8:\"products\";a:5:{s:7:\"heading\";a:1:{i:1;s:0:\"\";}s:8:\"products\";a:0:{}s:7:\"columns\";s:1:\"1\";s:11:\"image_width\";s:0:\"\";s:12:\"image_height\";s:0:\"\";}}'),
(3, 1, 0, 0, '', 'a:1:{i:1;s:7:\"Boisson\";}', '', 'a:1:{i:1;s:0:\"\";}', 'a:1:{i:1;s:0:\"\";}', '', '', 0, 0, 0, 0, '', '100%', 0, '', 'top left', 'no-repeat', 4, 0, 'a:4:{s:4:\"html\";a:1:{s:4:\"text\";a:1:{i:1;s:0:\"\";}}s:7:\"product\";a:4:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:0:\"\";s:5:\"width\";s:3:\"400\";s:6:\"height\";s:3:\"400\";}s:10:\"categories\";a:7:{s:10:\"categories\";a:0:{}s:7:\"columns\";s:1:\"1\";s:7:\"submenu\";s:1:\"1\";s:14:\"image_position\";s:1:\"1\";s:11:\"image_width\";s:0:\"\";s:12:\"image_height\";s:0:\"\";s:15:\"submenu_columns\";s:1:\"1\";}s:8:\"products\";a:5:{s:7:\"heading\";a:1:{i:1;s:0:\"\";}s:8:\"products\";a:0:{}s:7:\"columns\";s:1:\"1\";s:11:\"image_width\";s:0:\"\";s:12:\"image_height\";s:0:\"\";}}'),
(4, 1, 0, 1, '', 'a:1:{i:1;s:11:\"Charcuterie\";}', '', 'a:1:{i:1;s:0:\"\";}', 'a:1:{i:1;s:0:\"\";}', '', '', 0, 0, 0, 0, '', '100%', 0, '', 'top left', 'no-repeat', 4, 0, 'a:4:{s:4:\"html\";a:1:{s:4:\"text\";a:1:{i:1;s:0:\"\";}}s:7:\"product\";a:4:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:0:\"\";s:5:\"width\";s:3:\"400\";s:6:\"height\";s:3:\"400\";}s:10:\"categories\";a:7:{s:10:\"categories\";a:0:{}s:7:\"columns\";s:1:\"1\";s:7:\"submenu\";s:1:\"1\";s:14:\"image_position\";s:1:\"1\";s:11:\"image_width\";s:0:\"\";s:12:\"image_height\";s:0:\"\";s:15:\"submenu_columns\";s:1:\"1\";}s:8:\"products\";a:5:{s:7:\"heading\";a:1:{i:1;s:0:\"\";}s:8:\"products\";a:0:{}s:7:\"columns\";s:1:\"1\";s:11:\"image_width\";s:0:\"\";s:12:\"image_height\";s:0:\"\";}}');

-- --------------------------------------------------------

--
-- Structure de la table `oc_mega_menu_links`
--

DROP TABLE IF EXISTS `oc_mega_menu_links`;
CREATE TABLE IF NOT EXISTS `oc_mega_menu_links` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` text,
  `name_for_autocomplete` text,
  `url` text,
  `label` text,
  `label_text` text,
  `label_background` text,
  `image` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_mega_menu_modules`
--

DROP TABLE IF EXISTS `oc_mega_menu_modules`;
CREATE TABLE IF NOT EXISTS `oc_mega_menu_modules` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_mega_menu_modules`
--

INSERT INTO `oc_mega_menu_modules` (`id`, `name`) VALUES
(1, 'vertical');

-- --------------------------------------------------------

--
-- Structure de la table `oc_modification`
--

DROP TABLE IF EXISTS `oc_modification`;
CREATE TABLE IF NOT EXISTS `oc_modification` (
  `modification_id` int(11) NOT NULL AUTO_INCREMENT,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`modification_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(2, 2, 'Themevolty', 'themevolty', 'themevolty', '3.x', '', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n    <code>themevolty</code>\r\n    <name>Themevolty</name>\r\n    <version>3.x</version>\r\n    <author>themevolty</author>\r\n    \r\n    <file path=\"admin/controller/extension/theme/default.php\">\r\n        <operation>\r\n            <search><![CDATA[if (($this->request->server[\'REQUEST_METHOD\'] == \'POST\') && $this->validate()) {]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                if($this->request->post[\'theme_default_directory\'] == \"opc_electronic_electron_2501\"){\r\n                        $file       = DIR_SYSTEM.\"framework.php\";\r\n                        $data         = \"global \\$registry;\";\r\n                        $filecontent  = file_get_contents($file);\r\n                        $searchpos    = strpos($filecontent, \'global\');\r\n                        if(!empty($searchpos)){\r\n                        }else{\r\n                          $pos      = strpos($filecontent, \'<?php\');\r\n                          $filecontent  = substr($filecontent, 0, 5).\"\\r\\n\".$data.\"\\r\\n\".substr($filecontent, 5);\r\n                          file_put_contents($file, $filecontent);\r\n                        }\r\n                      }\r\n            ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"admin/language/en-gb/design/layout.php\">\r\n        <operation>\r\n            <search><![CDATA[$_[\'text_column_right\']   = \'Column Right\';]]></search>\r\n            <add position=\"after\"><![CDATA[$_[\'text_footer_top\']    = \'Footer Top\';]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"admin/view/template/design/layout_form.twig\">\r\n        <operation>\r\n            <search><![CDATA[onclick=\"addModule(\'content-bottom\');\"]]></search>\r\n            <add position=\"after\" offset=\"5\"><![CDATA[ <!-- footer  top -->\r\n                 <table id=\"module-footer-top\" class=\"table table-striped table-bordered table-hover\">\r\n                  <thead>\r\n                    <tr>\r\n                      <td class=\"text-center\">{{ text_footer_top }}</td>\r\n                    </tr>\r\n                  </thead>\r\n                  <tbody>\r\n                    {% for layout_module in layout_modules %}\r\n                    {% if layout_module.position == \'footer_top\' %}\r\n                    <tr id=\"module-row{{ module_row }}\">\r\n                      <td class=\"text-left\"><div class=\"input-group\">\r\n                          <select name=\"layout_module[{{ module_row }}][code]\" class=\"form-control input-sm\">\r\n                            {% for extension in extensions %}\r\n                            <optgroup label=\"{{ extension.name }}\">\r\n                            {% if not extension.module %}\r\n                            {% if extension.code == layout_module.code %}\r\n                            <option value=\"{{ extension.code }}\" selected=\"selected\">{{ extension.name }}</option>\r\n                            {% else %}\r\n                            <option value=\"{{ extension.code }}\">{{ extension.name }}</option>\r\n                            {% endif %}\r\n                            {% else %}\r\n                            {% for module in extension.module %}\r\n                            {% if module.code == layout_module.code %}\r\n                            <option value=\"{{ module.code }}\" selected=\"selected\">{{ module.name }}</option>\r\n                            {% else %}\r\n                            <option value=\"{{ module.code }}\">{{ module.name }}</option>\r\n                            {% endif %}\r\n                            {% endfor %}\r\n                            {% endif %}\r\n                            </optgroup>\r\n                            {% endfor %}\r\n                          </select>\r\n                          <input type=\"hidden\" name=\"layout_module[{{ module_row }}][position]\" value=\"{{ layout_module.position }}\" />\r\n                          <input type=\"hidden\" name=\"layout_module[{{ module_row }}][sort_order]\" value=\"{{ layout_module.sort_order }}\" />\r\n                          <div class=\"input-group-btn\"><a href=\"{{ layout_module.edit }}\" type=\"button\" data-toggle=\"tooltip\" title=\"{{ button_edit }}\" target=\"_blank\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-pencil\"></i></a>\r\n                            <button type=\"button\" onclick=\"$(\'#module-row{{ module_row }}\').remove();\" data-toggle=\"tooltip\" title=\"{{ button_remove }}\" class=\"btn btn-danger btn-sm\"><i class=\"fa fa fa-minus-circle\"></i></button>\r\n                          </div>\r\n                        </div></td>\r\n                    </tr>\r\n                    {% set module_row = module_row + 1 %}\r\n                    {% endif %}\r\n                    {% endfor %}\r\n                  </tbody>\r\n                  <tfoot>\r\n                    <tr>\r\n                      <td class=\"text-left\"><div class=\"input-group\">\r\n                          <select class=\"form-control input-sm\">\r\n                            <option value=\"\"></option>\r\n                            {% for extension in extensions %}\r\n                            <optgroup label=\"{{ extension.name }}\">\r\n                            {% if not extension.module %}\r\n                            <option value=\"{{ extension.code }}\">{{ extension.name }}</option>\r\n                            {% else %}\r\n                            {% for module in extension.module %}\r\n                            <option value=\"{{ module.code }}\">{{ module.name }}</option>\r\n                            {% endfor %}\r\n                            {% endif %}\r\n                            </optgroup>\r\n                            {% endfor %}\r\n                          </select>\r\n                          <div class=\"input-group-btn\">\r\n                            <button type=\"button\" onclick=\"addModule(\'footer-top\');\" data-toggle=\"tooltip\" title=\"{{ button_module_add }}\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-plus-circle\"></i></button>\r\n                          </div>\r\n                        </div></td>\r\n                    </tr>\r\n                  </tfoot>\r\n                </table>\r\n                <!-- end -->]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[$(\'#module-column-left,]]></search>\r\n            <add position=\"replace\"><![CDATA[$(\'#module-column-left, #module-footer-top,]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"admin/language/en-gb/common/column_left.php\">\r\n        <operation>\r\n            <search><![CDATA[$_[\'text_other_status\']         = \'Other Statuses\';]]></search>\r\n            <add position=\"after\"><![CDATA[$_[\'text_themevolty\']             = \'ThemeVolty Extension\';]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"admin/model/catalog/category.php\">\r\n        <operation>\r\n            <search><![CDATA[public function getCategories($data = array()) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                    public function getparentCategories($data = array()) {\r\n                        $sql = \"SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR \'&nbsp;&nbsp;&gt;&nbsp;&nbsp;\') AS name, c1.parent_id, c1.sort_order FROM \" . DB_PREFIX . \"category_path cp LEFT JOIN \" . DB_PREFIX . \"category c1 ON (cp.category_id = c1.category_id) LEFT JOIN \" . DB_PREFIX . \"category c2 ON (cp.path_id = c2.category_id) LEFT JOIN \" . DB_PREFIX . \"category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN \" . DB_PREFIX . \"category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\' AND cd2.language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\' AND c1.parent_id = \'0\'\";\r\n\r\n                        if (!empty($data[\'filter_name\'])) {\r\n                            $sql .= \" AND cd2.name LIKE \'%\" . $this->db->escape($data[\'filter_name\']) . \"%\'\";\r\n                        }\r\n\r\n                        $sql .= \" GROUP BY cp.category_id\";\r\n\r\n                        $sort_data = array(\r\n                            \'name\',\r\n                            \'sort_order\'\r\n                        );\r\n\r\n                        if (isset($data[\'sort\']) && in_array($data[\'sort\'], $sort_data)) {\r\n                            $sql .= \" ORDER BY \" . $data[\'sort\'];\r\n                        } else {\r\n                            $sql .= \" ORDER BY sort_order\";\r\n                        }\r\n\r\n                        if (isset($data[\'order\']) && ($data[\'order\'] == \'DESC\')) {\r\n                            $sql .= \" DESC\";\r\n                        } else {\r\n                            $sql .= \" ASC\";\r\n                        }\r\n\r\n                        if (isset($data[\'start\']) || isset($data[\'limit\'])) {\r\n                            if ($data[\'start\'] < 0) {\r\n                                $data[\'start\'] = 0;\r\n                            }\r\n\r\n                            if ($data[\'limit\'] < 1) {\r\n                                $data[\'limit\'] = 20;\r\n                            }\r\n\r\n                            $sql .= \" LIMIT \" . (int)$data[\'start\'] . \",\" . (int)$data[\'limit\'];\r\n                        }\r\n\r\n                        $query = $this->db->query($sql);\r\n\r\n                        return $query->rows;\r\n                    }\r\n            ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"admin/controller/common/column_left.php\">\r\n        <operation>\r\n            <search><![CDATA[// Stats]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                // themevolty\r\n                if($this->config->get(\'theme_default_directory\') == \"opc_electronic_electron_2501\"){\r\n                    $themevolty = array();\r\n                    \r\n                    $this->load->model(\'catalog/tvcmsbrandlist\');\r\n\r\n                    $module_name = $this->model_catalog_tvcmsbrandlist->getmodulename();\r\n                    $themevolty[] = array(\r\n                                \'name\'     => \'Custom Setting\',\r\n                                \'href\'     => $this->url->link(\'extension/module/tvcmscustomsetting\', \'user_token=\' . $this->session->data[\'user_token\'], true),\r\n                                \'children\' => array()       \r\n                            );\r\n                    foreach ($module_name as $key => $value) {\r\n                        if ($this->user->hasPermission(\'access\', \'report/report\')) {\r\n                            $themevolty[] = array(\r\n                                \'name\'     => $value[\'name\'],\r\n                                \'href\'     => $this->url->link(\'extension/module/\'.$value[\'code\'].\'\', \'user_token=\' . $this->session->data[\'user_token\'] . \'&module_id=\' . $value[\'module_id\'], true),\r\n                                \'children\' => array()       \r\n                            );\r\n                        }\r\n                    }\r\n                    $themevolty[] = array(\r\n                        \'name\'     => \"Comment List\",\r\n                        \'href\'     => $this->url->link(\'extension/module/tvcmscommentlist\', \'user_token=\'. $this->session->data[\'user_token\'] , true),\r\n                        \'children\' => array()       \r\n                    );\r\n                    $themevolty[] = array(\r\n                        \'name\'     => \"News Letter List\",\r\n                        \'href\'     => $this->url->link(\'extension/module/tvcmsnewsletterlist\', \'user_token=\'. $this->session->data[\'user_token\'] , true),\r\n                        \'children\' => array()       \r\n                    );\r\n                    $data[\'menus\'][] = array(\r\n                        \'id\'       => \'menu-themevolty\',\r\n                        \'icon\'     => \'fa-television\',\r\n                        \'name\'     => $this->language->get(\'text_themevolty\'),\r\n                        \'href\'     => \'\',\r\n                        \'children\' => $themevolty\r\n                    );\r\n                }\r\n            ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"admin/controller/startup/startup.php\">\r\n        <operation>\r\n            <search><![CDATA[// Cart]]></search>\r\n            <add position=\"after\"><![CDATA[// Themevolty Status\r\n        $this->registry->set(\'Tvcmsthemevoltystatus\', new Cart\\tvcmsthemevoltystatus($this->registry));]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/product/manufacturer.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                $data[\'comparedisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                $data[\'wishlistdisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                $date = $this->model_catalog_product->getcustomProductSpecials($result[\'product_id\']);\r\n                if(isset($date[\'date_start\'])){\r\n                    $sdate = $date[\'date_start\'];\r\n                }else{\r\n                    $sdate = \"\";\r\n                }\r\n\r\n                if(isset($date[\'date_end\'])){\r\n                    $edate = $date[\'date_end\'];\r\n                }else{\r\n                    $edate = \"\";\r\n                }\r\n                if ($result[\'image\']) {\r\n                    $gridimage = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                } else {\r\n                    $gridimage = $this->model_tool_image->resize(\'placeholder.png\', $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                }\r\n            $gethoverimage   = $this->model_catalog_product->getproductimage($result[\'product_id\']);\r\n\r\n                if(!empty(current($gethoverimage))){\r\n                    $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_width\'), $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_height\'));\r\n                    $gridhoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                }else{\r\n                    $gridhoverimage = $gridimage;\r\n                    $hoverimage = $image;\r\n                }\r\n                ]]></add>\r\n        </operation>   \r\n        <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                \'start_date\' => $sdate,\r\n                \'date_end\'   => $edate,\r\n                \'hoverimage\' => $hoverimage,\r\n                \'gridimage\' => $gridimage,\r\n                \'gridhoverimage\' => $gridhoverimage,\r\n                ]]></add>\r\n        </operation>   \r\n        <operation>\r\n            <search><![CDATA[$product_total = $this->model_catalog_product->getTotalProducts($filter_data);]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n              $data[\'no_product\'] = $product_total; \r\n              if(!empty($product_total)){\r\n                    $data[\'category_data\'] = \"ttvcontent-full-width\";\r\n                }else{\r\n                    $data[\'category_data\'] = \"\";\r\n                }    \r\n            ]]></add>\r\n        </operation>   \r\n    </file>\r\n    <file path=\"catalog/controller/product/special.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                $data[\'comparedisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                $data[\'wishlistdisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                $date = $this->model_catalog_product->getcustomProductSpecials($result[\'product_id\']);\r\n                if(isset($date[\'date_start\'])){\r\n                    $sdate = $date[\'date_start\'];\r\n                }else{\r\n                    $sdate = \"\";\r\n                }\r\n\r\n                if(isset($date[\'date_end\'])){\r\n                    $edate = $date[\'date_end\'];\r\n                }else{\r\n                    $edate = \"\";\r\n                }\r\n                $gethoverimage   = $this->model_catalog_product->getproductimage($result[\'product_id\']);\r\n                if ($result[\'image\']) {\r\n                    $gridimage = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                } else {\r\n                    $gridimage = $this->model_tool_image->resize(\'placeholder.png\', $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                }\r\n                if(!empty(current($gethoverimage))){\r\n                    $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_width\'), $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_height\'));\r\n                    $gridhoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                }else{\r\n                    $hoverimage = $image;\r\n                    $gridhoverimage = $gridimage;\r\n                }\r\n                ]]></add>\r\n        </operation>   \r\n        <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                \'start_date\' => $sdate,\r\n                \'date_end\'   => $edate,\r\n                \'hoverimage\' => $hoverimage,\r\n                \'gridimage\' => $gridimage,\r\n                \'gridhoverimage\' => $gridhoverimage,\r\n                ]]></add>\r\n        </operation>   \r\n        <operation>\r\n            <search><![CDATA[$product_total = $this->model_catalog_product->getTotalProductSpecials()]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n              $data[\'no_product\'] = $product_total; \r\n              if(!empty($product_total)){\r\n                    $data[\'category_data\'] = \"ttvcontent-full-width\";\r\n                }else{\r\n                    $data[\'category_data\'] = \"\";\r\n                }  \r\n            ]]></add>\r\n        </operation>   \r\n    </file>\r\n    <file path=\"catalog/controller/startup/startup.php\">\r\n        <operation>\r\n            <search><![CDATA[// Cart]]></search>\r\n            <add position=\"after\"><![CDATA[// Themevolty Status\r\n        $this->registry->set(\'Tvcmsthemevoltystatus\', new Cart\\tvcmsthemevoltystatus($this->registry));]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/product/category.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                    $data[\'comparedisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                    $data[\'wishlistdisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                    $date = $this->model_catalog_product->getcustomProductSpecials($result[\'product_id\']);\r\n                    if(isset($date[\'date_start\'])){\r\n                        $sdate = $date[\'date_start\'];\r\n                    }else{\r\n                        $sdate = \"\";\r\n                    }\r\n\r\n                    if(isset($date[\'date_end\'])){\r\n                        $edate = $date[\'date_end\'];\r\n                    }else{\r\n                        $edate = \"\";\r\n                    }\r\n                    if ($result[\'image\']) {\r\n                        $gridimage = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                    } else {\r\n                        $gridimage = $this->model_tool_image->resize(\'placeholder.png\', $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                    }\r\n\r\n                    $gethoverimage   = $this->model_catalog_product->getproductimage($result[\'product_id\']);\r\n\r\n                    if(!empty(current($gethoverimage))){\r\n                        $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_width\'), $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_height\'));\r\n                        $gridhoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n\r\n                    }else{\r\n                        $hoverimage = $image;\r\n                        $gridhoverimage = $gridimage;\r\n                    }\r\n                ]]></add>\r\n        </operation>   \r\n        <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                \'start_date\' => $sdate,\r\n                \'date_end\'   => $edate,\r\n                \'hoverimage\' => $hoverimage,\r\n                \'gridimage\' => $gridimage,\r\n                \'gridhoverimage\' => $gridhoverimage,\r\n                ]]></add>\r\n        </operation>\r\n         <operation>\r\n            <search><![CDATA[$results = $this->model_catalog_product->getProducts($filter_data)]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                $data[\'no_product\'] = $product_total;\r\n                if(!empty($product_total)){\r\n                    $data[\'category_data\'] = \"ttvcontent-full-width\";\r\n                }else{\r\n                    $data[\'category_data\'] = \"\";\r\n                }\r\n\r\n            ]]></add>\r\n        </operation>      \r\n    </file>\r\n\r\n    \r\n    <file path=\"catalog/controller/product/product.php\">\r\n        <operation>\r\n            <search><![CDATA[if ((float)$result[\'special\']) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                $data[\'comparedisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                $data[\'wishlistdisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                $date = $this->model_catalog_product->getcustomProductSpecials($result[\'product_id\']);\r\n                if(isset($date[\'date_start\'])){\r\n                    $sdate = $date[\'date_start\'];\r\n                }else{\r\n                    $sdate = \"\";\r\n                }\r\n\r\n                if(isset($date[\'date_end\'])){\r\n                    $edate = $date[\'date_end\'];\r\n                }else{\r\n                    $edate = \"\";\r\n                }\r\n                $gethoverimage   = $this->model_catalog_product->getproductimage($result[\'product_id\']);\r\n                if(!empty(current($gethoverimage))){\r\n                    $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_width\'), $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_height\'));\r\n                    \r\n                }else{\r\n                    $hoverimage = $image;\r\n                }\r\n                ]]></add>\r\n        </operation>   \r\n         <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                \'start_date\' => $sdate,\r\n                \'date_end\'   => $edate,\r\n                \'hoverimage\' => $hoverimage,\r\n            ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/model/catalog/product.php\">\r\n        <operation>\r\n            <search><![CDATA[class ModelCatalogProduct extends Model {]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                 public function getproductimage($product_id) {\r\n                    $query = $this->db->query(\"SELECT image FROM \" . DB_PREFIX . \"product_image WHERE product_id =  \'\" . (int)$product_id . \"\'\");\r\n                        return $query->rows;\r\n                }\r\n                public function getcategoryid($product_id) {\r\n                    $query = $this->db->query(\"SELECT category_id FROM \" . DB_PREFIX . \"product_to_category WHERE product_id = \'\" . (int)$product_id . \"\'\");\r\n                    return $query->row[\'category_id\'];\r\n                }\r\n                public function getcategoryname($category_id) {\r\n                    $query = $this->db->query(\"SELECT name FROM \" . DB_PREFIX . \"category_description WHERE category_id = \'\" . (int)$category_id . \"\'\");\r\n                    return $query->row[\'name\'];\r\n                }\r\n                public function getProductid($category_id) {\r\n                    $query = $this->db->query(\"SELECT * FROM \" . DB_PREFIX . \"product_to_category WHERE category_id IN (\".$category_id.\") GROUP BY product_id\");\r\n                    return $query->rows;\r\n                }\r\n                public function getcateid($category_id) {\r\n                    $query = $this->db->query(\"SELECT category_id FROM \" . DB_PREFIX . \"category WHERE parent_id  in (\".$category_id.\")\");      \r\n                    return $query->rows;\r\n                }\r\n                public function getmoduledetail($module) {\r\n                    $query = $this->db->query(\"SELECT setting FROM \" . DB_PREFIX . \"module WHERE code  = \'\".$module.\"\'\");       \r\n                    return $query->row;\r\n                }\r\n                public function gettvtabproduct() {\r\n                    $query = $this->db->query(\"SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM \" . DB_PREFIX . \"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = \'\" . (int)$this->config->get(\'config_customer_group_id\') . \"\' AND pd2.quantity = \'1\' AND ((pd2.date_start = \'0000-00-00\' OR pd2.date_start < NOW()) AND (pd2.date_end = \'0000-00-00\' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM \" . DB_PREFIX . \"product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = \'\" . (int)$this->config->get(\'config_customer_group_id\') . \"\' AND ((ps.date_start = \'0000-00-00\' OR ps.date_start < NOW()) AND (ps.date_end = \'0000-00-00\' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM \" . DB_PREFIX . \"product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = \'\" . (int)$this->config->get(\'config_customer_group_id\') . \"\') AS reward, (SELECT ss.name FROM \" . DB_PREFIX . \"stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\') AS stock_status, (SELECT wcd.unit FROM \" . DB_PREFIX . \"weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\') AS weight_class, (SELECT lcd.unit FROM \" . DB_PREFIX . \"length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\') AS length_class, (SELECT AVG(rating) AS total FROM \" . DB_PREFIX . \"review r1 WHERE r1.product_id = p.product_id AND r1.status = \'1\' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM \" . DB_PREFIX . \"review r2 WHERE r2.product_id = p.product_id AND r2.status = \'1\' GROUP BY r2.product_id) AS reviews, p.sort_order FROM \" . DB_PREFIX . \"product p LEFT JOIN \" . DB_PREFIX . \"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN \" . DB_PREFIX . \"product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN \" . DB_PREFIX . \"manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE pd.language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\' AND p.status = \'1\' AND p.date_available <= NOW() AND p2s.store_id = \'\" . (int)$this->config->get(\'config_store_id\') . \"\'\");\r\n                    if ($query->num_rows) {\r\n                        return array(\r\n                            \'product_id\'       => $query->row[\'product_id\'],\r\n                            \'name\'             => $query->row[\'name\'],\r\n                            \'description\'      => $query->row[\'description\'],\r\n                            \'meta_title\'       => $query->row[\'meta_title\'],\r\n                            \'meta_description\' => $query->row[\'meta_description\'],\r\n                            \'meta_keyword\'     => $query->row[\'meta_keyword\'],\r\n                            \'tag\'              => $query->row[\'tag\'],\r\n                            \'model\'            => $query->row[\'model\'],\r\n                            \'sku\'              => $query->row[\'sku\'],\r\n                            \'upc\'              => $query->row[\'upc\'],\r\n                            \'ean\'              => $query->row[\'ean\'],\r\n                            \'jan\'              => $query->row[\'jan\'],\r\n                            \'isbn\'             => $query->row[\'isbn\'],\r\n                            \'mpn\'              => $query->row[\'mpn\'],\r\n                            \'location\'         => $query->row[\'location\'],\r\n                            \'quantity\'         => $query->row[\'quantity\'],\r\n                            \'stock_status\'     => $query->row[\'stock_status\'],\r\n                            \'image\'            => $query->row[\'image\'],\r\n                            \'manufacturer_id\'  => $query->row[\'manufacturer_id\'],\r\n                            \'manufacturer\'     => $query->row[\'manufacturer\'],\r\n                            \'price\'            => ($query->row[\'discount\'] ? $query->row[\'discount\'] : $query->row[\'price\']),\r\n                            \'special\'          => $query->row[\'special\'],\r\n                            \'reward\'           => $query->row[\'reward\'],\r\n                            \'points\'           => $query->row[\'points\'],\r\n                            \'tax_class_id\'     => $query->row[\'tax_class_id\'],\r\n                            \'date_available\'   => $query->row[\'date_available\'],\r\n                            \'weight\'           => $query->row[\'weight\'],\r\n                            \'weight_class_id\'  => $query->row[\'weight_class_id\'],\r\n                            \'length\'           => $query->row[\'length\'],\r\n                            \'width\'            => $query->row[\'width\'],\r\n                            \'height\'           => $query->row[\'height\'],\r\n                            \'length_class_id\'  => $query->row[\'length_class_id\'],\r\n                            \'subtract\'         => $query->row[\'subtract\'],\r\n                            \'rating\'           => round($query->row[\'rating\']),\r\n                            \'reviews\'          => $query->row[\'reviews\'] ? $query->row[\'reviews\'] : 0,\r\n                            \'minimum\'          => $query->row[\'minimum\'],\r\n                            \'sort_order\'       => $query->row[\'sort_order\'],\r\n                            \'status\'           => $query->row[\'status\'],\r\n                            \'date_added\'       => $query->row[\'date_added\'],\r\n                            \'date_modified\'    => $query->row[\'date_modified\'],\r\n                            \'viewed\'           => $query->row[\'viewed\']\r\n                        );\r\n                    } else {\r\n                        return false;\r\n                    }\r\n                }\r\n                public function getcustomProductSpecials($product_id) {\r\n                    $query = $this->db->query(\"SELECT * FROM \" . DB_PREFIX . \"product_special WHERE product_id = \'\".$product_id.\"\' \");\r\n                    return $query->row;\r\n                }\r\n            ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/common/currency.php\">\r\n        <operation>\r\n            <search><![CDATA[$data[\'currencies\'] = array();]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                            $data[\'config_car_id\'] = $this->session->data[\'currency\'];\r\n                ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/common/language.php\">\r\n        <operation>\r\n            <search><![CDATA[$data[\'languages\'] = array();]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                    $lan =  $this->model_localisation_language->getLanguage($this->config->get(\'config_language_id\'));\r\n                    $data[\'confid_l_id\'] = $lan[\'name\'];\r\n                ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/common/footer.php\">\r\n        <operation>\r\n            <search><![CDATA[$data[\'contact\']]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                if($this->config->get(\'theme_default_directory\') == \"opc_electronic_electron_2501\"){\r\n\r\n                    $this->load->language(\'extension/module/tvcmscustomtext\');\r\n                                        $data[\'footerlogo\']                 = $this->load->controller(\'common/tvcmsfooterlogo\');\r\n\r\n                    $data[\'newsletterdata\']                 = $this->load->controller(\'common/tvcmsnewsletter\');\r\n                    $data[\'store\']                          = $this->config->get(\'config_name\');\r\n                    $data[\'address\']                        = nl2br($this->config->get(\'config_address\'));\r\n                    $data[\'telephone\']                      = $this->config->get(\'config_telephone\');\r\n                    $data[\'fax\']                            = $this->config->get(\'config_fax\');\r\n                    $data[\'email\']                          = $this->config->get(\'config_email\');\r\n                    $data[\'tvtags\']                         = $this->load->controller(\'common/tvcmstags\');\r\n                    $sql                                    = $this->db->query(\"SELECT * FROM \" . DB_PREFIX . \"country WHERE country_id = \'\" . $this->config->get(\'config_country_id\') . \"\' \");\r\n                    $data[\'country_name\']                   = $sql->row[\'name\'];\r\n\r\n                    $data[\'socialicon\']                     = $this->load->controller(\'common/tvcmssocialicon\');\r\n                    $data[\'paymenticon\']                    = $this->load->controller(\'common/tvcmspaymenticon\');\r\n                    $store_time                             = $this->load->controller(\'common/tvcmsstoretime\');\r\n\r\n                    $data[\'tvcmsstoretime_status\']          = $store_time[\'tvcmsstoretime_status\'];\r\n                    $data[\'tvcmsstoretime_monday_friday\']   = $store_time[\'tvcmsstoretime_monday_friday\'];\r\n                    $data[\'tvcmsstoretime_saterday\']        = $store_time[\'tvcmsstoretime_saterday\'];\r\n                    $data[\'tvcmsstoretime_sunday\']          = $store_time[\'tvcmsstoretime_sunday\'];\r\n                    $data[\'tvcmsstoretime_title\']           = $store_time[\'tvcmsstoretime_title\'];\r\n                    $data[\'tvcmsstoretime_information\']     = $store_time[\'tvcmsstoretime_information\'];\r\n                     $data[\'tvcmscustomsetting_theme_css_path\']     = $this->config->get(\'tvcmscustomsetting_theme_css_path\');\r\n                    $data[\'theme_option_status\']            = $this->config->get(\'tvcmscustomsetting_configuration\')[\'themeoptionstatus\'];\r\n                    $customsetting                          = $this->load->controller(\'common/tvcmscustomsetting\');\r\n                    $data[\'customsetting_status\']           = $customsetting[\'status\'];\r\n                    $data[\'customsetting_customtext\']       = $customsetting[\'customsub_text\'];\r\n                    $data[\'customsetting_customtextlink\']   = $customsetting[\'customsub_textlink\'];\r\n                    \r\n\r\n                    if ($this->customer->isLogged()) {\r\n                        $this->load->model(\'account/wishlist\');\r\n\r\n                        $data[\'text_wishlist_tv\']   = sprintf($this->model_account_wishlist->getTotalWishlist());\r\n                    } else {\r\n                        $data[\'text_wishlist_tv\']   = sprintf((isset($this->session->data[\'wishlist\']) ? count($this->session->data[\'wishlist\']) : 0));\r\n                    }\r\n\r\n                    $data[\'ttvcpmpare\']             = $this->url->link(\'product/compare\', \'\', true);\r\n\r\n                    if(!empty($this->session->data[\'compare\'])){\r\n                        $data[\'ttvcpmpare_count\']   = count($this->session->data[\'compare\']);\r\n                    }else{\r\n                        $data[\'ttvcpmpare_count\']   = 0;\r\n                    }\r\n\r\n                    $data[\'text_compare\']       = $this->language->get(\'text_compare\');\r\n                    $data[\'text_wishlist\']      = $this->language->get(\'text_wishlist\');\r\n                    $data[\'text_mycart\']        = $this->language->get(\'text_mycart\');\r\n                    $data[\'text_myaccount\']     = $this->language->get(\'text_myaccount\');\r\n                    $data[\'text_scrolltop\']     = $this->language->get(\'text_scrolltop\');\r\n                    $data[\'text_storeinfo\']     = $this->language->get(\'text_storeinfo\');\r\n                    $data[\'text_youraccount\']   = $this->language->get(\'text_youraccount\');\r\n                    $data[\'text_ourcompany\']    = $this->language->get(\'text_ourcompany\');\r\n\r\n                    $data[\'link_wishlist\']      = $this->url->link(\'account/wishlist\', \'\', true);\r\n                    $data[\'link_compare\']       = $this->url->link(\'product/compare\', \'\', true);\r\n                    $data[\'link_account\']       = $this->url->link(\'account/account\', \'\', true);\r\n                    $data[\'link_addcart\']       = $this->url->link(\'checkout/cart\', \'\', true);\r\n                }\r\n                ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/common/header.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged()) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                if($this->config->get(\'theme_default_directory\') == \"opc_electronic_electron_2501\"){\r\n                     $data[\'headeremail\']   = $this->config->get(\'config_email\');\r\n                     $data[\'headeropen\']    = $this->config->get(\'config_open\');\r\n                     $data[\'headerphone\']   = $this->config->get(\'config_telephone\');\r\n                     $data[\'headertab\']     = $this->load->controller(\'common/tvcmstags\');\r\n\r\n\r\n                    $this->load->language(\'extension/module/tvcmscustomtext\');\r\n                    $data[\'tv_lang_search_label\']       = $this->language->get(\'tv_lang_search_label\');\r\n                    $data[\'tv_lang_search_text_label\']  = $this->language->get(\'tv_lang_search_text_label\');\r\n                    $data[\'tv_lang_removecart_label\']   = $this->language->get(\'tv_lang_removecart_label\');\r\n                    $data[\'tv_lang_subtottal_label\']    = $this->language->get(\'tv_lang_subtottal_label\');\r\n                    $data[\'tv_lang_shipping_label\']     = $this->language->get(\'tv_lang_shipping_label\');\r\n                    $data[\'tv_lang_tax_label\']          = $this->language->get(\'tv_lang_tax_label\');\r\n                    $data[\'tv_lang_total_label\']        = $this->language->get(\'tv_lang_total_label\');\r\n                    $data[\'tv_lang_checkout_label\']     = $this->language->get(\'tv_lang_checkout_label\');\r\n                    $data[\'boxlayout\']                  = $this->config->get(\'tvcmscustomsetting_configuration\')[\'boxlayout\'];\r\n                    $data[\'loader_img\']                 = $server.\"image/catalog/themevolty/pageloader/ttv_loading.gif\";\r\n                    $data[\'tvcmscustomsetting_background_style_sheet\']            = $this->config->get(\'tvcmscustomsetting_background_style_sheet\');\r\n                    $data[\'theme_option_status\']        = $this->config->get(\'tvcmscustomsetting_configuration\')[\'themeoptionstatus\'];\r\n                    \r\n                    if(!empty($this->session->data[\'compare\'])){\r\n                        $data[\'ttvcpmpare_count\']   = count($this->session->data[\'compare\']);\r\n                    }else{\r\n                        $data[\'ttvcpmpare_count\']   = 0;\r\n                    }\r\n                    $data[\'mousehoverimage\']            = $this->load->controller(\'common/tvcmscustomsetting\')[\'mousehoverimage\'];\r\n                    $data[\'pageloader\']                 = $this->load->controller(\'common/tvcmscustomsetting\')[\'pageloader\'];\r\n                    $data[\'mainmenustickystatus\']       = $this->load->controller(\'common/tvcmscustomsetting\')[\'mainmenustickystatus\'];\r\n                    $data[\'themeoption\']                = $this->load->controller(\'common/tvcmscustomsthemeoption\');\r\n                     $data[\'custom_text\']               = $this->load->controller(\'common/tvcmscustomsetting\')[\'custom_text\'];\r\n                    $data[\'text_support\']               = $this->language->get(\'text_support\');\r\n                    if(empty($_SERVER[\'QUERY_STRING\']) || $_SERVER[\'QUERY_STRING\'] == \"route=common/home\"){\r\n                        $data[\'homeid\'] = \"index\";\r\n                        $data[\'header1\'] = 0;\r\n                    }else{\r\n                        $data[\'homeid\'] = \"\";\r\n                        $data[\'header1\'] = \"ttvcmsposition-block\"; \r\n                    }\r\n                    if(!empty($this->session->data[\'customer_id\'])){\r\n                        $data[\'logincust\'] = $this->session->data[\'customer_id\'];\r\n                    }else{\r\n                        $data[\'logincust\'] = 0;\r\n                    }\r\n                }\r\n\r\n                ]]></add>\r\n        </operation>    \r\n        <operation>\r\n            <search><![CDATA[$this->load->model(\'account/wishlist\');]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                   $data[\'text_wishlist_tv\'] = sprintf($this->model_account_wishlist->getTotalWishlist());\r\n                ]]></add>\r\n        </operation> \r\n        <operation>\r\n            <search><![CDATA[(isset($this->session->data[\'wishlist\']) ? count($this->session->data[\'wishlist\']) : 0));]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                  $data[\'text_wishlist_tv\'] = sprintf((isset($this->session->data[\'wishlist\']) ? count($this->session->data[\'wishlist\']) : 0));\r\n                ]]></add>\r\n        </operation> \r\n         \r\n    </file>\r\n    <file path=\"catalog/controller/common/cart.php\">\r\n        <operation>\r\n            <search><![CDATA[$this->load->model(\'tool/image\');]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                    $data[\'ttv_text_items\'] = sprintf($this->cart->countProducts() + (isset($this->session->data[\'vouchers\']) ? count($this->session->data[\'vouchers\']) : 0), $this->currency->format($total, $this->session->data[\'currency\']));\r\n                ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/common/menu.php\">\r\n        <operation>\r\n            <search><![CDATA[$this->load->model(\'catalog/product\');]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                $this->load->language(\'extension/module/tvcmscustomtext\');\r\n                if (isset($this->request->get[\'path\'])) {\r\n                    $parts = explode(\'_\', (string)$this->request->get[\'path\']);\r\n                } else {\r\n                    $parts = array();\r\n                }\r\n\r\n                if (isset($parts[0])) {\r\n                    $data[\'category_id\'] = $parts[0];\r\n                } else {\r\n                    $data[\'category_id\'] = 0;\r\n                }\r\n                if(empty($_SERVER[\'QUERY_STRING\']) || $_SERVER[\'QUERY_STRING\'] == \"route=common/home\"){\r\n                    $data[\'menuhomeactive\'] = \"current\";\r\n                }else{\r\n                    $data[\'menuhomeactive\'] = \"\";\r\n                }\r\n                $this->load->model(\'account/wishlist\');\r\n                if ($this->customer->isLogged()) {\r\n                    $this->load->model(\'account/wishlist\');\r\n\r\n                    $data[\'text_wishlist_tv\']   = sprintf($this->model_account_wishlist->getTotalWishlist());\r\n                } else {\r\n                    $data[\'text_wishlist_tv\']   = sprintf((isset($this->session->data[\'wishlist\']) ? count($this->session->data[\'wishlist\']) : 0));\r\n                }\r\n\r\n                $data[\'ttvcpmpare\']             = $this->url->link(\'product/compare\', \'\', true);\r\n\r\n                if(!empty($this->session->data[\'compare\'])){\r\n                    $data[\'ttvcpmpare_count\']   = count($this->session->data[\'compare\']);\r\n                }else{\r\n                    $data[\'ttvcpmpare_count\']   = 0;\r\n                }\r\n                ]]></add>\r\n        </operation>  \r\n        <operation>\r\n            <search><![CDATA[\'name\'     => $category[\'name\'],]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                \'category_id\'     => $category[\'category_id\'],\r\n                ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/product/compare.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($product_info[\'image\']) {]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                    $compareimage = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_compare_width\'), $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_compare_height\'));\r\n                ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                        $gethoverimage   = $this->model_catalog_product->getproductimage($product_info[\'product_id\']);\r\n\r\n                        if(!empty(current($gethoverimage))){\r\n                            $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_compare_width\'), $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_compare_height\'));\r\n                        }else{\r\n                            $hoverimage = $compareimage;\r\n                        }\r\n                ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[\'thumb\'        => $image,]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                    \'compare_thumb\' => $compareimage,\r\n                    \'hoverimage\'    => $hoverimage,\r\n                ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[$this->load->model(\'tool/image\');]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                $this->load->language(\'extension/module/tvcmscustomtext\');\r\n                $data[\'text_productcompater\'] = $this->language->get(\'text_productcompater\');\r\n            ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/common/search.php\">\r\n        <operation>\r\n            <search><![CDATA[class ControllerCommonSearch extends Controller {]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                    public function autocomplete() {\r\n                        $json = array();\r\n\r\n                        if (isset($this->request->get[\'filter_name\'])) {\r\n\r\n                            $this->load->model(\'catalog/tvcmsmodule\');\r\n                            $this->load->model(\'tool/image\');\r\n                            \r\n                            if (isset($this->request->get[\'filter_name\'])) {\r\n                                $filter_name = $this->request->get[\'filter_name\'];\r\n                            } else {\r\n                                $filter_name = \'\';\r\n                            }\r\n\r\n                            $limit = $this->config->get(\'tvcmscustomsetting_configuration\')[\'searchlimit\'];\r\n\r\n                            $filter_data = array(\r\n                                \'filter_name\'  => $filter_name,\r\n                                \'start\'        => 0,\r\n                                \'limit\'        => $limit\r\n                            );\r\n                            $html       = \'\';\r\n                            $alldataget = $this->model_catalog_tvcmsmodule->getProducts($filter_data);\r\n                            $results = $alldataget->rows;\r\n                            $html .=    \'<div class=\\\'ttvcmssearch-dropdown\\\'>\';\r\n\r\n                            $html .=    \'<div class=\\\'ttvsearch-dropdown-close-wrapper\\\'>\r\n                                            <div class=\\\'ttvsearch-dropdown-close\\\'>Close</div>\r\n                                        </div>\';\r\n                            if (!empty($results)) {\r\n                                \r\n                                foreach ($results as $result) {                            \r\n                                    $prod_price      = $this->currency->format($this->tax->calculate($result[\'price\'], $result[\'tax_class_id\'], $this->config->get(\'config_tax\')), $this->session->data[\'currency\']);\r\n                                    \r\n                                    $special    = $this->currency->format($this->tax->calculate($result[\'special\'], $result[\'tax_class_id\'], $this->config->get(\'config_tax\')), $this->session->data[\'currency\']);\r\n                                    \r\n                                    $tax        = $this->currency->format((float)$result[\'special\'] ? $result[\'special\'] : $result[\'price\'], $this->session->data[\'currency\']);\r\n\r\n                                    $prod_link = $this->url->link(\'product/product\', \'product_id=\' . $result[\'product_id\']);\r\n                                    $prod_img = $this->model_tool_image->resize($result[\'image\'],98,98);\r\n                                    $prod_name = strip_tags(html_entity_decode($result[\'name\'], ENT_QUOTES, \'UTF-8\'));\r\n\r\n                                    $html .= \'\r\n                                            <div class=\\\'ttvsearch-all-dropdown-wrapper\\\'>\r\n                                                <div class=\\\'ttvsearch-dropdown-wrapper\\\'>\r\n                                                    <a href=\\\'\'.$prod_link.\'\\\'>\r\n                                                        <div class=\\\'ttvsearch-dropdown-img-block\\\'>\r\n                                                            <img src=\\\'\'.$prod_img.\'\\\' alt=\\\'\'.$prod_name.\'\\\' />\r\n                                                        </div>\r\n                                                        <div class=\\\'ttvsearch-dropdown-content-box\\\'>\r\n                                                            <div class=\\\'ttvsearch-dropdown-title\\\'>\'.$prod_name.\'</div>\r\n                                                            <div class=\\\'product-price-and-shipping\\\'>\r\n                                                                <span class=\\\'regular-price\\\'>$18.90</span>\r\n                                                                <span class=\\\'price\\\'>\'.$prod_price.\'</span>\r\n                                                            </div>\r\n                                                        </div>\r\n                                                    </a>\r\n                                                </div>\r\n                                            </div>\r\n                                            \';  \r\n                                }\r\n                                $html .=    \'<div class=\\\'ttvsearch-more-search-wrapper\\\'>\r\n                                                <div class=\\\'ttvsearch-more-search\\\'>More Result</div>\r\n                                            </div>\';\r\n                            }else{\r\n                                $html .= \'<div class=\\\'ttvsearch-dropdown-wrapper\\\'>No product Found</div>\';\r\n                            }\r\n                            $html .= \'</div>\';\r\n                        }\r\n\r\n                        if (!empty($html)) {\r\n                            print_r($html);\r\n                        }\r\n                    }\r\n                ]]></add>\r\n        </operation>      \r\n    </file>\r\n    <file path=\"catalog/controller/product/search.php\">                            \r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                $data[\'comparedisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                $data[\'wishlistdisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                $date = $this->model_catalog_product->getcustomProductSpecials($result[\'product_id\']);\r\n                    if(isset($date[\'date_start\'])){\r\n                        $sdate = $date[\'date_start\'];\r\n                    }else{\r\n                        $sdate = \"\";\r\n                    }\r\n\r\n                    if(isset($date[\'date_end\'])){\r\n                        $edate = $date[\'date_end\'];\r\n                    }else{\r\n                        $edate = \"\";\r\n                    }\r\n                    if ($result[\'image\']) {\r\n                        $gridimage = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                    } else {\r\n                        $gridimage = $this->model_tool_image->resize(\'placeholder.png\', $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                    }\r\n                    $gethoverimage   = $this->model_catalog_product->getproductimage($result[\'product_id\']);\r\n\r\n                    if(!empty(current($gethoverimage))){\r\n                        $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_width\'), $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_height\'));\r\n                        $gridhoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_width\'), $this->config->get(\'tvcmscustomsetting_tvcmsproductgridimg_img_height\'));\r\n                    }else{\r\n                        $hoverimage = $image;\r\n                        $gridhoverimage = $gridimage;\r\n                    }\r\n\r\n                ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                    \'start_date\'     => $sdate,\r\n                    \'date_end\'       => $edate,\r\n                    \'hoverimage\'     => $hoverimage,\r\n                    \'gridimage\'      => $gridimage,\r\n                    \'gridhoverimage\' => $gridhoverimage,\r\n                ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[$product_total = $this->model_catalog_product->getTotalProducts($filter_data);]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                $data[\'no_product\'] = $product_total;   \r\n                if(!empty($product_total)){\r\n                    $data[\'category_data\'] = \"ttvcontent-full-width\";\r\n                }else{\r\n                    $data[\'category_data\'] = \"\";\r\n                } \r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n\r\n    <file path=\"catalog/controller/extension/module/latest.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                $data[\'comparedisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                $data[\'wishlistdisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                $date = $this->model_catalog_product->getcustomProductSpecials($result[\'product_id\']);\r\n                    if(isset($date[\'date_start\'])){\r\n                        $sdate = $date[\'date_start\'];\r\n                    }else{\r\n                        $sdate = \"\";\r\n                    }\r\n\r\n                    if(isset($date[\'date_end\'])){\r\n                        $edate = $date[\'date_end\'];\r\n                    }else{\r\n                        $edate = \"\";\r\n                    }\r\n                    \r\n                    $gethoverimage   = $this->model_catalog_product->getproductimage($result[\'product_id\']);\r\n\r\n                    if(!empty(current($gethoverimage))){\r\n                        $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $setting[\'width\'], $setting[\'height\']);\r\n\r\n                    }else{\r\n                        $hoverimage = $image;\r\n                    }\r\n                ]]></add>\r\n        </operation>   \r\n        <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                \'start_date\' => $sdate,\r\n                \'date_end\'   => $edate,\r\n                \'hoverimage\' => $hoverimage,\r\n                ]]></add>\r\n        </operation> \r\n        <operation>\r\n            <search><![CDATA[public function index($setting) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                protected function status(){\r\n                    return $this->Tvcmsthemevoltystatus->tabproductstatus();\r\n                }            \r\n            ]]></add>\r\n        </operation> \r\n        <operation>\r\n            <search><![CDATA[if ($results) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                if($this->config->get(\'theme_default_directory\') == \"opc_electronic_electron_2501\"){\r\n                    $this->load->model(\'catalog/tvcmsmodule\');\r\n                    $status                                         = $this->status();\r\n                    $data[\'status_new_main_form\']                   = $status[\'new_prod\'][\'main_form\'];\r\n                    $data[\'status_new_home_title\']                  = $status[\'new_prod\'][\'home_title\'];\r\n                    $data[\'status_new_home_sub_title\']              = $status[\'new_prod\'][\'home_sub_title\'];\r\n                    $data[\'status_new_home_description\']            = $status[\'new_prod\'][\'home_description\'];\r\n                    $data[\'status_new_home_image\']                  = $status[\'new_prod\'][\'home_image\'];\r\n                    $data[\'lang_id\']                                = $this->config->get(\'config_language_id\');\r\n                    if(!empty($data[\'status_new_main_form\'])){\r\n                        $name           = \"tvcmstabproducts\";\r\n                        $status_info    = $this->model_catalog_tvcmsmodule->getmoduelstatus($name);\r\n                        $data_info      = json_decode($status_info[\'setting\'],1);\r\n                        \r\n                        if(!empty($data[\'status_new_home_title\'])){\r\n                            $data[\'new_hometitle\'] = $data_info[\'tvcmstabproducts_pro_new\'][\'lang_text\'][$data[\'lang_id\']][\'hometitle\'];\r\n                        }\r\n                        if(!empty($data[\'status_new_home_sub_title\'])){\r\n                            $data[\'new_homesubtitle\'] = $data_info[\'tvcmstabproducts_pro_new\'][\'lang_text\'][$data[\'lang_id\']][\'homesubtitle\'];\r\n                        }\r\n                        if(!empty($data[\'status_new_home_description\'])){\r\n                            $data[\'new_homedes\'] = $data_info[\'tvcmstabproducts_pro_new\'][\'lang_text\'][$data[\'lang_id\']][\'homedes\'];\r\n                        }\r\n                        if(!empty($data[\'status_new_home_image\'])){\r\n                            $data[\'new_img\'] = $data_info[\'tvcmstabproducts_pro_new\'][\'lang_text\'][$data[\'lang_id\']][\'img\'];\r\n                        }   \r\n                    }\r\n                }\r\n                ]]></add>\r\n        </operation>     \r\n    </file>\r\n    <file path=\"catalog/controller/extension/module/featured.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                    $data[\'comparedisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                    $data[\'wishlistdisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                    $date = $this->model_catalog_product->getcustomProductSpecials($product_info[\'product_id\']);\r\n                    if(isset($date[\'date_start\'])){\r\n                        $sdate = $date[\'date_start\'];\r\n                    }else{\r\n                        $sdate = \"\";\r\n                    }\r\n\r\n                    if(isset($date[\'date_end\'])){\r\n                        $edate = $date[\'date_end\'];\r\n                    }else{\r\n                        $edate = \"\";\r\n                    }\r\n\r\n                     $gethoverimage   = $this->model_catalog_product->getproductimage($product_info[\'product_id\']);\r\n\r\n                        if(!empty(current($gethoverimage))){\r\n                            $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $setting[\'width\'], $setting[\'height\']);\r\n\r\n                            \r\n                        }else{\r\n                            $hoverimage = $image;\r\n                        }\r\n                ]]></add>\r\n        </operation>   \r\n         <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                                         \'hoverimage\'    => $hoverimage,\r\n                                         \'start_date\'   => $sdate,\r\n                        \'date_end\'      => $edate,\r\n                ]]></add>\r\n        </operation>  \r\n        <operation>\r\n            <search><![CDATA[public function index($setting) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                protected function status(){\r\n                    return $this->Tvcmsthemevoltystatus->tabproductstatus();\r\n                }            \r\n            ]]></add>\r\n        </operation> \r\n         <operation>\r\n            <search><![CDATA[public function index($setting) {]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                if($this->config->get(\'theme_default_directory\') == \"opc_electronic_electron_2501\"){\r\n                    $this->load->model(\'catalog/tvcmsmodule\');\r\n                    $status                                         = $this->status();\r\n                    $data[\'status_featured_main_form\']              = $status[\'featured_prod\'][\'main_form\'];\r\n                    $data[\'status_featured_display_in_tab\']         = $status[\'featured_prod\'][\'display_in_tab\'];\r\n                    $data[\'status_featured_home_title\']             = $status[\'featured_prod\'][\'home_title\'];\r\n                    $data[\'status_featured_home_sub_title\']         = $status[\'featured_prod\'][\'home_sub_title\'];\r\n                    $data[\'status_featured_home_description\']       = $status[\'featured_prod\'][\'home_description\'];\r\n                    $data[\'status_featured_home_image\']             = $status[\'featured_prod\'][\'home_image\'];\r\n                    $data[\'lang_id\']                                = $this->config->get(\'config_language_id\');\r\n                    $name                                           = \"tvcmstabproducts\";\r\n                    $status_info                                    = $this->model_catalog_tvcmsmodule->getmoduelstatus($name);\r\n                    $data_info                                      = json_decode($status_info[\'setting\'],1);\r\n\r\n                    if(!empty($data[\'status_featured_main_form\'])){\r\n                        if(!empty($data[\'status_featured_home_title\'])){\r\n                            $data[\'featured_home_title\'] = $data_info[\'tvcmstabproducts_pro_fea\'][\'lang_text\'][$data[\'lang_id\']][\'hometitle\'];\r\n                        }\r\n                        if(!empty($data[\'status_featured_home_sub_title\'])){\r\n                            $data[\'featured_home_sub_title\'] = $data_info[\'tvcmstabproducts_pro_fea\'][\'lang_text\'][$data[\'lang_id\']][\'homesubtitle\'];\r\n                        }\r\n                        if(!empty($data[\'status_featured_home_description\'])){\r\n                            $data[\'featured_home_description\'] = $data_info[\'tvcmstabproducts_pro_fea\'][\'lang_text\'][$data[\'lang_id\']][\'homedes\'];\r\n                        }\r\n                        if(!empty($data[\'status_featured_home_image\'])){\r\n                            $data[\'featured_home_image\'] = $data_info[\'tvcmstabproducts_pro_fea\'][\'lang_text\'][$data[\'lang_id\']][\'img\'];\r\n                        }\r\n                    }\r\n                }\r\n                ]]></add>\r\n        </operation>     \r\n    </file>\r\n    <file path=\"catalog/controller/extension/module/bestseller.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                    $data[\'comparedisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                    $data[\'wishlistdisplay\']   = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                    $date = $this->model_catalog_product->getcustomProductSpecials($result[\'product_id\']);\r\n                    if(isset($date[\'date_start\'])){\r\n                        $sdate = $date[\'date_start\'];\r\n                    }else{\r\n                        $sdate = \"\";\r\n                    }\r\n\r\n                    if(isset($date[\'date_end\'])){\r\n                        $edate = $date[\'date_end\'];\r\n                    }else{\r\n                        $edate = \"\";\r\n                    }\r\n\r\n                     $gethoverimage   = $this->model_catalog_product->getproductimage($result[\'product_id\']);\r\n\r\n                        if(!empty(current($gethoverimage))){\r\n                            $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $setting[\'width\'], $setting[\'height\']);\r\n\r\n                            \r\n                        }else{\r\n                            $hoverimage = $image;\r\n                        }\r\n                ]]></add>\r\n        </operation>   \r\n         <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                    \'hoverimage\'    => $hoverimage,\r\n                    \'start_date\'    => $sdate,\r\n                    \'date_end\'      => $edate,\r\n                ]]></add>\r\n        </operation>    \r\n        <operation>\r\n            <search><![CDATA[public function index($setting) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                protected function status(){\r\n                    return $this->Tvcmsthemevoltystatus->tabproductstatus();\r\n                }            \r\n            ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[if ($results) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                if($this->config->get(\'theme_default_directory\') == \"opc_electronic_electron_2501\"){\r\n                    $this->load->model(\'catalog/tvcmsmodule\');\r\n                    $status                                         = $this->status();\r\n                    $data[\'status_best_main_form\']                  = $status[\'best_seller_prod\'][\'main_form\'];\r\n                    $data[\'status_best_home_title\']                 = $status[\'best_seller_prod\'][\'home_title\'];\r\n                    $data[\'status_best_home_sub_title\']             = $status[\'best_seller_prod\'][\'home_sub_title\'];\r\n                    $data[\'status_best_home_description\']           = $status[\'best_seller_prod\'][\'home_description\'];\r\n                    $data[\'status_best_home_image\']                 = $status[\'best_seller_prod\'][\'home_image\'];\r\n\r\n                    $data[\'lang_id\']                                = $this->config->get(\'config_language_id\');\r\n                    if(!empty($data[\'status_best_main_form\'])){\r\n                        $name           = \"tvcmstabproducts\";\r\n                        $status_info    = $this->model_catalog_tvcmsmodule->getmoduelstatus($name);\r\n                        $data_info      = json_decode($status_info[\'setting\'],1);\r\n                        \r\n                        if(!empty($data[\'status_best_home_title\'])){\r\n                            $data[\'best_hometitle\'] = $data_info[\'tvcmstabproducts_pro_best\'][\'lang_text\'][$data[\'lang_id\']][\'hometitle\'];\r\n                        }\r\n                        if(!empty($data[\'status_best_home_sub_title\'])){\r\n                            $data[\'best_homesubtitle\'] = $data_info[\'tvcmstabproducts_pro_best\'][\'lang_text\'][$data[\'lang_id\']][\'homesubtitle\'];\r\n                        }\r\n                        if(!empty($data[\'status_best_home_description\'])){\r\n                            $data[\'best_homedes\'] = $data_info[\'tvcmstabproducts_pro_best\'][\'lang_text\'][$data[\'lang_id\']][\'homedes\'];\r\n                        }\r\n                        if(!empty($data[\'status_best_home_image\'])){\r\n                            $data[\'best_img\'] = $data_info[\'tvcmstabproducts_pro_new\'][\'lang_text\'][$data[\'lang_id\']][\'img\'];\r\n                        }   \r\n                    }\r\n                }\r\n                ]]></add>\r\n        </operation>        \r\n    </file>\r\n    <file path=\"catalog/controller/extension/module/special.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->customer->isLogged() || !$this->config->get(\'config_customer_price\')) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                    $data[\'comparedisplay\']     = $this->config->get(\'tvcmscustomsetting_configuration\')[\'comparedisplay\'];\r\n                    $data[\'wishlistdisplay\']    = $this->config->get(\'tvcmscustomsetting_configuration\')[\'wishlistdisplay\'];\r\n                    $date = $this->model_catalog_product->getcustomProductSpecials($result[\'product_id\']);\r\n                    if(isset($date[\'date_start\'])){\r\n                        $sdate = $date[\'date_start\'];\r\n                    }else{\r\n                        $sdate = \"\";\r\n                    }\r\n\r\n                    if(isset($date[\'date_end\'])){\r\n                        $edate = $date[\'date_end\'];\r\n                    }else{\r\n                        $edate = \"\";\r\n                    }\r\n\r\n                     $gethoverimage   = $this->model_catalog_product->getproductimage($result[\'product_id\']);\r\n\r\n                        if(!empty(current($gethoverimage))){\r\n                            $hoverimage = $this->model_tool_image->resize(current($gethoverimage)[\'image\'], $setting[\'width\'], $setting[\'height\']);\r\n\r\n                            \r\n                        }else{\r\n                            $hoverimage = $image;\r\n                        }\r\n                ]]></add>\r\n        </operation>   \r\n        <operation>\r\n            <search><![CDATA[\'thumb\'       => $image,]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                                         \'hoverimage\'    => $hoverimage,\r\n                                          \'start_date\'      => $sdate,\r\n                        \'date_end\'      => $edate,\r\n                ]]></add>\r\n        </operation> \r\n        <operation>\r\n            <search><![CDATA[public function index($setting) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                protected function status(){\r\n                    return $this->Tvcmsthemevoltystatus->tabproductstatus();\r\n                }            \r\n            ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[if ($results) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                if($this->config->get(\'theme_default_directory\') == \"opc_electronic_electron_2501\"){\r\n                    $this->load->model(\'catalog/tvcmsmodule\');\r\n                    $status                                         = $this->status();\r\n                    $data[\'status_special_main_form\']               = $status[\'special_prod\'][\'main_form\'];\r\n                    $data[\'status_special_home_title\']              = $status[\'special_prod\'][\'home_title\'];\r\n                    $data[\'status_special_home_sub_title\']          = $status[\'special_prod\'][\'home_sub_title\'];\r\n                    $data[\'status_special_home_description\']        = $status[\'special_prod\'][\'home_description\'];\r\n                    $data[\'status_special_home_image\']              = $status[\'special_prod\'][\'home_image\'];\r\n                    $data[\'lang_id\']                                = $this->config->get(\'config_language_id\');\r\n                    if(!empty($data[\'status_special_main_form\'])){\r\n                        $name           = \"tvcmstabproducts\";\r\n                        $status_info    = $this->model_catalog_tvcmsmodule->getmoduelstatus($name);\r\n                        $data_info      = json_decode($status_info[\'setting\'],1);\r\n                        \r\n                        if(!empty($data[\'status_special_home_title\'])){\r\n                            $data[\'special_hometitle\'] = $data_info[\'tvcmstabproducts_pro_special\'][\'lang_text\'][$data[\'lang_id\']][\'hometitle\'];\r\n                        }\r\n                        if(!empty($data[\'status_special_home_sub_title\'])){\r\n                            $data[\'special_homesubtitle\'] = $data_info[\'tvcmstabproducts_pro_special\'][\'lang_text\'][$data[\'lang_id\']][\'homesubtitle\'];\r\n                        }\r\n                        if(!empty($data[\'status_special_home_description\'])){\r\n                            $data[\'special_homedes\'] = $data_info[\'tvcmstabproducts_pro_special\'][\'lang_text\'][$data[\'lang_id\']][\'homedes\'];\r\n                        }\r\n                        if(!empty($data[\'status_special_home_image\'])){\r\n                            $data[\'special_img\'] = $data_info[\'tvcmstabproducts_pro_new\'][\'lang_text\'][$data[\'lang_id\']][\'img\'];\r\n                        }   \r\n                    }\r\n                }\r\n                ]]></add>\r\n        </operation> \r\n    </file>\r\n    <file path=\"catalog/controller/common/home.php\">\r\n        <operation>\r\n            <search><![CDATA[$data[\'header\'] = $this->load->controller(\'common/header\');]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                $data[\'footer_top\'] = $this->load->controller(\'common/footer_top\');\r\n            ]]></add>\r\n        </operation> \r\n    </file>\r\n    <file path=\"catalog/controller/extension/module/category.php\">\r\n        <operation>\r\n            <search><![CDATA[$categories = $this->model_catalog_category->getCategories(0);]]></search>\r\n            <add position=\"after\"><![CDATA[foreach ($categories as $tvcmscategory) {\r\n            $tvcmschildren_data = array();\r\n            $children = $this->model_catalog_category->getCategories($tvcmscategory[\'category_id\']);\r\n            foreach($children as $child) {\r\n                $filter_data = array(\'filter_category_id\' => $child[\'category_id\'], \'filter_sub_category\' => true);\r\n                $tvcmschildren_data[] = array(\r\n                    \'category_id\' => $child[\'category_id\'],\r\n                    \'name\' => $child[\'name\'] . ($this->config->get(\'config_product_count\') ? \' (\' . $this->model_catalog_product->getTotalProducts($filter_data) . \')\' : \'\'),\r\n                    \'href\' => $this->url->link(\'product/category\', \'path=\' . $tvcmscategory[\'category_id\'] . \'_\' . $child[\'category_id\'])\r\n                );\r\n            }\r\n            $filter_data = array(\r\n                \'filter_category_id\'  => $tvcmscategory[\'category_id\'],\r\n                \'filter_sub_category\' => true\r\n            );\r\n            $data[\'tvmcscategories\'][] = array(\r\n                \'category_id\' => $tvcmscategory[\'category_id\'],\r\n                \'name\'        => $tvcmscategory[\'name\'] . ($this->config->get(\'config_product_count\') ? \' (\' . $this->model_catalog_product->getTotalProducts($filter_data) . \')\' : \'\'),\r\n                \'children\'    => $tvcmschildren_data,\r\n                \'href\'        => $this->url->link(\'product/category\', \'path=\' . $tvcmscategory[\'category_id\'])\r\n            );\r\n        }]]></add>\r\n        </operation> \r\n             \r\n    </file>\r\n   <file path=\"catalog/controller/account/download.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/account/order.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/account/recurring.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/account/return.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/account/reward.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/account/transaction.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/extension/credit_card/sagepay_direct.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/extension/credit_card/sagepay_server.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/product/category.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/product/manufacturer.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/product/product.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/product/search.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path=\"catalog/controller/product/special.php\">\r\n        <operation>\r\n            <search><![CDATA[new Pagination();]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                new Tvcmspagination();\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n   \r\n    <file path=\"system/library/response.php\">\r\n        <operation>\r\n            <search><![CDATA[if ($this->output) {]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                //speedup\r\n                    if(!defined(\'HTTP_CATALOG\')){\r\n                        $config = $GLOBALS[\'registry\']->get(\'config\');\r\n                        //echo \"<pre>\"; print_r($config->get(\'tvcmscustomsetting_configuration\')[\'htmlminify\']); echo \"</pre>\"; die;\r\n                        $status = $config->get(\'tvcmscustomsetting_configuration\')[\'htmlminify\'];\r\n                        if(!empty($status)){\r\n                            $this->output = minify($this->output);\r\n                        }\r\n                    }\r\n                //speedup\r\n            ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[echo $output;]]></search>\r\n            <add position=\"after\" offset=\"3\"><![CDATA[\r\n                function minify($output = \'\'){\r\n                    if(isset($GLOBALS[\'registry\']) && !$GLOBALS[\'registry\']->get(\'speedup\')){\r\n                        $speedup = new tvcmsminify($GLOBALS[\'registry\']); \r\n                        $GLOBALS[\'registry\']->set(\'speedup\', $speedup);\r\n                    }\r\n                    if($GLOBALS[\'registry\']->get(\'speedup\'))\r\n                        return $GLOBALS[\'registry\']->get(\'speedup\')->minify_html($output);\r\n                    return $output;\r\n                }\r\n            ]]></add>\r\n        </operation> \r\n    </file>\r\n\r\n</modification>', 1, '2019-02-16 16:34:02');
INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(3, 2, '<font color=\"#263238\"><b>Twig Debug</b></font>', 'twig-debug', '<font color=\"#263238\"><b>Trydalcoholic</b></font>', '1.0', '//goo.gl/pSwwUR', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n	<name><![CDATA[<font color=\"#263238\"><b>Twig Debug</b></font>]]></name>\n	<code>twig-debug</code>\n	<version>1.0</version>\n	<author><![CDATA[<font color=\"#263238\"><b>Trydalcoholic</b></font>]]></author>\n	<link>//goo.gl/pSwwUR</link>\n	<file path=\"system/library/template/twig.php\">\n		<operation>\n			<search><![CDATA[$config = array(\'autoescape\' => false);]]></search>\n			<add position=\"replace\">\n				<![CDATA[\n				$config = array(\n					\'autoescape\'	=> false,\n					\'debug\'			=> true,\n				);\n				]]>\n			</add>\n		</operation>\n		<operation>\n			<search><![CDATA[$this->twig = new \\Twig_Environment($loader, $config);]]></search>\n			<add position=\"after\">\n				<![CDATA[$this->twig->addExtension(new \\Twig_Extension_Debug());]]>\n			</add>\n		</operation>\n	</file>\n</modification>', 1, '2020-05-19 15:30:00');

-- --------------------------------------------------------

--
-- Structure de la table `oc_module`
--

DROP TABLE IF EXISTS `oc_module`;
CREATE TABLE IF NOT EXISTS `oc_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(28, 'Home Page', 'featured', '{\"name\":\"Home Page\",\"product_name\":\"\",\"product\":[\"43\",\"42\"],\"limit\":\"4\",\"width\":\"270\",\"height\":\"313\",\"status\":\"1\"}'),
(80, 'Brand List', 'tvcmsbrandlist', '{\"status\":\"1\",\"name\":\"Brand List\"}'),
(86, 'Left Right Product', 'tvcmsleftproduct', '{\"name\":\"Left Right Product\",\"status\":\"1\",\"left_feature_title\":\"Feature Products\",\"status_left_feature\":\"0\",\"left_new_title\":\"New Products\",\"status_left_new\":\"0\",\"left_best_title\":\"Ny be mpividy\",\"status_left_best\":\"1\",\"left_special_title\":\"Special Products\",\"status_left_special\":\"0\",\"right_feature_title\":\"Feature Products\",\"status_right_feature\":\"0\",\"right_new_title\":\"New Products\",\"status_right_new\":\"0\",\"right_best_title\":\"Best Products\",\"status_right_best\":\"0\",\"right_special_title\":\"Special Products\",\"status_right_special\":\"0\"}'),
(74, 'Category Slider', 'tvcmscategoryslider', '{\"name\":\"Category Slider\",\"status\":\"1\",\"tvcmscategoryslider_main\":{\"2\":{\"tvcmscategoryslider_main_cat\":\"Nos Categories\"},\"1\":{\"tvcmscategoryslider_main_cat\":\"Our Category\"},\"3\":{\"tvcmscategoryslider_main_cat\":\"Ny kataloginay\"}}}'),
(48, 'bestproduct', 'bestseller', '{\"name\":\"bestproduct\",\"limit\":\"15\",\"width\":\"270\",\"height\":\"313\",\"status\":\"1\"}'),
(53, 'home', 'latest', '{\"name\":\"home\",\"limit\":\"5\",\"width\":\"270\",\"height\":\"313\",\"status\":\"1\"}'),
(65, 'Tab Product', 'tvcmstabproducts', '{\"name\":\"Tab Product\",\"status\":\"1\",\"tvcmstabproducts_pro_fea\":{\"status\":\"1\",\"no_pro\":\"11\",\"tab_name_id\":\"ttvcmstab-featured-product\",\"tab_name_class_slider\":\"ttvtab-featured-product\",\"tab_name_class_pagination\":\"ttvtab-featured\",\"cate_pro\":[\"25\",\"20\",\"18\"],\"dis_rand\":\"1\",\"dis_tap\":\"1\",\"bannerside\":\"left\",\"bannerstatus\":\"0\",\"lang_text\":{\"2\":{\"tabtitle\":\"Featured\",\"hometitle\":\"Featured \"},\"1\":{\"tabtitle\":\"Featured\",\"hometitle\":\"Featured \"},\"3\":{\"tabtitle\":\"Singanina\",\"hometitle\":\"Singanina\"}}},\"product_name\":\"\",\"tvcmstabproducts_pro_new\":{\"status\":\"1\",\"no_pro\":\"11\",\"tab_name_id\":\"ttvcmstab-new-product\",\"tab_name_class_slider\":\"ttvtab-new-product\",\"tab_name_class_pagination\":\"ttvtab-new\",\"cate_pro\":\"4\",\"dis_tap\":\"1\",\"bannerside\":\"left\",\"bannerstatus\":\"0\",\"lang_text\":{\"2\":{\"tabtitle\":\"New Product\",\"hometitle\":\"New Product\"},\"1\":{\"tabtitle\":\"New Product\",\"hometitle\":\"New Product\"},\"3\":{\"tabtitle\":\"Entana vaovao\",\"hometitle\":\"Entana vaovao\"}}},\"tvcmstabproducts_pro_best\":{\"status\":\"0\",\"no_pro\":\"11\",\"tab_name_id\":\"ttvcmstab-best-seller-product\",\"tab_name_class_slider\":\"ttvtab-best-seller-product\",\"tab_name_class_pagination\":\"ttvtab-best-seller\",\"dis_tap\":\"1\",\"bannerside\":\"left\",\"bannerstatus\":\"0\",\"lang_text\":{\"2\":{\"tabtitle\":\"Best Seller\",\"hometitle\":\"Best Seller\"},\"1\":{\"tabtitle\":\"Best Seller\",\"hometitle\":\"Best Seller\"},\"3\":{\"tabtitle\":\"Be mpividy\",\"hometitle\":\"Be mpividy\"}}},\"tvcmstabproducts_pro_spe\":{\"status\":\"0\",\"no_pro\":\"11\",\"tab_name_id\":\"ttvcmstab-special-product\",\"tab_name_class_slider\":\"ttvtab-special-product\",\"tab_name_class_pagination\":\"ttvtab-special\",\"dis_tap\":\"1\",\"bannerside\":\"left\",\"bannerstatus\":\"1\",\"lang_text\":{\"2\":{\"tabtitle\":\"special\",\"hometitle\":\"Special Trend \",\"img\":\"catalog\\/themevolty\\/tabproducts\\/Special_Trend_Banner.jpg\"},\"1\":{\"tabtitle\":\"special\",\"hometitle\":\"Special Trend \",\"img\":\"catalog\\/themevolty\\/tabproducts\\/Special_Trend_Banner.jpg\"},\"3\":{\"tabtitle\":\"Fiainambidy\",\"hometitle\":\"Fiainambidy\",\"img\":\"\"}}},\"tvcmstabproducts_pro_cus\":{\"status\":\"1\",\"tab_name_id\":\"ttvcmstab-special-product\",\"tab_name_class_slider\":\"ttvtab-special-product\",\"tab_name_class_pagination\":\"ttvtab-special\",\"bannerside\":\"left\",\"bannerstatus\":\"0\",\"lang_text\":{\"2\":{\"maintitle\":\"\"},\"1\":{\"maintitle\":\"\"},\"3\":{\"maintitle\":\"\"}}}}'),
(123, 'Flash', 'responsive_slideshow', '{\"name\":\"Flash\",\"banner_id\":\"10\",\"width\":\"250\",\"height\":\"350\",\"effect\":\"book\",\"duration\":\"20\",\"duration1\":\"100\",\"delay\":\"20\",\"delay1\":\"100\",\"autoplay\":\"1\",\"playpause\":\"1\",\"stoponhover\":\"1\",\"loop\":\"0\",\"caption\":\"1\",\"controls\":\"1\",\"controlsthumb\":\"1\",\"status\":\"1\"}'),
(122, 'Information', 'responsive_slideshow', '{\"name\":\"Information\",\"banner_id\":\"9\",\"width\":\"250\",\"height\":\"200\",\"effect\":\"slices\",\"duration\":\"20\",\"duration1\":\"100\",\"delay\":\"20\",\"delay1\":\"100\",\"autoplay\":\"1\",\"playpause\":\"1\",\"stoponhover\":\"1\",\"loop\":\"0\",\"caption\":\"1\",\"controls\":\"1\",\"controlsthumb\":\"1\",\"status\":\"1\"}'),
(104, 'Category Product', 'tvcmscategoryproduct', '{\"name\":\"Category Product\",\"status\":\"1\",\"tvcmscategoryproduct_main\":{\"2\":{\"maintitle\":\"Nos Cat\\u00e9gories de Produits\"},\"1\":{\"maintitle\":\"Our Categories Product\"},\"3\":{\"maintitle\":\" Ireo sokajy vokarintsika\"}}}'),
(82, 'Footer Logo', 'tvcmsfooterlogo', '{\"name\":\"Footer Logo\",\"status\":\"1\",\"tvcmsfooterlogo_des\":{\"2\":{\"main_img\":\"catalog\\/Capture.PNG\",\"main_short_des\":\"\"},\"1\":{\"main_img\":\"catalog\\/Capture.PNG\",\"main_short_des\":\"\"},\"3\":{\"main_img\":\"catalog\\/Capture.PNG\",\"main_short_des\":\"\"}}}'),
(116, 'Animation', 'responsive_slideshow', '{\"name\":\"Animation\",\"banner_id\":\"7\",\"width\":\"900\",\"height\":\"300\",\"effect\":\"kenburns\",\"duration\":\"20\",\"duration1\":\"100\",\"delay\":\"20\",\"delay1\":\"100\",\"autoplay\":\"1\",\"playpause\":\"1\",\"stoponhover\":\"1\",\"loop\":\"0\",\"caption\":\"1\",\"controls\":\"1\",\"controlsthumb\":\"1\",\"status\":\"1\"}'),
(117, 'Promotion', 'responsive_slideshow', '{\"name\":\"Promotion\",\"banner_id\":\"6\",\"width\":\"180\",\"height\":\"280\",\"effect\":\"louvers\",\"duration\":\"20\",\"duration1\":\"100\",\"delay\":\"20\",\"delay1\":\"100\",\"autoplay\":\"1\",\"playpause\":\"1\",\"stoponhover\":\"1\",\"loop\":\"0\",\"caption\":\"1\",\"controls\":\"1\",\"controlsthumb\":\"1\",\"status\":\"1\"}'),
(85, 'Payment Icon', 'tvcmspaymenticon', '{\"name\":\"Payment Icon\",\"status\":1,\"tvcmspaymenticon_main\":{\"1\":{\"main_img\":\"catalog\\/themevolty\\/paymenticon\\/demo_main_img.jpg\",\"maintitle\":\"Main Title\",\"main_short_des\":\"Short Description\",\"main_des\":\"Description\"},\"2\":{\"main_img\":\"catalog\\/themevolty\\/paymenticon\\/demo_main_img.jpg\",\"maintitle\":\"Main Title\",\"main_short_des\":\"Short Description\",\"main_des\":\"Description\"}}}'),
(93, 'Social Icon', 'tvcmssocialicon', '{\"name\":\"Social Icon\",\"status\":1,\"tvcmssocialicon_main\":{\"1\":{\"main_img\":\"catalog\\/themevolty\\/socialicon\\/demo_img_1.jpg\",\"maintitle\":\"Visit Our Social Media :\",\"main_short_des\":\"Sub Descriptio\",\"main_des\":\"Description\"},\"2\":{\"main_img\":\"catalog\\/themevolty\\/socialicon\\/demo_img_1.jpg\",\"maintitle\":\"Visit Our Social Media :\",\"main_short_des\":\"Sub Descriptio\",\"main_des\":\"Description\"}}}');

-- --------------------------------------------------------

--
-- Structure de la table `oc_option`
--

DROP TABLE IF EXISTS `oc_option`;
CREATE TABLE IF NOT EXISTS `oc_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(14, 'select', 1),
(12, 'date', 11),
(13, 'select', 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_option_description`
--

DROP TABLE IF EXISTS `oc_option_description`;
CREATE TABLE IF NOT EXISTS `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`option_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(14, 2, 'Saveur'),
(1, 2, 'Radio'),
(2, 2, 'Checkbox'),
(4, 2, 'Text'),
(6, 2, 'Textarea'),
(8, 2, 'Date'),
(7, 2, 'File'),
(9, 2, 'Time'),
(10, 2, 'Date &amp; Time'),
(12, 2, 'Delivery Date'),
(14, 3, 'Tsirony'),
(13, 1, 'Color'),
(13, 2, 'Color'),
(1, 3, 'Radio'),
(2, 3, 'Checkbox'),
(4, 3, 'Text'),
(6, 3, 'Textarea'),
(8, 3, 'Date'),
(7, 3, 'File'),
(9, 3, 'Time'),
(10, 3, 'Date &amp; Time'),
(12, 3, 'Delivery Date'),
(14, 1, 'Savour'),
(13, 3, 'Color');

-- --------------------------------------------------------

--
-- Structure de la table `oc_option_value`
--

DROP TABLE IF EXISTS `oc_option_value`;
CREATE TABLE IF NOT EXISTS `oc_option_value` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_value_id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(59, 14, '', 4),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(58, 14, '', 3),
(57, 14, '', 2),
(56, 14, '', 1),
(49, 13, '', 0),
(50, 13, '', 0),
(51, 13, '', 0),
(52, 13, '', 0),
(53, 13, '', 0),
(54, 13, '', 0),
(55, 13, '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_option_value_description`
--

DROP TABLE IF EXISTS `oc_option_value_description`;
CREATE TABLE IF NOT EXISTS `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`option_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(58, 1, 14, 'Curry'),
(58, 2, 14, 'Curry'),
(57, 3, 14, 'Legioma'),
(43, 2, 1, 'Large'),
(32, 2, 1, 'Small'),
(45, 2, 2, 'Checkbox 4'),
(44, 2, 2, 'Checkbox 3'),
(31, 2, 1, 'Medium'),
(59, 3, 14, ' Makamba'),
(23, 2, 2, 'Checkbox 1'),
(24, 2, 2, 'Checkbox 2'),
(57, 1, 14, 'Vegetable'),
(57, 2, 14, 'Legume'),
(49, 1, 13, 'black'),
(49, 2, 13, 'black'),
(50, 1, 13, 'blue'),
(50, 2, 13, 'blue'),
(51, 1, 13, 'brown'),
(51, 2, 13, 'brown'),
(52, 1, 13, 'darkslategray'),
(52, 2, 13, 'darkslategray'),
(53, 1, 13, 'gray'),
(53, 2, 13, 'gray'),
(54, 1, 13, 'green'),
(54, 2, 13, 'green'),
(55, 1, 13, 'orange'),
(55, 2, 13, 'orange'),
(43, 3, 1, 'Large'),
(32, 3, 1, 'Small'),
(45, 3, 2, 'Checkbox 4'),
(44, 3, 2, 'Checkbox 3'),
(31, 3, 1, 'Medium'),
(59, 1, 14, ' Shrimp'),
(59, 2, 14, 'Crevette'),
(58, 3, 14, 'Curry'),
(23, 3, 2, 'Checkbox 1'),
(24, 3, 2, 'Checkbox 2'),
(56, 3, 14, 'Akoho'),
(56, 1, 14, 'Chicken'),
(56, 2, 14, 'Poulet'),
(49, 3, 13, 'black'),
(50, 3, 13, 'blue'),
(51, 3, 13, 'brown'),
(52, 3, 13, 'darkslategray'),
(53, 3, 13, 'gray'),
(54, 3, 13, 'green'),
(55, 3, 13, 'orange');

-- --------------------------------------------------------

--
-- Structure de la table `oc_order`
--

DROP TABLE IF EXISTS `oc_order`;
CREATE TABLE IF NOT EXISTS `oc_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_order`
--

INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(5, 0, 'INV-2020-00', 0, 'Tsena B.com', 'http://localhost/tsenabe/upload/', 8, 1, 'Saro', 'RAMANANTOANINA', 'rakotonarivosandratr@yahoo.fr', '0332848333', '', '', 'Saro', 'RAMANANTOANINA', '', 'Anosy', '', 'Antananarivo', '', 'Madagascar', 127, 'Antananarivo', 1938, '', '[]', 'Cash On Delivery', 'cod', 'Saro', 'RAMANANTOANINA', '', 'Anosy', '', 'Antananarivo', '', 'Madagascar', 127, 'Antananarivo', 1938, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '32000.0000', 1, 0, '0.0000', 0, '', 1, 1, 'MGA', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36', 'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7', '2020-05-18 10:18:06', '2020-05-18 10:18:15');

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_history`
--

DROP TABLE IF EXISTS `oc_order_history`;
CREATE TABLE IF NOT EXISTS `oc_order_history` (
  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_order_history`
--

INSERT INTO `oc_order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(14, 5, 1, 0, '', '2020-05-18 10:18:15'),
(13, 5, 1, 0, '', '2020-05-18 10:18:12');

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_option`
--

DROP TABLE IF EXISTS `oc_order_option`;
CREATE TABLE IF NOT EXISTS `oc_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_product`
--

DROP TABLE IF EXISTS `oc_order_product`;
CREATE TABLE IF NOT EXISTS `oc_order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL,
  PRIMARY KEY (`order_product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_order_product`
--

INSERT INTO `oc_order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(5, 5, 79, 'Ambré de Nosy Be Prestige 52°', 'RHM-001', 1, '30000.0000', '30000.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_recurring`
--

DROP TABLE IF EXISTS `oc_order_recurring`;
CREATE TABLE IF NOT EXISTS `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_recurring_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_recurring_transaction`
--

DROP TABLE IF EXISTS `oc_order_recurring_transaction`;
CREATE TABLE IF NOT EXISTS `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_recurring_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_shipment`
--

DROP TABLE IF EXISTS `oc_order_shipment`;
CREATE TABLE IF NOT EXISTS `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`order_shipment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_status`
--

DROP TABLE IF EXISTS `oc_order_status`;
CREATE TABLE IF NOT EXISTS `oc_order_status` (
  `order_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`order_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(1, 3, 'Miandry'),
(1, 1, 'Waiting'),
(1, 2, 'En attente'),
(2, 2, 'Preparer'),
(2, 1, 'To prepare'),
(2, 3, 'Hampiomanana'),
(3, 2, 'Valider'),
(3, 1, 'Validate'),
(3, 3, 'Ekena'),
(4, 2, 'Livrer'),
(4, 1, 'Deliver'),
(4, 3, 'Aterina'),
(5, 2, 'Terminer'),
(5, 1, 'Finish'),
(5, 3, 'Vita'),
(6, 2, 'Annulé'),
(6, 1, 'Canceled'),
(6, 3, 'Hanafoana');

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_total`
--

DROP TABLE IF EXISTS `oc_order_total`;
CREATE TABLE IF NOT EXISTS `oc_order_total` (
  `order_total_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`order_total_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_order_total`
--

INSERT INTO `oc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(15, 5, 'total', 'Total', '32000.0000', 9),
(14, 5, 'shipping', 'Flat Shipping Rate', '2000.0000', 3),
(13, 5, 'sub_total', 'Sub-Total', '30000.0000', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_order_voucher`
--

DROP TABLE IF EXISTS `oc_order_voucher`;
CREATE TABLE IF NOT EXISTS `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  PRIMARY KEY (`order_voucher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_product`
--

DROP TABLE IF EXISTS `oc_product`;
CREATE TABLE IF NOT EXISTS `oc_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(28, 'EAU-001', '', '', '', '', '', '', '', 6, 7, 'catalog/demo/product/SAINTO1L5.png', 12, 1, '2000.0000', 200, 0, '2018-03-15', '0.15000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 47, '2009-02-03 16:06:50', '2020-05-14 10:55:00'),
(71, 'BG-008', '', '', '', '', '', '', '', 10, 6, '', 15, 1, '1200.0000', 0, 0, '2020-05-13', '0.00400000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 10:20:35', '2020-05-14 10:48:04'),
(72, 'BG-007', '', '', '', '', '', '', '', 8, 6, 'catalog/demo/product/big/big orange 2l.jpg', 15, 1, '3300.0000', 0, 0, '2020-05-13', '0.02000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 11, '2020-05-13 10:21:54', '2020-05-14 10:46:37'),
(73, 'BG-006', '', '', '', '', '', '', '', 10, 6, 'catalog/demo/product/big/big cola 2l.jpg', 15, 1, '3300.0000', 0, 0, '2020-05-13', '0.04000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 26, '2020-05-13 10:23:23', '2020-05-19 15:17:04'),
(70, 'EAU-002', '', '', '', '', '', '', '', 8, 6, 'catalog/demo/product/SAINTO.png', 12, 1, '1000.0000', 0, 0, '2020-05-13', '0.05000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 10:17:18', '2020-05-14 10:55:30'),
(42, 'RHM-020', '', '', '', '', '', '', '', 6, 7, 'catalog/demo/product/MAKI.png', 8, 1, '32000.0000', 400, 0, '2018-03-15', '0.07000000', 1, '1.00000000', '2.00000000', '3.00000000', 1, 1, 1, 0, 1, 25, '2009-02-03 21:07:37', '2020-05-14 10:52:48'),
(49, 'EPC-003', '', '', '', '', '', '', '', 1000, 6, '', 14, 1, '200.0000', 0, 0, '2020-05-13', '0.02000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, '2020-05-13 09:23:29', '2020-05-14 10:51:20'),
(51, '100', '', '', '', '', '', '', '', 40, 6, '', 0, 1, '400.0000', 0, 0, '2020-05-13', '0.15000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, '2020-05-13 09:30:15', '2020-05-13 09:30:51'),
(52, 'EPC-001', '', '', '', '', '', '', '', 40, 6, '', 14, 1, '500.0000', 0, 0, '2020-05-13', '0.15000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:32:21', '2020-05-14 10:50:05'),
(48, 'PAT-001', '', '', '', '', '', '', '', 46, 7, 'catalog/demo/product/NOUILLE MATSIRO LEGUME.png', 9, 1, '700.0000', 0, 0, '2018-03-15', '0.08500000', 0, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 113, '2009-02-08 17:21:51', '2020-05-22 14:27:17'),
(53, '104', '', '', '', '', '', '', '', 12, 6, '', 0, 1, '2200.0000', 0, 0, '2020-05-13', '1.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:34:15', '2020-05-13 09:35:06'),
(54, '106', '', '', '', '', '', '', '', 40, 6, '', 0, 1, '2000.0000', 0, 0, '2020-05-13', '0.05000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:36:33', '2020-05-13 09:39:09'),
(55, '108', '', '', '', '', '', '', '', 20, 6, '', 0, 1, '4000.0000', 0, 0, '2020-05-13', '0.10000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:38:24', '2020-05-13 09:38:24'),
(56, '109', '', '', '', '', '', '', '', 40, 6, 'catalog/demo/product/savour/sucre roux 200g.jpg', 14, 1, '7000.0000', 0, 0, '2020-05-13', '0.20000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:40:34', '2020-05-13 19:53:29'),
(57, 'VNG-006', '', '', '', '', '', '', '', 40, 6, '', 0, 1, '200.0000', 0, 0, '2020-05-13', '0.05000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:43:03', '2020-05-14 11:05:43'),
(58, '111', '', '', '', '', '', '', '', 8, 6, '', 0, 1, '500.0000', 0, 0, '2020-05-13', '0.00250000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:44:54', '2020-05-13 09:53:19'),
(59, '112', '', '', '', '', '', '', '', 8, 6, '', 0, 1, '400.0000', 0, 0, '2020-05-13', '0.00250000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:46:38', '2020-05-13 09:46:38'),
(60, '113', '', '', '', '', '', '', '', 6, 6, '', 0, 1, '1000.0000', 0, 0, '2020-05-13', '0.01000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:48:35', '2020-05-13 09:48:35'),
(61, '114', '', '', '', '', '', '', '', 6, 6, '', 0, 1, '1500.0000', 0, 0, '2020-05-13', '0.01000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:51:34', '2020-05-13 09:51:34'),
(62, '115', '', '', '', '', '', '', '', 6, 6, '', 0, 1, '800.0000', 0, 0, '2020-05-13', '0.01000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:55:55', '2020-05-13 09:55:55'),
(63, '115', '', '', '', '', '', '', '', 120, 6, '', 0, 1, '400.0000', 0, 0, '2020-05-13', '0.00300000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 09:58:32', '2020-05-13 09:58:32'),
(64, '116', '', '', '', '', '', '', '', 72, 6, 'catalog/demo/product/NOSY KELLY.png', 13, 1, '800.0000', 0, 0, '2020-05-13', '0.00800000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 10:00:10', '2020-05-13 19:52:41'),
(65, 'SAV-003', '', '', '', '', '', '', '', 10, 6, 'catalog/demo/product/savonnerie tropicale/savony citron 380g.jpg', 13, 1, '3100.0000', 0, 0, '2020-05-13', '0.38000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 10:02:07', '2020-05-14 10:56:18'),
(66, 'SAV-002', '', '', '', '', '', '', '', 12, 6, 'catalog/demo/product/SAVONY CITRON .png', 0, 1, '3500.0000', 0, 0, '2020-05-13', '0.78000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 10:04:04', '2020-05-14 10:57:03'),
(67, 'SAV-001', '', '', '', '', '', '', '', 18, 6, '', 13, 1, '1800.0000', 0, 0, '2020-05-13', '0.01200000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 10:06:15', '2020-05-14 11:03:29'),
(68, '120', '', '', '', '', '', '', '', 100, 6, '', 0, 1, '500.0000', 0, 0, '2020-05-13', '0.20000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 10:07:32', '2020-05-13 10:07:32'),
(69, '121', '', '', '', '', '', '', '', 20, 6, 'catalog/demo/product/CAFE 30G.png', 9, 1, '700.0000', 0, 0, '2020-05-13', '0.00300000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-13 10:08:54', '2020-05-13 19:49:35'),
(74, 'VN-001', '', '', '', '', '', '', '', 6, 6, '', 8, 1, '3300.0000', 0, 0, '2020-05-14', '0.07500000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-14 10:22:46', '2020-05-14 10:22:46'),
(75, 'VN-002', '', '', '', '', '', '', '', 6, 6, '', 8, 1, '35000.0000', 0, 0, '2020-05-14', '0.07500000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-14 10:26:05', '2020-05-14 10:26:05'),
(76, 'VN-003', '', '', '', '', '', '', '', 6, 6, '', 8, 1, '40000.0000', 0, 0, '2020-05-14', '0.07500000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-14 10:27:52', '2020-05-14 10:27:52'),
(77, 'VN-004', '', '', '', '', '', '', '', 12, 6, 'catalog/demo/product/dzama/vin mahitasoa piloboka.jpg', 8, 1, '30000.0000', 0, 0, '2020-05-14', '0.07500000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-14 10:30:29', '2020-05-14 10:30:29'),
(78, 'VN-005', '', '', '', '', '', '', '', 12, 6, 'catalog/demo/product/dzama/vin piloboka rouge rubis.jpg', 8, 1, '25000.0000', 0, 0, '2020-05-14', '0.07500000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 2, '2020-05-14 10:32:45', '2020-05-14 10:32:45'),
(79, 'RHM-001', '', '', '', '', '', '', '', 5, 6, 'catalog/demo/product/dzama/Ambré de Nosy Be Prestige 52°.jpg', 8, 1, '30000.0000', 0, 0, '2020-05-14', '0.07000000', 0, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 4, '2020-05-14 10:35:30', '2020-05-22 13:48:43'),
(80, 'RHM-002', '', '', '', '', '', '', '', 6, 6, 'catalog/demo/product/dzama/Blanc de Nosy Be Prestige 42°.jpg', 8, 1, '35000.0000', 0, 0, '2020-05-14', '0.07000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-14 10:37:25', '2020-05-14 10:37:25'),
(81, 'RHM-003', '', '', '', '', '', '', '', 12, 6, 'catalog/demo/product/dzama/Coco Punch 18°.jpg', 8, 1, '40000.0000', 0, 0, '2020-05-14', '0.07000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-14 10:39:59', '2020-05-14 10:39:59'),
(82, 'RHM-004', '', '', '', '', '', '', '', 6, 6, 'catalog/demo/product/dzama/Couleur Café 18° 2.jpg', 8, 1, '32000.0000', 0, 0, '2020-05-14', '0.07000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-14 10:43:00', '2020-05-14 10:43:00'),
(83, 'RHM-005', '', '', '', '', '', '', '', 12, 6, 'catalog/demo/product/dzama/Cuvée blanche 40°.jpg', 8, 1, '40000.0000', 0, 0, '2020-05-14', '0.07000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2020-05-14 10:45:10', '2020-05-14 10:45:10');

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_attribute`
--

DROP TABLE IF EXISTS `oc_product_attribute`;
CREATE TABLE IF NOT EXISTS `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`product_id`,`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_description`
--

DROP TABLE IF EXISTS `oc_product_description`;
CREATE TABLE IF NOT EXISTS `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `astuce` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `astuce`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(28, 1, 'SAINTO GM', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;ul&gt;\r\n&lt;/ul&gt;\r\n', '', '', 'SAINTO GM', '', ''),
(28, 3, 'SAINTO GM', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;ul&gt;\r\n&lt;/ul&gt;\r\n', '', '', 'SAINTO GM', '', ''),
(70, 1, 'SAINTO PM', '', '', '', 'SAINTO PM', '', ''),
(70, 3, 'SAINTO PM', '', '', '', 'SAINTO PM', '', ''),
(71, 1, 'BIG Orange PM', '', '', '', 'BIG Orange PM', '', ''),
(71, 3, 'BIG Orange PM', '', '', '', 'BIG Orange PM', '', ''),
(72, 1, 'BIG Orange GM', '', '', '', 'BIG Orange GM', '', ''),
(72, 3, 'BIG Orange GM', '', '', '', 'BIG Orange GM', '', ''),
(73, 2, 'BIG Cola PM', '', '', '', 'BIG Cola PM', '', ''),
(79, 1, 'Ambré de Nosy Be Prestige 52°', '', '', '', 'Ambré de Nosy Be Prestige 52°', '', ''),
(79, 3, 'Ambré de Nosy Be Prestige 52°', '', '', '', 'Ambré de Nosy Be Prestige 52°', '', ''),
(80, 2, 'Blanc de Nosy Be Prestige 42°', '', '', '', 'Blanc de Nosy Be Prestige 42°', '', ''),
(80, 1, 'Blanc de Nosy Be Prestige 42°', '', '', '', 'Blanc de Nosy Be Prestige 42°', '', ''),
(80, 3, 'Blanc de Nosy Be Prestige 42°', '', '', '', 'Blanc de Nosy Be Prestige 42°', '', ''),
(81, 2, 'Coco Punch 18°', '', '', '', 'Coco Punch 18°', '', ''),
(81, 1, 'Coco Punch 18°', '', '', '', 'Coco Punch 18°', '', ''),
(81, 3, 'Coco Punch 18°', '', '', '', 'Coco Punch 18°', '', ''),
(82, 2, 'Couleur Café 18°', '', '', '', 'Couleur Café 18°', '', ''),
(82, 1, 'Couleur Café 18°', '', '', '', 'Couleur Café 18°', '', ''),
(82, 3, 'Couleur Café 18°', '', '', '', 'Couleur Café 18°', '', ''),
(83, 2, 'Cuvée blanche 40°', '', '', '', 'Cuvée blanche 40°', '', ''),
(83, 1, 'Cuvée blanche 40°', '', '', '', 'Cuvée blanche 40°', '', ''),
(83, 3, 'Cuvée blanche 40°', '', '', '', 'Cuvée blanche 40°', '', ''),
(74, 2, 'VIN RN7 Blanc', '', '', '', 'VIN RN7 Blanc', '', ''),
(74, 1, 'VIN RN7 Blanc', '', '', '', 'VIN RN7 Blanc', '', ''),
(74, 3, 'VIN RN7 Blanc', '', '', '', 'VIN RN7 Blanc', '', ''),
(75, 2, 'VIN RN7 Rosé', '', '', '', 'VIN RN7 Rosé', '', ''),
(75, 1, 'VIN RN7 Rosé', '', '', '', 'VIN RN7 Rosé', '', ''),
(75, 3, 'VIN RN7 Rosé', '', '', '', 'VIN RN7 Rosé', '', ''),
(76, 2, 'VIN RN7 Rouge', '', '', '', 'VIN RN7 Rouge', '', ''),
(76, 1, 'VIN RN7 Rouge', '', '', '', 'VIN RN7 Rouge', '', ''),
(76, 3, 'VIN RN7 Rouge', '', '', '', 'VIN RN7 Rouge', '', ''),
(77, 2, 'Vin Mahitasoa Piloboka', '', '', '', 'Vin Mahitasoa Piloboka', '', ''),
(77, 1, 'Vin Mahitasoa Piloboka', '', '', '', 'Vin Mahitasoa Piloboka', '', ''),
(77, 3, 'Vin Mahitasoa Piloboka', '', '', '', 'Vin Mahitasoa Piloboka', '', ''),
(78, 2, 'Vin Mahitasoa Rouge Rubis', '', '', '', 'Vin Mahitasoa Rouge Rubis', '', ''),
(78, 1, 'Vin Mahitasoa Rouge Rubis', '', '', '', 'Vin Mahitasoa Rouge Rubis', '', ''),
(78, 3, 'Vin Mahitasoa Rouge Rubis', '', '', '', 'Vin Mahitasoa Rouge Rubis', '', ''),
(42, 1, 'Maki Ambré avec raphia 37,5° ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', 'Maki Ambré avec raphia 37,5° ', '', ''),
(42, 3, 'Maki Ambré avec raphia 37,5° ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', 'Maki Ambré avec raphia 37,5° ', '', ''),
(48, 1, 'Pâte instantanée MATSIRO', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n  &lt;div&gt;\r\n    \r\n   &lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;A Madagascar, les pâtes Matsiro Salone sont une référence dans la gamme des nouilles instantanées.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;Elles sont aussi délicieuses que simples d’utilisation.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;Idéales pour un repas rapide, mais savoureux, ou pour un gouter chaud et consistant.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;Pour la préparation, il suffit de verser le contenu dans un bol et d’y ajouter de l’eau bouillante et recouvrir le tout durant quelques minutes.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;Vous pouvez composer votre&amp;nbsp;plat de nouilles Matsiro Salone&amp;nbsp;selon vos gouts.&lt;/span&gt;&lt;/p&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n  &lt;div&gt;\r\n     &lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/rwHtbnTVae0&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture&quot; allowfullscreen=&quot;&quot;&gt;&lt;/iframe&gt;\r\n   \r\n  &lt;/div&gt;\r\n&lt;/div&gt;', '', 'Pâte instantanée MATSIRO', '', ''),
(48, 3, 'Pâte instantanée MATSIRO', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n  &lt;div&gt;\r\n   &lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;A Madagascar, les pâtes Matsiro Salone sont une référence dans la gamme des nouilles instantanées.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;Elles sont aussi délicieuses que simples d’utilisation.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;Idéales pour un repas rapide, mais savoureux, ou pour un gouter chaud et consistant.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;Pour la préparation, il suffit de verser le contenu dans un bol et d’y ajouter de l’eau bouillante et recouvrir le tout durant quelques minutes.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p style=&quot;margin-left: 36pt; text-indent: -18pt;&quot;&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot;&gt;Vous pouvez composer votre&amp;nbsp;plat de nouilles Matsiro Salone&amp;nbsp;selon vos gouts.&lt;/span&gt;&lt;/p&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n  &lt;div&gt;\r\n     &lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/rwHtbnTVae0&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture&quot; allowfullscreen=&quot;&quot;&gt;&lt;/iframe&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;', '', 'Pâte instantanée MATSIRO', '', ''),
(49, 2, 'Massala', '', '', '', 'Massala', '', ''),
(51, 3, 'Poivre', '', '', '', 'Poivre', '', ''),
(52, 2, 'Carry', '', '', '', 'Carry', '', ''),
(53, 3, 'Sucre blanc/blond', '', '', '', 'Sucre blanc/blond', '', ''),
(54, 3, 'Sucre blanc/roux 50g', '', '', '', 'Sucre blanc/roux 50g', '', ''),
(55, 3, 'Sucre blanc/roux 100g', '', '', '', 'Sucre blanc/roux 100g', '', ''),
(56, 3, 'Sucre blanc/roux 200g', '', '', '', 'Sucre blanc/roux 200g', '', ''),
(57, 2, 'Vinaigre blanc/rouge 6° 5Cl', '', '', '', 'Vinaigre blanc/rouge 6° 5Cl', '', ''),
(58, 3, 'Vinaigre blanc/rouge 4,5° 25Cl', '', '', '', 'Vinaigre blanc/rouge 4,5° 25Cl', '', ''),
(59, 3, 'Vinaigre blanc/rouge 6° 25cl', '', '', '', 'Vinaigre blanc/rouge 6° 25cl', '', ''),
(60, 3, 'Vinaigre blanc/rouge 4,5° 100Cl', '', '', '', 'Vinaigre blanc/rouge 4,5° 100Cl', '', ''),
(61, 3, 'Vinaigre blanc/rouge 6° 100Cl', '', '', '', 'Vinaigre blanc/rouge 6° 100Cl', '', ''),
(62, 3, 'Vinaigre blanc 10° 100Cl', '', '', '', 'Vinaigre blanc 10° 100Cl', '', ''),
(63, 3, 'Madio Sachet', '', '', '', 'Madio Sachet', '', ''),
(64, 3, 'Savon de toilette Nosy Kelly', '', '', '', 'Savon de toilette Nosy Kelly', '', ''),
(65, 2, 'Savon de ménage Demi Barre Citron ONE', '', '', '', 'Savon de ménage Demi Barre Citron ONE', '', ''),
(66, 2, 'Savon de ménage Nosy Citron Frais', '', '', '', 'Savon de ménage Nosy Citron Frais', '', ''),
(67, 2, 'Savon de ménage Nosy', '', '', '', 'Savon de ménage Nosy', '', ''),
(68, 3, 'SEL FIN MATSIRO', '', '', '', 'SEL FIN MATSIRO', '', ''),
(69, 3, 'S\'CAFE', '', '', '', 'S\'CAFE', '', ''),
(49, 1, 'Massala', '', '', '', 'Massala', '', ''),
(49, 3, 'Massala', '', '', '', 'Massala', '', ''),
(51, 2, 'Poivre', '', '', '', 'Poivre', '', ''),
(51, 1, 'Poivre', '', '', '', 'Poivre', '', ''),
(52, 1, 'Carry', '', '', '', 'Carry', '', ''),
(52, 3, 'Carry', '', '', '', 'Carry', '', ''),
(53, 2, 'Sucre blanc/blond', '', '', '', 'Sucre blanc/blond', '', ''),
(53, 1, 'Sucre blanc/blond', '', '', '', 'Sucre blanc/blond', '', ''),
(54, 2, 'Sucre blanc/roux 50g', '', '', '', 'Sucre blanc/roux 50g', '', ''),
(54, 1, 'Sucre blanc/roux 50g', '', '', '', 'Sucre blanc/roux 50g', '', ''),
(55, 2, 'Sucre blanc/roux 100g', '', '', '', 'Sucre blanc/roux 100g', '', ''),
(55, 1, 'Sucre blanc/roux 100g', '', '', '', 'Sucre blanc/roux 100g', '', ''),
(56, 2, 'Sucre blanc/roux 200g', '', '', '', 'Sucre blanc/roux 200g', '', ''),
(56, 1, 'Sucre blanc/roux 200g', '', '', '', 'Sucre blanc/roux 200g', '', ''),
(57, 1, 'Vinaigre blanc/rouge 6° 5Cl', '', '', 'Vinaigre', 'Vinaigre blanc/rouge 6° 5Cl', '', ''),
(57, 3, 'Vinaigre blanc/rouge 6° 5Cl', '', '', 'Vinaigre', 'Vinaigre blanc/rouge 6° 5Cl', '', ''),
(58, 2, 'Vinaigre blanc/rouge 4,5° 25Cl', '', '', '', 'Vinaigre blanc/rouge 4,5° 25Cl', '', ''),
(58, 1, 'Vinaigre blanc/rouge 4,5° 25Cl', '', '', '', 'Vinaigre blanc/rouge 4,5° 25Cl', '', ''),
(59, 2, 'Vinaigre blanc/rouge 6° 25cl', '', '', '', 'Vinaigre blanc/rouge 6° 25cl', '', ''),
(59, 1, 'Vinaigre blanc/rouge 6° 25cl', '', '', '', 'Vinaigre blanc/rouge 6° 25cl', '', ''),
(60, 2, 'Vinaigre blanc/rouge 4,5° 100Cl', '', '', '', 'Vinaigre blanc/rouge 4,5° 100Cl', '', ''),
(60, 1, 'Vinaigre blanc/rouge 4,5° 100Cl', '', '', '', 'Vinaigre blanc/rouge 4,5° 100Cl', '', ''),
(61, 2, 'Vinaigre blanc/rouge 6° 100Cl', '', '', '', 'Vinaigre blanc/rouge 6° 100Cl', '', ''),
(61, 1, 'Vinaigre blanc/rouge 6° 100Cl', '', '', '', 'Vinaigre blanc/rouge 6° 100Cl', '', ''),
(62, 2, 'Vinaigre blanc 10° 100Cl', '', '', '', 'Vinaigre blanc 10° 100Cl', '', ''),
(62, 1, 'Vinaigre blanc 10° 100Cl', '', '', '', 'Vinaigre blanc 10° 100Cl', '', ''),
(63, 2, 'Madio Sachet', '', '', '', 'Madio Sachet', '', ''),
(63, 1, 'Madio Sachet', '', '', '', 'Madio Sachet', '', ''),
(64, 2, 'Savon de toilette Nosy Kelly', '', '', '', 'Savon de toilette Nosy Kelly', '', ''),
(64, 1, 'Savon de toilette Nosy Kelly', '', '', '', 'Savon de toilette Nosy Kelly', '', ''),
(65, 1, 'Savon de ménage Demi Barre Citron ONE', '', '', '', 'Savon de ménage Demi Barre Citron ONE', '', ''),
(65, 3, 'Savon de ménage Demi Barre Citron ONE', '', '', '', 'Savon de ménage Demi Barre Citron ONE', '', ''),
(66, 1, 'Savon de ménage Nosy Citron Frais', '', '', '', 'Savon de ménage Nosy Citron Frais', '', ''),
(66, 3, 'Savon de ménage Nosy Citron Frais', '', '', '', 'Savon de ménage Nosy Citron Frais', '', ''),
(67, 1, 'Savon de ménage Nosy', '', '', '', 'Savon de ménage Nosy', '', ''),
(67, 3, 'Savon de ménage Nosy', '', '', '', 'Savon de ménage Nosy', '', ''),
(68, 2, 'SEL FIN MATSIRO', '', '', '', 'SEL FIN MATSIRO', '', ''),
(68, 1, 'SEL FIN MATSIRO', '', '', '', 'SEL FIN MATSIRO', '', ''),
(69, 2, 'S\'CAFE', '', '', '', 'S\'CAFE', '', ''),
(69, 1, 'S\'CAFE', '', '', '', 'S\'CAFE', '', ''),
(28, 2, 'SAINTO GM', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;ul&gt;\r\n&lt;/ul&gt;\r\n', '', '', 'SAINTO GM', '', ''),
(70, 2, 'SAINTO PM', '', '', '', 'SAINTO PM', '', ''),
(71, 2, 'BIG Orange PM', '', '', '', 'BIG Orange PM', '', ''),
(72, 2, 'BIG Orange GM', '', '', '', 'BIG Orange GM', '', ''),
(73, 1, 'BIG Cola PM', '', '', '', 'BIG Cola PM', '', ''),
(42, 2, 'Maki Ambré avec raphia 37,5° ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', 'Maki Ambré avec raphia 37,5° ', '', ''),
(73, 3, 'BIG Cola PM', '', '', '', 'BIG Cola PM', '', ''),
(79, 2, 'Ambré de Nosy Be Prestige 52°', '', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', 'Ambré de Nosy Be Prestige 52°', '', ''),
(48, 2, 'Pâte instantanée MATSIRO', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n  &lt;div&gt;\r\n   \r\n   &lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;!--[if !supportLists]--&gt;&lt;/p&gt;&lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;!--[if !supportLists]--&gt;&lt;/p&gt;&lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;!--[if !supportLists]--&gt;&lt;/p&gt;&lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;!--[if !supportLists]--&gt;&lt;/p&gt;&lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;span lang=&quot;FR&quot; style=&quot;text-indent: -18pt;&quot;&gt;&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span lang=&quot;FR&quot; style=&quot;text-indent: -18pt;&quot;&gt;A Madagascar, les pâtes Matsiro\r\nSalone sont une référence dans la gamme des nouilles instantanées.&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;!--[if !supportLists]--&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;\r\n&lt;/span&gt;&lt;/span&gt;&lt;!--[endif]--&gt;&lt;span lang=&quot;FR&quot;&gt;Elles sont aussi délicieuses que\r\nsimples d’utilisation.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;!--[if !supportLists]--&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;\r\n&lt;/span&gt;&lt;/span&gt;&lt;!--[endif]--&gt;&lt;span lang=&quot;FR&quot;&gt;Idéales pour un repas rapide, mais\r\nsavoureux, ou pour un gouter chaud et consistant. &lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;!--[if !supportLists]--&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;\r\n&lt;/span&gt;&lt;/span&gt;&lt;!--[endif]--&gt;&lt;span lang=&quot;FR&quot;&gt;Pour la préparation, il suffit de\r\nverser le contenu dans un bol et d’y ajouter de l’eau bouillante et recouvrir\r\nle tout durant quelques minutes. &lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n&lt;/p&gt;&lt;p style=&quot;margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1&quot;&gt;&lt;!--[if !supportLists]--&gt;&lt;span lang=&quot;FR&quot;&gt;-&lt;span style=&quot;font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &amp;quot;Times New Roman&amp;quot;;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;\r\n&lt;/span&gt;&lt;/span&gt;&lt;!--[endif]--&gt;&lt;span lang=&quot;FR&quot;&gt;Vous pouvez composer\r\nvotre&amp;nbsp;plat de nouilles Matsiro Salone&amp;nbsp;selon vos gouts.&lt;o:p&gt;&lt;/o:p&gt;&lt;/span&gt;&lt;/p&gt;\r\n  &lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n  &lt;div&gt;\r\n     &lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/rwHtbnTVae0&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture&quot; allowfullscreen=&quot;&quot;&gt;&lt;/iframe&gt;\r\n   \r\n  &lt;/div&gt;\r\n&lt;/div&gt;', '', 'Pâte instantanée MATSIRO', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_discount`
--

DROP TABLE IF EXISTS `oc_product_discount`;
CREATE TABLE IF NOT EXISTS `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_discount_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=453 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_filter`
--

DROP TABLE IF EXISTS `oc_product_filter`;
CREATE TABLE IF NOT EXISTS `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_image`
--

DROP TABLE IF EXISTS `oc_product_image`;
CREATE TABLE IF NOT EXISTS `oc_product_image` (
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_image_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2489 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product_image`
--

INSERT INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(2488, 48, 'catalog/demo/product/nouille matsiro.png', 0),
(2487, 48, 'catalog/demo/product/NOUILLES MATSIRO CREVETTES.png', 0),
(2486, 48, 'catalog/demo/product/MATSIRO POULET.png', 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_option`
--

DROP TABLE IF EXISTS `oc_product_option`;
CREATE TABLE IF NOT EXISTS `oc_product_option` (
  `product_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`product_option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=230 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product_option`
--

INSERT INTO `oc_product_option` (`product_option_id`, `product_id`, `option_id`, `value`, `required`) VALUES
(229, 48, 14, '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_option_value`
--

DROP TABLE IF EXISTS `oc_product_option_value`;
CREATE TABLE IF NOT EXISTS `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL,
  PRIMARY KEY (`product_option_value_id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product_option_value`
--

INSERT INTO `oc_product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`, `price`, `price_prefix`, `points`, `points_prefix`, `weight`, `weight_prefix`) VALUES
(25, 229, 48, 14, 59, 10, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(27, 229, 48, 14, 58, 10, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(28, 229, 48, 14, 57, 10, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(26, 229, 48, 14, 56, 10, 1, '0.0000', '+', 0, '+', '0.00000000', '+');

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_recurring`
--

DROP TABLE IF EXISTS `oc_product_recurring`;
CREATE TABLE IF NOT EXISTS `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_related`
--

DROP TABLE IF EXISTS `oc_product_related`;
CREATE TABLE IF NOT EXISTS `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_reward`
--

DROP TABLE IF EXISTS `oc_product_reward`;
CREATE TABLE IF NOT EXISTS `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_reward_id`)
) ENGINE=MyISAM AUTO_INCREMENT=579 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product_reward`
--

INSERT INTO `oc_product_reward` (`product_reward_id`, `product_id`, `customer_group_id`, `points`) VALUES
(577, 42, 1, 100),
(578, 28, 1, 400);

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_special`
--

DROP TABLE IF EXISTS `oc_product_special`;
CREATE TABLE IF NOT EXISTS `oc_product_special` (
  `product_special_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_special_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=467 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_to_category`
--

DROP TABLE IF EXISTS `oc_product_to_category`;
CREATE TABLE IF NOT EXISTS `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(28, 20),
(28, 72),
(42, 20),
(42, 59),
(48, 18),
(48, 46),
(49, 18),
(49, 90),
(51, 18),
(51, 90),
(52, 18),
(52, 90),
(53, 18),
(53, 89),
(54, 18),
(54, 89),
(55, 18),
(55, 89),
(56, 18),
(56, 89),
(57, 18),
(57, 88),
(58, 18),
(58, 88),
(59, 18),
(59, 88),
(60, 18),
(60, 88),
(61, 18),
(61, 88),
(62, 18),
(62, 88),
(63, 18),
(63, 78),
(64, 18),
(64, 78),
(65, 18),
(65, 78),
(66, 18),
(66, 78),
(67, 18),
(67, 78),
(68, 18),
(68, 25),
(69, 18),
(69, 45),
(70, 20),
(70, 72),
(71, 20),
(71, 72),
(72, 20),
(72, 72),
(73, 20),
(73, 72),
(74, 20),
(74, 59),
(75, 20),
(75, 59),
(76, 20),
(76, 59),
(77, 20),
(77, 59),
(78, 20),
(78, 59),
(79, 20),
(79, 59),
(80, 20),
(80, 59),
(81, 20),
(81, 59),
(82, 20),
(82, 59),
(83, 20),
(83, 59);

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_to_download`
--

DROP TABLE IF EXISTS `oc_product_to_download`;
CREATE TABLE IF NOT EXISTS `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_to_layout`
--

DROP TABLE IF EXISTS `oc_product_to_layout`;
CREATE TABLE IF NOT EXISTS `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product_to_layout`
--

INSERT INTO `oc_product_to_layout` (`product_id`, `store_id`, `layout_id`) VALUES
(42, 0, 0),
(72, 0, 0),
(52, 0, 0),
(28, 0, 0),
(71, 0, 0),
(70, 0, 0),
(48, 0, 0),
(73, 0, 0),
(51, 0, 0),
(45, 0, 0),
(31, 0, 0),
(29, 0, 0),
(35, 0, 0),
(49, 0, 0),
(33, 0, 0),
(46, 0, 0),
(53, 0, 0),
(54, 0, 0),
(55, 0, 0),
(56, 0, 0),
(57, 0, 0),
(58, 0, 0),
(59, 0, 0),
(60, 0, 0),
(61, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0),
(66, 0, 0),
(67, 0, 0),
(68, 0, 0),
(69, 0, 0),
(74, 0, 0),
(75, 0, 0),
(76, 0, 0),
(77, 0, 0),
(78, 0, 0),
(79, 0, 0),
(80, 0, 0),
(81, 0, 0),
(82, 0, 0),
(83, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_product_to_store`
--

DROP TABLE IF EXISTS `oc_product_to_store`;
CREATE TABLE IF NOT EXISTS `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(28, 0),
(29, 0),
(31, 0),
(33, 0),
(35, 0),
(42, 0),
(45, 0),
(46, 0),
(48, 0),
(49, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_recurring`
--

DROP TABLE IF EXISTS `oc_recurring`;
CREATE TABLE IF NOT EXISTS `oc_recurring` (
  `recurring_id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`recurring_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_recurring_description`
--

DROP TABLE IF EXISTS `oc_recurring_description`;
CREATE TABLE IF NOT EXISTS `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`recurring_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_return`
--

DROP TABLE IF EXISTS `oc_return`;
CREATE TABLE IF NOT EXISTS `oc_return` (
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`return_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_return`
--

INSERT INTO `oc_return` (`return_id`, `order_id`, `product_id`, `customer_id`, `firstname`, `lastname`, `email`, `telephone`, `product`, `model`, `quantity`, `opened`, `return_reason_id`, `return_action_id`, `return_status_id`, `comment`, `date_ordered`, `date_added`, `date_modified`) VALUES
(1, 6, 0, 5, 'Dimbisoa ', 'RAKOTONARIVO', 'sandratrarakotonarivo@yahoo.fr', '0332848333', 'Pâte instantanée MATSIRO', 'PAT-001', 1, 1, 3, 3, 2, '', '2020-05-15', '2020-05-15 09:42:08', '2020-05-15 09:44:40');

-- --------------------------------------------------------

--
-- Structure de la table `oc_return_action`
--

DROP TABLE IF EXISTS `oc_return_action`;
CREATE TABLE IF NOT EXISTS `oc_return_action` (
  `return_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`return_action_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent'),
(1, 2, 'Refunded'),
(2, 2, 'Credit Issued'),
(3, 2, 'Replacement Sent'),
(1, 3, 'Refunded'),
(2, 3, 'Credit Issued'),
(3, 3, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Structure de la table `oc_return_history`
--

DROP TABLE IF EXISTS `oc_return_history`;
CREATE TABLE IF NOT EXISTS `oc_return_history` (
  `return_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`return_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_return_reason`
--

DROP TABLE IF EXISTS `oc_return_reason`;
CREATE TABLE IF NOT EXISTS `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`return_reason_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details'),
(1, 2, 'Dead On Arrival'),
(2, 2, 'Received Wrong Item'),
(3, 2, 'Order Error'),
(4, 2, 'Faulty, please supply details'),
(5, 2, 'Other, please supply details'),
(1, 3, 'Dead On Arrival'),
(2, 3, 'Received Wrong Item'),
(3, 3, 'Order Error'),
(4, 3, 'Faulty, please supply details'),
(5, 3, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Structure de la table `oc_return_status`
--

DROP TABLE IF EXISTS `oc_return_status`;
CREATE TABLE IF NOT EXISTS `oc_return_status` (
  `return_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`return_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products'),
(1, 2, 'Pending'),
(3, 2, 'Complete'),
(2, 2, 'Awaiting Products'),
(1, 3, 'Pending'),
(3, 3, 'Complete'),
(2, 3, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Structure de la table `oc_review`
--

DROP TABLE IF EXISTS `oc_review`;
CREATE TABLE IF NOT EXISTS `oc_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`review_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_review`
--

INSERT INTO `oc_review` (`review_id`, `product_id`, `customer_id`, `author`, `text`, `rating`, `status`, `date_added`, `date_modified`) VALUES
(1, 42, 0, 'Austin', 'The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed specifically for the creative professional, this display provides more space for easier access to all the tools and palettes needed to edit, format and composite your work. Combine this display with a Mac Pro, MacBook Pro, or PowerMac G5 and there\'s no limit to what you can achieve.', 5, 0, '2018-03-16 15:27:02', '2018-05-30 09:25:39'),
(4, 28, 0, 'Austin', 'The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed specifically for the creative professional, this display provides more space for easier access to all the tools and palettes needed to edit, format and composite your work. Combine this display with a Mac Pro, MacBook Pro, or PowerMac G5 and there\'s no limit to what you can achieve.', 3, 1, '2018-03-16 15:27:55', '2018-03-16 15:32:51'),
(7, 48, 0, 'Austin', 'The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed specifically for the creative professional, this display provides more space for easier access to all the tools and palettes needed to edit, format and composite your work. Combine this display with a Mac Pro, MacBook Pro, or PowerMac G5 and there\'s no limit to what you can achieve.', 4, 1, '2018-03-16 15:28:38', '2018-03-16 15:32:18');

-- --------------------------------------------------------

--
-- Structure de la table `oc_revslider_attachment_images`
--

DROP TABLE IF EXISTS `oc_revslider_attachment_images`;
CREATE TABLE IF NOT EXISTS `oc_revslider_attachment_images` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_revslider_css`
--

DROP TABLE IF EXISTS `oc_revslider_css`;
CREATE TABLE IF NOT EXISTS `oc_revslider_css` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `handle` text NOT NULL,
  `settings` text,
  `hover` text,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_revslider_css`
--

INSERT INTO `oc_revslider_css` (`id`, `handle`, `settings`, `hover`, `params`) VALUES
(1, '.tp-caption.lightgrey_divider', NULL, NULL, '{\"text-decoration\":\"none\",\"background-color\":\"rgba(235, 235, 235, 1)\",\"width\":\"370px\",\"height\":\"3px\",\"background-position\":\"initial initial\",\"background-repeat\":\"initial initial\",\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}'),
(2, '.tp-caption.large_bold_grey', NULL, NULL, '{\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(102, 102, 102)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"padding\":\"1px 4px 0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(3, '.tp-caption.medium_thin_grey', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"30px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(102, 102, 102)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"1px 4px 0px\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(4, '.tp-caption.small_thin_grey', NULL, NULL, '{\"font-size\":\"18px\",\"line-height\":\"26px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(117, 117, 117)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"1px 4px 0px\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(5, '.tp-caption.large_bold_darkblue', NULL, NULL, '{\"font-size\":\"58px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(52, 73, 94)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(6, '.tp-caption.medium_bg_darkblue', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(52, 73, 94)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(7, '.tp-caption.medium_bold_red', NULL, NULL, '{\"font-size\":\"24px\",\"line-height\":\"30px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(227, 58, 12)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(8, '.tp-caption.medium_light_red', NULL, NULL, '{\"font-size\":\"21px\",\"line-height\":\"26px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(227, 58, 12)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(9, '.tp-caption.medium_bg_red', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(227, 58, 12)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(10, '.tp-caption.medium_bold_orange', NULL, NULL, '{\"font-size\":\"24px\",\"line-height\":\"30px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(243, 156, 18)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(11, '.tp-caption.medium_bg_orange', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(243, 156, 18)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(12, '.tp-caption.medium_bg_asbestos', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(127, 140, 141)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(13, '.tp-caption.large_bold_white', NULL, NULL, '{\"font-size\":\"58px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(14, '.tp-caption.medium_light_white', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(15, '.tp-caption.grassfloor', NULL, NULL, '{\"text-decoration\":\"none\",\"background-color\":\"rgba(160, 179, 151, 1)\",\"width\":\"4000px\",\"height\":\"150px\",\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}'),
(16, '.tp-caption.mediumlarge_light_white', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"40px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(17, '.tp-caption.mediumlarge_light_white_center', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"40px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"#ffffff\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px 0px 0px 0px\",\"text-align\":\"center\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(18, '.tp-caption.black_heavy_60', NULL, NULL, '{\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(19, '.tp-caption.black_bold_bg_20', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(0, 0, 0)\",\"padding\":\"5px 8px\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(20, '.tp-caption.green_bold_bg_20', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(134, 181, 103)\",\"padding\":\"5px 8px\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(21, '.tp-caption.greenishbg_heavy_70', NULL, NULL, '{\"font-size\":\"70px\",\"line-height\":\"70px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"padding\":\"50px\",\"text-shadow\":\"none\",\"background-color\":\"rgba(40, 67, 62, 0.8)\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(22, '.tp-caption.borderbox_725x130', NULL, NULL, '{\"min-width\":\"725px\",\"min-height\":\"130px\",\"background-color\":\"transparent\",\"text-decoration\":\"none\",\"border-width\":\"2px\",\"border-color\":\"rgb(255, 255, 255)\",\"border-style\":\"solid\"}'),
(23, '.tp-caption.black_heavy_70', NULL, NULL, '{\"font-size\":\"70px\",\"line-height\":\"70px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(24, '.tp-caption.light_heavy_70', NULL, NULL, '{\"font-size\":\"70px\",\"line-height\":\"70px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(25, '.tp-caption.black_bold_40', NULL, NULL, '{\"font-size\":\"40px\",\"line-height\":\"40px\",\"font-weight\":\"800\",\"font-family\":\"Raleway\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(26, '.tp-caption.white_bold_bg_20', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(255, 255, 255)\",\"padding\":\"5px 8px\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(27, '.tp-caption.red_bold_bg_20', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(224, 51, 0)\",\"padding\":\"5px 8px\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(28, '.tp-caption.blue_bold_bg_20', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(53, 152, 220)\",\"padding\":\"5px 8px\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(29, '.tp-caption.light_heavy_40', NULL, NULL, '{\"font-size\":\"40px\",\"line-height\":\"40px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(30, '.tp-caption.white_thin_34', NULL, NULL, '{\"font-size\":\"35px\",\"line-height\":\"35px\",\"font-weight\":\"200\",\"font-family\":\"Raleway\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(31, '.tp-caption.light_heavy_70_shadowed', NULL, NULL, '{\"font-size\":\"70px\",\"line-height\":\"70px\",\"font-weight\":\"900\",\"font-family\":\"Raleway\",\"color\":\"#ffffff\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"0px 0px 7px rgba(0, 0, 0, 0.25)\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(32, '.tp-caption.light_medium_30_shadowed', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"40px\",\"font-weight\":\"700\",\"font-family\":\"Raleway\",\"color\":\"#ffffff\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"0px 0px 7px rgba(0, 0, 0, 0.25)\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}'),
(33, '.tp-caption.bignumbers_white', NULL, NULL, '{\"color\":\"#ffffff\",\"background-color\":\"rgba(0, 0, 0, 0)\",\"font-size\":\"84px\",\"line-height\":\"84px\",\"font-weight\":\"800\",\"font-family\":\"Raleway\",\"text-decoration\":\"none\",\"padding\":\"0px 0px 0px 0px\",\"text-shadow\":\"rgba(0, 0, 0, 0.247059) 0px 0px 7px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 255, 255)\",\"border-style\":\"none solid none none\"}'),
(34, '.tp-caption.bignumbers_whitefdf', NULL, NULL, '{\"font-size\":\"92px\",\"line-height\":\"61px\",\"font-weight\":\"500\",\"font-family\":\"Raleway\",\"color\":\"rgb(43, 38, 38)\",\"text-decoration\":\"none\",\"padding\":\"0px\",\"text-shadow\":\"rgba(0, 0, 0, 0.247059) 0px 0px 7px\",\"background-color\":\"rgba(0, 0, 0, 0.00784314)\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 255, 255)\",\"border-style\":\"none\"}'),
(35, '.tp-caption.simplegreat_text', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"40px\",\"font-weight\":\"normal\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"#ffffff\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px 0px 0px 0px\",\"text-align\":\"center\",\"text-transform\":\"uppercase\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}'),
(36, '.tp-caption.black', NULL, NULL, '{\"color\":\"#000\",\"text-shadow\":\"none\"}'),
(37, '.tp-caption.revslider-big-text', NULL, NULL, '{\"font-size\":\"72px\",\"line-height\":\"64px\",\"font-weight\":\"400\",\"font-family\":\"Montserrat\",\"color\":\"rgb(46, 93, 96)\",\"text-decoration\":\"none\",\"letter-spacing\":\"-4px\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}'),
(38, '.tp-caption.revslider-excerpt-lower', NULL, NULL, '{\"font-size\":\"22px\",\"letter-spacing\":\"-1px\",\"line-height\":\"30px\",\"text-decoration\":\"none\",\"color\":\"#2e5d60\",\"background-color\":\"transparent\",\"font-weight\":\"300\",\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}');

-- --------------------------------------------------------

--
-- Structure de la table `oc_revslider_layer_animations`
--

DROP TABLE IF EXISTS `oc_revslider_layer_animations`;
CREATE TABLE IF NOT EXISTS `oc_revslider_layer_animations` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `handle` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_revslider_settings`
--

DROP TABLE IF EXISTS `oc_revslider_settings`;
CREATE TABLE IF NOT EXISTS `oc_revslider_settings` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `general` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_revslider_sliders`
--

DROP TABLE IF EXISTS `oc_revslider_sliders`;
CREATE TABLE IF NOT EXISTS `oc_revslider_sliders` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `title` tinytext NOT NULL,
  `alias` tinytext,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_revslider_sliders`
--

INSERT INTO `oc_revslider_sliders` (`id`, `title`, `alias`, `params`) VALUES
(1, 'home', 'home', '{\"title\":\"home\",\"alias\":\"home\",\"source_type\":\"gallery\",\"post_types\":\"product\",\"post_category\":\"category_20\",\"post_sortby\":\"ID\",\"prd_img_width\":\"880\",\"prd_img_height\":\"345\",\"posts_sort_direction\":\"DESC\",\"max_slider_posts\":\"30\",\"excerpt_limit\":\"55\",\"slider_template_id\":\"\",\"posts_list\":\"\",\"slider_type\":\"fullwidth\",\"fullscreen_offset_container\":\"\",\"fullscreen_offset_size\":\"\",\"fullscreen_min_height\":\"\",\"full_screen_align_force\":\"off\",\"auto_height\":\"off\",\"force_full_width\":\"off\",\"min_height\":\"0\",\"width\":\"960\",\"height\":\"400\",\"responsitive_w1\":\"940\",\"responsitive_sw1\":\"770\",\"responsitive_w2\":\"780\",\"responsitive_sw2\":\"500\",\"responsitive_w3\":\"510\",\"responsitive_sw3\":\"310\",\"responsitive_w4\":\"0\",\"responsitive_sw4\":\"0\",\"responsitive_w5\":\"0\",\"responsitive_sw5\":\"0\",\"responsitive_w6\":\"0\",\"responsitive_sw6\":\"0\",\"delay\":\"9000\",\"shuffle\":\"off\",\"lazy_load\":\"off\",\"use_wpml\":\"off\",\"enable_static_layers\":\"off\",\"next_slide_on_window_focus\":\"off\",\"simplify_ie8_ios4\":\"off\",\"stop_slider\":\"off\",\"stop_after_loops\":0,\"stop_at_slide\":2,\"show_timerbar\":\"top\",\"loop_slide\":\"loop\",\"position\":\"center\",\"margin_top\":0,\"margin_bottom\":0,\"margin_left\":0,\"margin_right\":0,\"shadow_type\":\"2\",\"padding\":0,\"background_color\":\"#E9E9E9\",\"background_dotted_overlay\":\"none\",\"show_background_image\":\"false\",\"background_image\":\"\",\"bg_fit\":\"cover\",\"bg_repeat\":\"no-repeat\",\"bg_position\":\"center top\",\"stop_on_hover\":\"on\",\"keyboard_navigation\":\"off\",\"navigation_style\":\"round\",\"navigaion_type\":\"bullet\",\"navigation_arrows\":\"solo\",\"navigaion_always_on\":\"false\",\"hide_thumbs\":200,\"navigaion_align_hor\":\"center\",\"navigaion_align_vert\":\"bottom\",\"navigaion_offset_hor\":\"0\",\"navigaion_offset_vert\":20,\"leftarrow_align_hor\":\"left\",\"leftarrow_align_vert\":\"center\",\"leftarrow_offset_hor\":20,\"leftarrow_offset_vert\":0,\"rightarrow_align_hor\":\"right\",\"rightarrow_align_vert\":\"center\",\"rightarrow_offset_hor\":20,\"rightarrow_offset_vert\":0,\"thumb_width\":100,\"thumb_height\":50,\"thumb_amount\":5,\"use_spinner\":\"0\",\"spinner_color\":\"#FFFFFF\",\"use_parallax\":\"off\",\"disable_parallax_mobile\":\"off\",\"parallax_type\":\"mouse\",\"parallax_bg_freeze\":\"off\",\"parallax_level_1\":\"5\",\"parallax_level_2\":\"10\",\"parallax_level_3\":\"15\",\"parallax_level_4\":\"20\",\"parallax_level_5\":\"25\",\"parallax_level_6\":\"30\",\"parallax_level_7\":\"35\",\"parallax_level_8\":\"40\",\"parallax_level_9\":\"45\",\"parallax_level_10\":\"50\",\"touchenabled\":\"on\",\"swipe_velocity\":75,\"swipe_min_touches\":1,\"drag_block_vertical\":\"false\",\"disable_on_mobile\":\"off\",\"disable_kenburns_on_mobile\":\"off\",\"hide_slider_under\":0,\"hide_defined_layers_under\":0,\"hide_all_layers_under\":0,\"hide_arrows_on_mobile\":\"off\",\"hide_bullets_on_mobile\":\"off\",\"hide_thumbs_on_mobile\":\"off\",\"hide_thumbs_under_resolution\":0,\"hide_thumbs_delay_mobile\":1500,\"start_with_slide\":\"1\",\"first_transition_active\":\"false\",\"first_transition_type\":\"fade\",\"first_transition_duration\":300,\"first_transition_slot_amount\":7,\"reset_transitions\":\"\",\"reset_transition_duration\":0,\"0\":\"Execute settings on all slides\",\"jquery_noconflict\":\"off\",\"js_to_body\":\"false\",\"output_type\":\"none\",\"custom_css\":\"\",\"custom_javascript\":\"\",\"template\":\"false\"}');

-- --------------------------------------------------------

--
-- Structure de la table `oc_revslider_slides`
--

DROP TABLE IF EXISTS `oc_revslider_slides`;
CREATE TABLE IF NOT EXISTS `oc_revslider_slides` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `slider_id` int(9) NOT NULL,
  `slide_order` int(11) NOT NULL,
  `params` text NOT NULL,
  `layers` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_revslider_slides`
--

INSERT INTO `oc_revslider_slides` (`id`, `slider_id`, `slide_order`, `params`, `layers`) VALUES
(1, 1, 1, '{\"background_type\":\"image\",\"image\":\"http:\\/\\/localhost\\/tsena\\/upload\\/image\\/catalog\\/revslider_media_folder\\/gift-voucher-birthday.jpg\"}', ''),
(2, 1, 2, '{\"background_type\":\"image\",\"image\":\"http:\\/\\/localhost\\/tsena\\/upload\\/image\\/catalog\\/revslider_media_folder\\/cocacola.png\"}', '');

-- --------------------------------------------------------

--
-- Structure de la table `oc_revslider_static_slides`
--

DROP TABLE IF EXISTS `oc_revslider_static_slides`;
CREATE TABLE IF NOT EXISTS `oc_revslider_static_slides` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `slider_id` int(9) NOT NULL,
  `params` text NOT NULL,
  `layers` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_seo_url`
--

DROP TABLE IF EXISTS `oc_seo_url`;
CREATE TABLE IF NOT EXISTS `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`seo_url_id`),
  KEY `query` (`query`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM AUTO_INCREMENT=941 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_seo_url`
--

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES
(940, 0, 1, 'product_id=48', 'ipod-classic'),
(928, 0, 1, 'category_id=20', 'Electronic'),
(890, 0, 2, 'manufacturer_id=8', 'Dzama'),
(772, 0, 1, 'information_id=4', 'about_us'),
(933, 0, 1, 'product_id=42', 'test'),
(910, 0, 2, 'category_id=88', 'Vinaigre'),
(929, 0, 2, 'category_id=18', 'PPN'),
(893, 0, 2, 'category_id=46', 'Pâte instantanée'),
(894, 0, 1, 'category_id=45', 'Women'),
(911, 0, 2, 'category_id=25', 'sel'),
(912, 0, 2, 'category_id=89', 'Sucre'),
(913, 0, 2, 'category_id=90', 'Epices'),
(897, 0, 2, 'category_id=78', 'Savonnerie'),
(916, 0, 2, 'product_id=51', 'Poivre'),
(935, 0, 1, 'product_id=28', 'htc-touch-hd'),
(814, 0, 1, 'product_id=45', 'macbook-pro'),
(816, 0, 1, 'product_id=31', 'nikon-d300'),
(817, 0, 1, 'product_id=29', 'palm-treo-pro'),
(818, 0, 1, 'product_id=35', 'product-8'),
(932, 0, 1, 'product_id=49', 'samsung-galaxy-tab-10-1'),
(820, 0, 1, 'product_id=33', 'samsung-syncmaster-941bw'),
(821, 0, 1, 'product_id=46', 'sony-vaio'),
(925, 0, 2, 'manufacturer_id=9', 'Salone'),
(829, 0, 1, 'manufacturer_id=5', 'htc'),
(841, 0, 1, 'information_id=6', 'delivery'),
(842, 0, 1, 'information_id=3', 'privacy'),
(843, 0, 1, 'information_id=5', 'terms');

-- --------------------------------------------------------

--
-- Structure de la table `oc_session`
--

DROP TABLE IF EXISTS `oc_session`;
CREATE TABLE IF NOT EXISTS `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_session`
--

INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('01643f2619d590fe8ce95f4900', '{\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-19 06:07:28'),
('025a53d5d5c58c5d263342ff5f', '{\"api_id\":\"3\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-05-12 08:38:25'),
('03384f552b47dcb93d06a06fe1', '{\"api_id\":\"3\"}', '2020-05-12 13:34:08'),
('047e117299ef1edc51f9a8565b', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"hAHOxqqIzrnbnx9QeKGnjG8vnTsk3zjY\"}', '2019-03-05 10:12:25'),
('061c29b4a5b36e75052b7680e9', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-20 08:57:19'),
('09164b28a5a44a71b6eeacc51c', '{\"language\":\"mg-MG\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"QSuJfHtnN3S1xe2Zm5ZPE9qOz7kXLFMV\"}', '2020-05-24 14:44:40'),
('0926b3d92dd849f757c60ab68d', '{\"language\":\"ar\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"aRWGtKlanKhpEBCLBK2nmjccDsQohojJ\"}', '2019-03-05 13:22:53'),
('0a7ac245c9d2935ea91b1fab6f', 'false', '2018-05-29 03:15:35'),
('0ac6d2ef1bb533a79657fa29b5', '{\"language\":\"mg-MG\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"GAR5mTd8cOJAIQRPbtamiOnZkGzOE8z6\",\"install\":\"Zx5Sily3Zo\"}', '2020-05-22 16:12:58'),
('0b6ce6ac904590ac3eadfeafd7', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-04-23 06:55:06'),
('0bdf351999a318fec4bc0619a2', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-27 09:53:40'),
('0c8d16943344d7882865bf030d', '{\"api_id\":\"3\"}', '2020-05-15 13:31:34'),
('0f2f13bbbea4801c03c2380206', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-07 04:27:17'),
('0f59ebff88669f9721f905b62d', '{\"api_id\":\"3\"}', '2020-05-15 13:32:37'),
('109069ab9463456efb8e9df07f', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-25 08:52:27'),
('1172dcfff16de2adf34e9f1191', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-25 03:00:42'),
('1258c9e1dcb4a23b461097db7a', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-10 06:02:14'),
('12a2d1ea4c44d0ade79710d8f2', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-06 13:08:27'),
('143a5dfbd1c92f4e6e98245566', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-06 11:54:44'),
('147a4a7ab36a0d2a7f7dda444f', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-09 12:01:14'),
('1677b9114e37d2132aecc0e8a0', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-10 06:35:54'),
('171add43e45444d9580e1ff877', '{\"api_id\":\"3\"}', '2020-05-15 16:51:29'),
('1721706d7942802fbe8b1e06a3', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-03 10:55:41'),
('18e37daedcfcde7ba044d700f4', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-30 02:29:40'),
('195f7e9a3ba5a080ba109f566a', '{\"api_id\":\"3\"}', '2020-05-15 16:51:07'),
('1b0fe8c68063751737782a9d76', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-19 05:19:00'),
('1c894bfa4b84ed762cd022a2ee', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-17 01:52:36'),
('1d36584c5abbc3a4eea9a9fa89', '{\"api_id\":\"3\"}', '2020-05-15 12:54:30'),
('1d4706d472fd31a6e892afb80a', '{\"user_id\":\"1\",\"user_token\":\"aA1Nz7JVZVykRvMwr8qub8A73z4l2pGY\",\"language\":\"en-gb\",\"currency\":\"MGA\"}', '2020-05-14 18:46:32'),
('1e2dfdb7c9d35b18ba65a1fa26', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-17 08:46:37'),
('1ee10efeccd8445c2e7f3557e4', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-26 13:21:40'),
('203e2c428f18cbf04b0fbc3878', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"37vSyMbMYNhBkC9VcPD7R0ZR3i9FxCiK\"}', '2018-03-15 09:45:59'),
('209b84fc9cdc6b8c0a3684b09c', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"iUpDsRY2yjFg5fAheEMSlbteW5S8r6AK\",\"wishlist\":[\"32\"]}', '2019-03-06 13:23:16'),
('25ef97c75f30a87b08d006b827', '{\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-22 05:19:54'),
('268b1cff6e4f67bd6173c558a8', '{\"user_id\":\"1\",\"user_token\":\"CWEgU5FuZeJK50Z73GN7whzv86dVxDYv\",\"language\":\"fr-FR\",\"currency\":\"MGA\",\"install\":\"Ow2au4xxef\"}', '2020-05-19 16:20:27'),
('2728bcb2f7fc090d2544c99337', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-23 13:10:45'),
('285cc53e74d595ca4a4235ee7a', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"3\",\"user_token\":\"3bwM09nuKPx7T77uXfJ2kpYYHh2S5qkA\"}', '2020-05-14 11:32:02'),
('2e16dcb40df932e6bd40e1f88f', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-10 04:26:37'),
('30f6d914855e90d926a36937cd', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"I4RtAEgvCBc5ku0c9ciDwZ6ZSWLvZUML\"}', '2019-03-06 13:14:08'),
('3155f6e976c9ff01a35ffdfb5f', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-08 09:42:44'),
('31db89415e6e78c2f2a7a3aeb3', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"compare\":[\"44\"],\"customer_id\":\"1\"}', '2018-03-28 09:56:00'),
('321edf26069fe5f07925cadd14', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-30 03:02:57'),
('350e0f1383f629b4f89887cb85', '{\"api_id\":\"3\"}', '2020-05-18 07:56:40'),
('354c21150f6f55d11ee2bbf874', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"9qvICZ9QPkk6GzDgSKlSi8NY1XKWvCcu\"}', '2020-05-21 16:10:25'),
('3684130cac3694bf0010b556af', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-28 10:12:49'),
('374cfd7da0c9b90c83402dad68', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-15 07:24:17'),
('3779ca5c90ae1ba5c36aa5f992', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"wishlist\":[\"32\"],\"compare\":[\"32\"],\"user_id\":\"1\",\"user_token\":\"sEBfr7vTUodn3fNrYUO4daApVPKeQ5OD\"}', '2019-04-10 09:54:12'),
('386dc223bd46fadf18f7f5e31b', '{\"api_id\":\"3\"}', '2020-05-14 12:53:45'),
('3ab29628447b4f595ff8a47509', '{\"api_id\":\"3\"}', '2020-05-18 07:56:28'),
('3b42a6109fc5db8f6936b35159', '{\"api_id\":\"3\"}', '2020-05-15 13:05:42'),
('3e0987a2f8d7e052be47077167', '{\"api_id\":\"3\"}', '2020-05-15 17:32:18'),
('3ef1268e583662f68ee483b796', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-16 08:25:12'),
('3f493ddb00de8852e03add7d20', '{\"api_id\":\"3\"}', '2020-05-15 17:06:29'),
('3ff811f9d8bfdbba82592ea753', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-04-23 07:26:41'),
('41b9ca3e3b2ef15ca467bf6300', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-15 07:24:03'),
('42bf361291a6cb986c219b1db8', '{\"api_id\":\"3\"}', '2020-05-12 10:44:37'),
('433b14746b03cf79a808effcd8', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"sC4ehMEUQ3WWYYP3pYByUqS2NHefwGUR\"}', '2018-03-21 10:28:21'),
('43b16e71443325e74f0ccb38e3', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-04-23 07:12:28'),
('452d27aeba6740732cda1b9812', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-26 01:46:12'),
('474444a37b50341dafe5cc1d37', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-06 12:44:02'),
('4aeccda5e074b27578da465011', '{\"user_id\":\"1\",\"user_token\":\"Nq34xIofyLSnp1yHiVMPYrm46ardgr5a\"}', '2018-04-30 08:36:45'),
('4cd4fc0b7bea13eff42e9fab98', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"GMbpqc3ceYfviyWr4WVt44Pi18hDKyaG\"}', '2018-03-13 04:53:17'),
('4e5b5c7f61d46ceeed4c674ef0', '{\"api_id\":\"3\"}', '2020-05-12 10:43:57'),
('4ebf3ae5c5d95b4e8a8cd6fe02', 'false', '2020-05-21 15:48:01'),
('4ef6fe85a19a857e3ced6cd205', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\",\"customer_id\":\"5\",\"customer\":{\"customer_id\":\"5\",\"customer_group_id\":\"1\",\"firstname\":\"Dimbisoa \",\"lastname\":\"RAKOTONARIVO\",\"email\":\"sandratrarakotonarivo@yahoo.fr\",\"telephone\":\"0332848333\",\"custom_field\":[]},\"payment_address\":{\"firstname\":\"Dimbisoa \",\"lastname\":\"RAZAKA\",\"company\":\"\",\"address_1\":\"Amboditsiry\",\"address_2\":\"Analamahitsy\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":[]},\"payment_methods\":{\"cod\":{\"code\":\"cod\",\"title\":\"Paiement \\u00e0 la livraison\",\"terms\":\"\",\"sort_order\":\"5\"}},\"shipping_address\":{\"firstname\":\"Dimbisoa \",\"lastname\":\"RAZAKA\",\"company\":\"\",\"address_1\":\"Amboditsiry\",\"address_2\":\"Analamahitsy\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":[]},\"shipping_methods\":{\"flat\":{\"title\":\"Forfait\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Exp\\u00e9dition au forfait\",\"cost\":\"2000.00\",\"tax_class_id\":\"0\",\"text\":\"2 000,00Ar\"}},\"sort_order\":\"1\",\"error\":false}},\"payment_method\":{\"code\":\"cod\",\"title\":\"Paiement \\u00e0 la livraison\",\"terms\":\"\",\"sort_order\":\"5\"},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Exp\\u00e9dition au forfait\",\"cost\":\"2000.00\",\"tax_class_id\":\"0\",\"text\":\"2 000,00Ar\"}}', '2020-05-15 07:22:20'),
('509f8324a10979ebee73708f8f', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-09 14:04:46'),
('51715578bf9eb288fbe2d00627', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-06 07:07:11'),
('52546fccf6bf86a1ace49b5c2f', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"lmz5eQNNBsEn2m2dS4uu3T93tuuKJsgw\"}', '2019-04-08 14:24:31'),
('531eda79e3cc6c261bd425bb69', '{\"api_id\":\"3\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-05-12 08:38:12'),
('55924598f9a1b3bedd59073df1', 'false', '2018-03-19 01:44:00'),
('56f568456323f45105a880f727', '{\"api_id\":\"3\",\"language\":\"en-gb\",\"currency\":\"MGA\"}', '2020-05-18 07:06:33'),
('5a073c29fb63ab443967f95311', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"0WF2vjgp68g1ycjbN8wJo14X7BLLYpV3\"}', '2018-05-29 06:20:52'),
('5a199c91d6199fafefd635cf2d', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-17 08:10:00'),
('5b4beeb59ee8f6f29e76010fe5', '{\"language\":\"ar\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"0YkYHhXVuoZmPfUdxs5E49pFZIC2xV7b\",\"compare\":[\"36\"],\"wishlist\":[\"36\"],\"customer_id\":\"2\"}', '2019-04-08 13:27:07'),
('5b7f68a832de9d17ae5eba394d', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-30 06:38:20'),
('5b80297111efbae3fcebccafb2', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-09 13:01:11'),
('5d00fc86abed58bb8ee8cfb5c8', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"account\":\"register\",\"customer_id\":\"4\",\"payment_address\":{\"address_id\":\"3\",\"firstname\":\"Miandrisoa Sandratr\",\"lastname\":\"RAKOTONARI\",\"company\":\"\",\"address_1\":\"amboditsiry\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"3\",\"firstname\":\"Miandrisoa Sandratr\",\"lastname\":\"RAKOTONARI\",\"company\":\"\",\"address_1\":\"amboditsiry\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null},\"user_id\":\"3\",\"user_token\":\"eforiUSMqGyglY1wxAyaIvluAEnXarso\"}', '2020-05-14 13:08:41'),
('5ecfcadfdb8d1804a0264d9a64', '{\"api_id\":\"3\",\"language\":\"en-gb\",\"currency\":\"MGA\"}', '2020-05-18 07:06:57'),
('5f17f666a1d5d5448e838166f4', '{\"api_id\":\"3\"}', '2020-05-12 13:32:07'),
('65a22adc371864d5808b2e4c62', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-26 01:46:11'),
('690359535dc7ff618973975211', '{\"api_id\":\"3\"}', '2020-05-18 06:32:20'),
('69c824164b02076b707ca23522', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-25 08:33:22'),
('6ccaa4141cce0e7f61631e7d83', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"0bwIHY0nBXUxUEkAI4mRpadULLHD0hTt\"}', '2018-05-19 05:14:58'),
('6ccba677e72c267c5d408a70f1', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-19 03:36:07'),
('6cef8613486b8294377532803f', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"BkpfI0XsfWfecCxpcbTmsvksLj7gIKTp\"}', '2018-05-21 03:14:56'),
('6ea8afa0e4ff0e34556439b7a1', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-25 08:44:32'),
('70ac4837656b3505bb6f2b3ea8', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-20 02:01:05'),
('719df2b7b42c6afd3ea3c6846b', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"5i0b47XIyKoCwwwfE8RhI7cbCNDZg3vJ\"}', '2019-03-30 12:00:53'),
('71f432b24c6a560087335c6908', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"wishlist\":[\"28\"],\"compare\":[\"28\"],\"user_id\":\"1\",\"user_token\":\"f41D7AKtaOJlgmdgZ0ECHDqovFQQk5DN\",\"account\":\"guest\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"wew\",\"lastname\":\"test\",\"email\":\"ronil.patel001@yahoo.com\",\"telephone\":\"8855902031\",\"custom_field\":[],\"shipping_address\":\"1\"},\"payment_address\":{\"firstname\":\"wew\",\"lastname\":\"test\",\"company\":\"df\",\"address_1\":\"dfdfdfdfdfd\",\"address_2\":\"dfd\",\"postcode\":\"395010\",\"city\":\"fdf\",\"country_id\":\"220\",\"zone_id\":\"3495\",\"country\":\"Ukraine\",\"iso_code_2\":\"UA\",\"iso_code_3\":\"UKR\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Odes\'ka Oblast\'\",\"zone_code\":\"51\"},\"shipping_address\":{\"firstname\":\"wew\",\"lastname\":\"test\",\"company\":\"df\",\"address_1\":\"dfdfdfdfdfd\",\"address_2\":\"dfd\",\"postcode\":\"395010\",\"city\":\"fdf\",\"country_id\":\"220\",\"zone_id\":\"3495\",\"country\":\"Ukraine\",\"iso_code_2\":\"UA\",\"iso_code_3\":\"UKR\",\"address_format\":\"\",\"zone\":\"Odes\'ka Oblast\'\",\"zone_code\":\"51\",\"custom_field\":[]},\"shipping_methods\":{\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"$5.00\"}},\"sort_order\":\"1\",\"error\":false}},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"$5.00\"},\"comment\":\"\",\"payment_methods\":{\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"}},\"payment_method\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"5\"},\"order_id\":2}', '2019-04-10 09:52:58'),
('748132c7b281b4c4953a5a7388', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"kCIWhQ5mNVc2ZJ1ekcIPvPxs368eceFx\"}', '2018-03-27 09:53:33'),
('749500cfd0177ef6457eadc78f', 'false', '2018-03-27 01:39:19'),
('784b51b4b84446f0603ea49249', 'false', '2018-03-24 02:53:06'),
('78c334e6aa1040ae5c69c5e26d', '{\"api_id\":\"3\"}', '2020-05-12 10:39:58'),
('793733de68884c93204babe9e7', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"Rk2Ik5eCqb30Ag8tw7PWZwQvdx0pqjtJ\",\"install\":\"vZP9g3deL7\"}', '2018-03-14 09:59:57'),
('7df131a86ddafecf3bda9dca1a', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-25 03:00:48'),
('7dfccdfefb607463012b381146', '{\"api_id\":\"3\"}', '2020-05-18 07:07:20'),
('7eb02653744c42d07b799ec2eb', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-04-11 10:08:44'),
('7f20dc855624e13c2b579205cf', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"tZCs8q3rTJCBOnoan9Ob5yxiR4ioygZX\"}', '2019-03-04 12:32:19'),
('7f79baedfb15f6ae73f714e975', '{\"api_id\":\"3\"}', '2020-05-15 07:23:10'),
('7f8fc961e8b63bd5e95bfcabc6', '{\"user_id\":\"1\",\"user_token\":\"sPzvl2jbqfZoDD6nvrtrthyZJuuoIRqV\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-29 10:18:18'),
('7fc99ed912795c2470e649493f', '{\"api_id\":\"3\"}', '2020-05-12 10:47:16'),
('802effc37d8374d57749cd8efd', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-16 10:29:09'),
('80ed2090df45c38c507046bfbb', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"5BPEriVlRjn9M6VM7TweHL2Dlr56rM1H\",\"install\":\"xwRXHFdYMG\"}', '2018-03-23 10:16:12'),
('812b32958ae6db228b65dc8e25', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"compare\":[\"41\"],\"wishlist\":[\"41\"]}', '2019-03-25 13:13:50'),
('81d28a0c1ee9e0152b1f6e83b6', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"jvCkJeI6t1WwpbYQ0LIlLeZWDpFQfSBy\"}', '2019-03-05 07:37:59'),
('81e49f053ae5d04ec7af5f1d36', '{\"api_id\":\"3\"}', '2020-05-15 07:22:55'),
('81e502e1d854b49a7b280b7d80', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-04-13 08:23:48'),
('82f65902abc20b7f4c7b37bada', '{\"user_id\":\"1\",\"user_token\":\"lSnuV1qTDY5lkX6jaQx2MKAUMMYfjKLa\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-12 10:55:58'),
('83bd91ab0d03d351396161f009', 'false', '2018-03-26 01:46:12'),
('8538bf8bead1fb10d3a10b3ae5', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-07 04:39:28'),
('8955657ba810d7b7fe14a1aeb7', '{\"user_id\":\"1\",\"user_token\":\"jM9V4DM1RhgKQgYKjKEVat05fNdyV0Su\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-05 11:22:46'),
('8c743ae28607488504d60bef9e', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-10 06:38:55'),
('8da1874de0e4573145775c060b', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"0Ak9Smzy5zuW4aKDvn6kE5afZK2G6YCL\"}', '2020-05-18 15:16:08'),
('8e39af55bbb8ab13f2fb1b297b', '{\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-21 12:58:31'),
('8e46355b69109d041e8014c77b', '{\"api_id\":\"3\"}', '2020-05-15 12:55:37'),
('8fa54662c262c8b069cdf91dc2', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"customer_id\":\"2\",\"shipping_address\":false}', '2019-04-10 07:54:34'),
('900e78a51fdd27d5f190b6db27', '{\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-21 07:23:16'),
('90dd16d790b86d716f9614b678', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"JjZhujo97Y0ggz5ptXqKNefnJD6gLXlr\"}', '2018-05-24 08:32:50'),
('91483138640460cadd285b83ba', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"CQBiA50vE5hXnP2MimVVtRH4HXaZVf1G\"}', '2018-05-16 09:19:57'),
('91582fa3318b6c60543d6cb782', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-26 10:53:36'),
('94f9b671c7655dac09ca298cd4', '{\"api_id\":\"3\"}', '2020-05-14 12:56:40'),
('96696e653516a1757972c83956', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"raNsXOLEd8psz74TJe3KQJVDDLaFnEWu\"}', '2019-03-06 06:10:43'),
('97d5c9c5152e3016fa85a7e4c7', '{\"api_id\":\"3\"}', '2020-05-15 07:23:16'),
('99b6a88d6c7567dc5576c62f18', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"dAxyUircmvcKCymGvtairKkKibJ5qon5\"}', '2018-03-26 10:12:04'),
('99e34c4ffd85d2f2f13787a323', 'false', '2018-05-25 03:00:17'),
('9a0eabc4bdb6b2a4b5e559a6ac', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"RuwDz7a2n6FU4xnQ0rlIyVHsMfrHJEhf\"}', '2019-03-01 14:50:36'),
('9abb4e5c503a603ae3a41881f6', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-30 01:43:53'),
('9c3ad156a55fc05a582d73a584', 'false', '2020-05-15 06:42:49'),
('9cf489c675360d7077f01d6ae8', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"Zyy1ApLPszvvXHoDPxYhHXfKic8qrnTU\",\"account\":\"register\",\"customer_id\":\"6\",\"payment_address\":{\"address_id\":\"5\",\"firstname\":\"Jisia\",\"lastname\":\"Rakotonarivo\",\"company\":\"aaaa\",\"address_1\":\"ambohitrarahaba\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"5\",\"firstname\":\"Jisia\",\"lastname\":\"Rakotonarivo\",\"company\":\"aaaa\",\"address_1\":\"ambohitrarahaba\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null}}', '2020-05-15 17:30:38'),
('9e1a49faabdbb262d4d7648ef7', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-17 08:34:17'),
('a0f97c29533d096299cce597fa', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-03 06:15:30'),
('a11910bc0d354f47bf1c0ad9b0', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-01 09:23:01'),
('a572df33c18e59b0f397368c7d', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-06 04:37:30'),
('a5c959e92519797459523d1fc2', '{\"api_id\":\"3\"}', '2020-05-12 08:38:33'),
('a81704589aaf32013fb7f6e074', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-12 10:40:55'),
('a90af826bb0832650c0856022c', '{\"api_id\":\"3\",\"language\":\"en-gb\",\"currency\":\"MGA\"}', '2020-05-18 07:06:10'),
('a97efbd0225c5615b0be26afba', '{\"user_id\":\"1\",\"user_token\":\"dJ6EfHa6cbDbBrS7Bvtbizl2fqSl2lix\"}', '2020-05-15 17:37:28'),
('a9be639fcb3a6699763a41a458', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"jy4uYxJIy4WJjblAFJaulIt7O7UBkqFw\"}', '2018-04-23 06:57:27'),
('abc17337a4ef592dd20836afcc', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-04-12 06:34:54'),
('abde0d7133e0e1e1890974394b', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-19 05:18:09'),
('abdf380704c4d8650e926255e7', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"wishlist\":[\"28\"],\"compare\":[\"28\"]}', '2019-03-02 13:15:44'),
('ac1b18a09d5c855a61245a5fa7', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"iEH9lLOxk439WYR8cSDzBWpxGOhhti3I\"}', '2020-05-13 11:19:49'),
('ac3d71fc8dfc3cf2a9404adc82', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"compare\":[\"48\"],\"customer_id\":\"7\"}', '2020-05-16 08:19:00'),
('aca15327410962e583e8340798', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"wishlist\":[\"28\"],\"compare\":[\"28\"],\"user_id\":\"1\",\"user_token\":\"5UtIpOmJsMmK79jDntNRdyMpD50laheg\"}', '2019-04-09 13:24:47'),
('ae2c28854f27e504dcd8e3ca1c', '{\"api_id\":\"3\"}', '2020-05-14 12:53:33'),
('af37a1c0e2a1cda7691545d9de', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"4fJDOvsxRHV8VizNmkDhy1C5HzfsRJ2q\"}', '2018-03-17 04:57:36'),
('af9816759eaa61a17d62dd0ef0', '{\"user_id\":\"1\",\"user_token\":\"VceCWUIb0he7OuUjnNhzyCE98ATMbivq\",\"language\":\"mg-MG\",\"currency\":\"MGA\"}', '2020-05-23 07:27:58'),
('b0ae549848527b7585c098cad6', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-19 04:05:59'),
('b20575232c02bcff7649068b28', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-30 02:47:59'),
('b302a9c11120286b24ac05ec96', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-10 06:36:30'),
('b4653908684e6b5d31b25f40af', '{\"api_id\":\"3\"}', '2020-05-18 06:33:37'),
('b60226301accd692bf7a8ed11e', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-13 13:53:42'),
('b6c61e2084240a11c1caafe771', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-16 08:08:24'),
('b87b818091f209fdf174a40a2d', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-25 04:17:33'),
('bbdaa39a416334653674d9a0b3', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-19 03:23:42'),
('bf47185d8c2d9102e814700fd6', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"wCOROlnd3tV2qrG8NykeSWJhER90S974\"}', '2019-04-09 13:56:25'),
('bf5423933c342e125310f7386e', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-24 02:53:03'),
('bfa7b718fc425ec28d519816d7', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"8793DL5Jem8Z3wAm94GeilCnwnwpMQ9i\"}', '2020-05-20 10:52:54'),
('c0737c226625dfb2628eee2471', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-21 02:26:52'),
('c0b2cb9c6834f59968d7d9c952', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-10 06:12:33'),
('c1e068622627767aebab8994d3', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-02 09:46:48'),
('c224071304bfeb2da132cec22a', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-02 04:17:42'),
('c2292fcae7c4a0b9612bdf115c', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-12 13:33:39'),
('c2a33f4f7bac05dcbe919ffce1', '{\"api_id\":\"3\"}', '2020-05-12 10:46:54'),
('c495b083360f803dc136417ca5', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-27 05:40:39'),
('c693e428bd0998bd8aa08a8f57', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"H2NmCvHI9auGtuPYGZGuHspcCre6AJNH\",\"customer_id\":\"1\",\"shipping_address\":{\"address_id\":\"1\",\"firstname\":\"rio\",\"lastname\":\"Austin\",\"company\":\"\",\"address_1\":\"rio\",\"address_2\":\"\",\"postcode\":\"rio\",\"city\":\"rio\",\"zone_id\":\"3516\",\"zone\":\"Angus\",\"zone_code\":\"AGS\",\"country_id\":\"222\",\"country\":\"United Kingdom\",\"iso_code_2\":\"GB\",\"iso_code_3\":\"GBR\",\"address_format\":\"\",\"custom_field\":null}}', '2018-03-22 10:23:13'),
('c7532abc40c8cc57ed3e345cab', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"K4PwjRU4h8ukb7bJME6FXr9E1nof4tnp\"}', '2018-03-24 05:08:20'),
('c929d5a2109c247b00ce48ecfb', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-29 10:01:28'),
('caad1cfb2cb162b1e4145cc545', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-26 10:53:23'),
('cb20ee644d7b3033bcb3d7fe02', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-08 10:34:42'),
('cb30d99f838461b0c2eb8d3bff', '{\"api_id\":\"3\"}', '2020-05-15 07:24:25'),
('cb52f70bd08134e96c25bbe772', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-04-30 08:34:32'),
('cc3a36d2b178025d077db67b46', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-25 08:08:10'),
('cd1870fed32a532c59f6adba57', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"GuNy1E9VtFPfwWP26qmz5yO6VBGHHf5C\",\"customer_id\":\"5\",\"shipping_address\":{\"address_id\":\"4\",\"firstname\":\"Dimbisoa \",\"lastname\":\"RAZAKA\",\"company\":\"\",\"address_1\":\"Amboditsiry\",\"address_2\":\"Analamahitsy\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null},\"payment_address\":{\"address_id\":\"4\",\"firstname\":\"Dimbisoa \",\"lastname\":\"RAZAKA\",\"company\":\"\",\"address_1\":\"Amboditsiry\",\"address_2\":\"Analamahitsy\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null}}', '2020-05-15 07:24:55'),
('cd2c587b33687ca8403a6b598d', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"Va4CM0HXfTewzfRInFdzdpsW6LDPJGau\"}', '2019-03-25 04:30:40'),
('cd4370bd64f62381daed898689', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-10 06:37:08'),
('ce916d39a7c0e29c4edba9c0b6', 'false', '2018-05-29 03:15:35'),
('cfe946bacee9771e30462729d5', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"uNG6xx0Sl1jjWj1sz62JbYXAali0JpI0\"}', '2018-03-16 10:05:12'),
('d1d0313a3ad9cb386831ffc163', '{\"api_id\":\"3\"}', '2020-05-15 13:12:50'),
('d2e04a2daa5bd35763e5595966', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"9EkFOpUfZSEUeJSz0YLeJu1J0h9nQUvR\"}', '2020-05-16 14:50:09'),
('d4f2c05d41fdfc80c22e58c57d', 'false', '2018-03-23 01:58:49'),
('d596e156044bc9ba56ae98d95f', '{\"api_id\":\"3\"}', '2020-05-15 16:50:42'),
('d65ad893d41da8c4f0209ed443', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-29 03:15:43'),
('d72b0753ceba0297fc88a58edf', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"iUdeWWl58BDoj12mzFruCMSPbikwzWaS\",\"account\":\"register\",\"customer_id\":\"8\",\"payment_address\":{\"address_id\":\"6\",\"firstname\":\"Saro\",\"lastname\":\"RAMANANTOANINA\",\"company\":\"\",\"address_1\":\"Anosy\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"6\",\"firstname\":\"Saro\",\"lastname\":\"RAMANANTOANINA\",\"company\":\"\",\"address_1\":\"Anosy\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null}}', '2020-05-18 08:04:29'),
('d74eeac39bb1e3a04cd6b73832', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"i6fy1Bz8hBaNdLZnAuKlQBx21AO7fKfN\",\"customer_id\":\"3\",\"shipping_address\":{\"address_id\":\"2\",\"firstname\":\"Miandrisoa Sandratra\",\"lastname\":\"RAKOTONARIVO\",\"company\":\"\",\"address_1\":\"Amboditsiry\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null},\"payment_address\":{\"address_id\":\"2\",\"firstname\":\"Miandrisoa Sandratra\",\"lastname\":\"RAKOTONARIVO\",\"company\":\"\",\"address_1\":\"Amboditsiry\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"Antananarivo\",\"zone_id\":\"1938\",\"zone\":\"Antananarivo\",\"zone_code\":\"AN\",\"country_id\":\"127\",\"country\":\"Madagascar\",\"iso_code_2\":\"MG\",\"iso_code_3\":\"MDG\",\"address_format\":\"\",\"custom_field\":null},\"compare\":[\"41\",\"40\"]}', '2020-05-12 14:56:17'),
('d8a8717edf23d19d6ec5b0fee4', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-30 02:45:52'),
('d8bf2785b66c9b7019e00fa988', '{\"user_id\":\"1\",\"user_token\":\"NwrQkof5EYcwfA6c5mNhpvyK6leXpNly\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-30 08:54:04'),
('dafd7a6ee8240dedfc5a661bc7', '{\"api_id\":\"3\"}', '2020-05-18 07:05:23'),
('db726c5e784f67880e9515e3cb', '{\"api_id\":\"3\"}', '2020-05-15 16:52:14'),
('db8eae13aaa0e5aaaece80e283', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-19 05:19:23'),
('dc96cc62ed15342f1a4e35eda7', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"9xxjM1tLOFm78gj44IWxATQpcdE89UWG\",\"customer_id\":\"1\",\"payment_address\":{\"address_id\":\"1\",\"firstname\":\"rio\",\"lastname\":\"Austin\",\"company\":\"\",\"address_1\":\"rio\",\"address_2\":\"\",\"postcode\":\"rio\",\"city\":\"rio\",\"zone_id\":\"3516\",\"zone\":\"Angus\",\"zone_code\":\"AGS\",\"country_id\":\"222\",\"country\":\"United Kingdom\",\"iso_code_2\":\"GB\",\"iso_code_3\":\"GBR\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"1\",\"firstname\":\"rio\",\"lastname\":\"Austin\",\"company\":\"\",\"address_1\":\"rio\",\"address_2\":\"\",\"postcode\":\"rio\",\"city\":\"rio\",\"zone_id\":\"3516\",\"zone\":\"Angus\",\"zone_code\":\"AGS\",\"country_id\":\"222\",\"country\":\"United Kingdom\",\"iso_code_2\":\"GB\",\"iso_code_3\":\"GBR\",\"address_format\":\"\",\"custom_field\":null}}', '2018-03-19 10:13:32'),
('de69ddf20c273d287ea5d6f7d2', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-29 09:06:07'),
('df1cbb53e4718f98718e99bce6', '{\"api_id\":\"3\"}', '2020-05-12 13:40:16'),
('dfdf20783f7d3e1084f56f8010', '{\"user_id\":\"1\",\"user_token\":\"R9ulvmawFToIc2yDYdY9QrcfzBN5cZpp\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-17 08:52:17'),
('e0a9dcebaee37a5a0582a07d65', '{\"api_id\":\"3\"}', '2020-05-14 11:27:03'),
('e0b6891704227e586447bfcdde', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-30 02:38:34'),
('e1577189d28cecd4924d84282b', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"compare\":[\"32\",\"47\"]}', '2019-04-04 05:12:25'),
('e1a5c065d9c22b4d6bf890fa21', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-19 03:34:44'),
('e20a7a27394e4bc854f5883b1d', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"acZta7ibyyWZBRpzfHfLgQA2aboZehhy\"}', '2019-03-04 13:02:51'),
('e27382ed986ae14003c20123de', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-15 07:23:49'),
('e30ff92da65946ac2122a3bbb1', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-30 02:57:21'),
('e400cb817307d5e75f909c25b3', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-21 02:27:35'),
('e644816378da95ea5f7a91ee12', '{\"language\":\"fr-FR\",\"currency\":\"MGA\",\"user_id\":\"1\",\"user_token\":\"fbtXmJOTN15vAcbrcozYC2uZfbRIcoWS\"}', '2020-05-13 18:30:53'),
('e66899c3f77f710b74defdf510', '{\"api_id\":\"3\",\"language\":\"en-gb\",\"currency\":\"MGA\"}', '2020-05-18 07:05:49'),
('e7382b3a7a13f32242ff058af1', '{\"api_id\":\"3\"}', '2020-05-15 17:06:39'),
('e7e993a7fefea4295f493d73a5', 'false', '2019-03-07 04:02:37'),
('e86b4b00ea71139180a153722f', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-30 09:39:28'),
('e8c893fd2093f45bf91fe5acf5', '{\"api_id\":\"3\"}', '2020-05-15 07:10:24'),
('e9caf4cb36b22b4003d5d7e012', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"bTK9SDadYTZtD8lNeNCEmqi5XHJ5z2On\",\"install\":\"pNwVtXpZUR\"}', '2018-03-20 10:05:43'),
('edbb8aae2382faffa78f522277', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"cP95uXeFY1QDbrIUBqGSci8skwOMC3Lb\",\"install\":\"GQsvSfUVmx\"}', '2018-05-22 06:08:48'),
('eddb5f765ddd524b859a231c55', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-21 02:29:32'),
('eeb004199a6b2ca20a6085944e', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-03-30 01:52:17'),
('ef4aade0bea5af4f99c282c0ad', '{\"api_id\":\"3\"}', '2020-05-15 07:22:32'),
('f074f8036d00d252549dea6f77', '{\"api_id\":\"3\"}', '2020-05-15 13:34:40'),
('f108e4a0e1bfe09f50f4d0f5fb', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-18 05:09:31'),
('f15b496034d86ef6973ff7d57d', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-04 08:23:39'),
('f1781c614e66d467200c615e45', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-15 12:55:27'),
('f1efe67041ac3d0e0881e2edc1', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-19 03:34:38'),
('f256ad3b3cad7bd1f23e4bacd2', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"Nal9QeuyzIjyihRUV5wdJrMp6HwjkVeF\"}', '2018-05-30 07:02:22'),
('f35444e73f0911ee05a5c01551', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-15 07:15:52'),
('f4c3494e93e0c937ca5e3fa229', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-04-08 09:43:19'),
('f51428d9f9d6d258ccd6a63089', '{\"api_id\":\"3\"}', '2020-05-15 17:06:01'),
('f5bd115822426101bc14a8d04b', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"PxirLVhRDmhB80HYVohcqicOd7q876LR\"}', '2019-03-02 13:21:04'),
('f6539ff181252d8b0975a05f03', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"hmDO8VbjOwfHYInLFLH5Pn1l38MTljdA\"}', '2018-03-28 09:58:34'),
('f794213599f457792a6192a36a', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2018-05-26 04:57:44'),
('f7bc342739562f75a591883f4e', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-06 11:50:53'),
('f8409fbbea931d3d54a4f6d05c', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"e6qjYDx3v76Y7FAH5nZvXBNvn0lBVWFK\",\"install\":\"9QoGikqCxe\"}', '2018-05-25 09:18:54'),
('fadd79de823e41f66133a9d26f', '{\"api_id\":\"3\",\"language\":\"fr-FR\",\"currency\":\"MGA\"}', '2020-05-15 13:32:29'),
('fcc73b17f59ae4f8dfb554acbf', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2019-03-06 11:08:18'),
('fec84337c81cd568abddf88ac2', 'false', '2020-05-18 06:22:51');

-- --------------------------------------------------------

--
-- Structure de la table `oc_setting`
--

DROP TABLE IF EXISTS `oc_setting`;
CREATE TABLE IF NOT EXISTS `oc_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6124 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(5518, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(6, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(7, 0, 'voucher', 'total_voucher_status', '1', 0),
(5517, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(6116, 0, 'config', 'config_error_display', '1', 0),
(6117, 0, 'config', 'config_error_log', '1', 0),
(6118, 0, 'config', 'config_error_filename', 'error.log', 0),
(6107, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(6115, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/mp4\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(6113, 0, 'config', 'config_file_max_size', '300000', 0),
(6114, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\nmp4\r\ndoc', 0),
(6108, 0, 'config', 'config_compression', '0', 0),
(6109, 0, 'config', 'config_secure', '0', 0),
(6110, 0, 'config', 'config_password', '1', 0),
(6111, 0, 'config', 'config_shared', '0', 0),
(6112, 0, 'config', 'config_encryption', '95XxtuVk0LhLWEjATaTemP9lC4BQ6NRpp5pZqicdKfnKtsEueaM5mB6oPAATC9Uzwb061laTEfxzLIiZs8RIfg1WOEsTELAcCkTDYXAKKJAygNptafDJPN15fKKImP3R3eCwRdGsKLyjo3bUquXdTNssoWX7o6TYAV2pJTDHdKPdCU96MIOvXjEwiMT7SgoITwqV55QsO8Vsni6MaxlrX1KLxTuxdeYWYgwZ3RkctgwHhOF3NBgzvExPsbY8L9yu2z1bzqhagafebchUxkGeB69PqGecoArGunbdez7oO6wiEy8bg0GvKVXItSuYgHZl61d3BJEUEFKhExk0z48szwYtWoAfDRZfykrYurZ8ImL7PQoekAUIf4X0I4rFbyOVcrFqN74VGJPzfwbQP2ccpWoGo0JMQkkcNeQ0OXLqq4ZrTJj3ZCilK3RBn3nfFk5GXJShTvdulzmeTlff70IvCOFIk3ESI5F26XP3x9ZRueSmk9RCcUMH5qaqnWSwDvxdog0MvjnxyH6CbzP8wZtT4kSIzqV8bj5mgSClTsEeqRtI4O87EfRKQTn4gBjXg4gF72OYC5DzrbSG1Mu8a4uP6hj0xevNu9sgtCUoxdMo9BOHJw7YRB4h5rka7fOi4CtsLvnSAOHP61CxJAQvIXFbSZjgWi4b3Ib2c3Ne2BWPJgdfbJYoVzQnM2QFZM7enHb7YeOusoEnZCfgKM6ZnblOwhgnw9mbCFJ7V5bKJ44jmY7jqHLIg4eu9fRHMw1Hmhnqzig85DrfNQhGIopHDWs7h2Vbw4sFkoybhiOiaqNHo7XDkxNEk1EfZgituAxtI7LDlwfozNqUkiHuwmGoLzQgS7mBjX2W8qR1xOmvocCu5XUMGSD0msnvmW8TWm3mVeC2ShAdULQFHOm5LnDhYejqwX5ILpR85FllgjnrhW8gblg5B7sEPzGhZUlgvzp2o45M1iQJKhi9CoMt3dfe3Zj4cmYsGSsBIQrD2mdCTTqxkQIFJIoq7qKe7rzzQjBMi5Wl', 0),
(64, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(65, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(66, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(67, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(68, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(69, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(70, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(71, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(5516, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(5515, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '0', 0),
(5514, 0, 'shipping_flat', 'shipping_flat_cost', '2000.00', 0),
(77, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(78, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(79, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(80, 0, 'total_tax', 'total_tax_status', '1', 0),
(81, 0, 'total_total', 'total_total_sort_order', '9', 0),
(82, 0, 'total_total', 'total_total_status', '1', 0),
(83, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(84, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(85, 0, 'total_credit', 'total_credit_status', '1', 0),
(86, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(87, 0, 'total_reward', 'total_reward_status', '1', 0),
(88, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(89, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(90, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(91, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(92, 0, 'module_category', 'module_category_status', '1', 0),
(93, 0, 'module_account', 'module_account_status', '1', 0),
(4658, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(4657, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(4656, 0, 'theme_default', 'theme_default_image_cart_height', '125', 0),
(4655, 0, 'theme_default', 'theme_default_image_cart_width', '125', 0),
(4654, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(4653, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(4652, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(4649, 0, 'theme_default', 'theme_default_image_related_width', '315', 0),
(4650, 0, 'theme_default', 'theme_default_image_related_height', '340', 0),
(4651, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(4648, 0, 'theme_default', 'theme_default_image_additional_height', '340', 0),
(113, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(114, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(115, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(116, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(117, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(118, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(119, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(120, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(121, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(122, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(123, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(124, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(125, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(126, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(127, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(128, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(129, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(130, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(131, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(132, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(133, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(134, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(135, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(136, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(137, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(138, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(139, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(140, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(141, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(142, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(143, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(144, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(145, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(146, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(147, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(148, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(149, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(150, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(151, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(152, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(153, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(154, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(155, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(156, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(157, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(158, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(159, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(160, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(161, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(162, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(6106, 0, 'config', 'config_seo_url', '0', 0),
(6105, 0, 'config', 'config_maintenance', '0', 0),
(6104, 0, 'config', 'config_mail_alert_email', '', 0),
(6103, 0, 'config', 'config_mail_alert', '[\"order\"]', 1),
(6101, 0, 'config', 'config_mail_smtp_port', '25', 0),
(6102, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(2660, 0, 'developer', 'developer_theme', '0', 0),
(2661, 0, 'developer', 'developer_sass', '0', 0),
(6100, 0, 'config', 'config_mail_smtp_password', '', 0),
(6099, 0, 'config', 'config_mail_smtp_username', '', 0),
(6097, 0, 'config', 'config_mail_parameter', '', 0),
(6098, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(6096, 0, 'config', 'config_mail_engine', 'mail', 0),
(6095, 0, 'config', 'config_icon', 'catalog/cart.png', 0),
(4647, 0, 'theme_default', 'theme_default_image_additional_width', '315', 0),
(4646, 0, 'theme_default', 'theme_default_image_product_height', '340', 0),
(4645, 0, 'theme_default', 'theme_default_image_product_width', '315', 0),
(4644, 0, 'theme_default', 'theme_default_image_popup_height', '837', 0),
(6094, 0, 'config', 'config_logo', 'catalog/Capture.PNG', 0),
(6093, 0, 'config', 'config_captcha_page', '[\"review\",\"return\",\"contact\"]', 1),
(6091, 0, 'config', 'config_return_status_id', '2', 0),
(6092, 0, 'config', 'config_captcha', '', 0),
(4642, 0, 'theme_default', 'theme_default_image_thumb_height', '837', 0),
(4643, 0, 'theme_default', 'theme_default_image_popup_width', '800', 0),
(5596, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_testimonial_mainimg_width', '200', 0),
(5595, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tabproduct_img_height', '430', 0),
(5594, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tabproduct_img_width', '380', 0),
(5593, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tabproduct_mainimg_height', '200', 0),
(5592, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tabproduct_mainimg_width', '200', 0),
(5591, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmssingleblock_img_height', '68', 0),
(5590, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmssingleblock_img_width', '69', 0),
(5589, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_sliderimage_img_height', '450', 0),
(5588, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_sliderimage_img_width', '1056', 0),
(5587, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_special_img_height', '235', 0),
(5586, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_special_img_width', '235', 0),
(5585, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_specialproduct_mainimg_height', '384', 0),
(5584, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_specialproduct_mainimg_width', '265', 0),
(5583, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_social_img_height', '200', 0),
(5582, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_social_img_width', '200', 0),
(5581, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsproductgridimg_img_height', '928', 0),
(5580, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsproductgridimg_img_width', '800', 0),
(5579, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_payment_img_height', '30', 0),
(5578, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_payment_img_width', '60', 0),
(5577, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_payment_mainimg_height', '200', 0),
(5576, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_payment_mainimg_width', '200', 0),
(5575, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_offerbanner_img_height', '400', 0),
(5574, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_offerbanner_img_width', '1328', 0),
(5573, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_lefttestimonial_img_height', '50', 0),
(5572, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_lefttestimonial_img_width', '50', 0),
(6090, 0, 'config', 'config_return_id', '0', 0),
(6088, 0, 'config', 'config_affiliate_commission', '5', 0),
(6089, 0, 'config', 'config_affiliate_id', '4', 0),
(6087, 0, 'config', 'config_affiliate_auto', '0', 0),
(6085, 0, 'config', 'config_affiliate_group_id', '1', 0),
(6086, 0, 'config', 'config_affiliate_approval', '0', 0),
(6084, 0, 'config', 'config_stock_checkout', '0', 0),
(4641, 0, 'theme_default', 'theme_default_image_thumb_width', '800', 0),
(4640, 0, 'theme_default', 'theme_default_image_category_height', '386', 0),
(4639, 0, 'theme_default', 'theme_default_image_category_width', '1540', 0),
(5571, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_leftproduct_img_height', '105', 0),
(5570, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_leftproduct_img_width', '100', 0),
(5569, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_leftbanner_img_height', '400', 0),
(5568, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_leftbanner_img_width', '241', 0),
(5567, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsnewsletterpobgimg_img_height', '444', 0),
(5566, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsnewsletterpobgimg_img_width', '306', 0),
(5565, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsnewsletterpoimg_img_height', '444', 0),
(5564, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsnewsletterpoimg_img_width', '306', 0),
(5563, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_newproduct_mainimg_height', '200', 0),
(279, 0, 'module_information', 'module_information_status', '1', 0),
(5562, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_newproduct_mainimg_width', '200', 0),
(5561, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_footerproduct_height', '131', 0),
(5560, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_footerproduct_width', '125', 0),
(5559, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_footerlogo_img_height', '52', 0),
(5558, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_footerlogo_img_width', '189', 0),
(5557, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_featureproduct_mainimg_height', '200', 0),
(5556, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_featureproduct_mainimg_width', '200', 0),
(5555, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_customerservice_img_height', '33', 0),
(5554, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_customerservice_img_width', '38', 0),
(4638, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(4635, 0, 'theme_default', 'theme_default_directory', 'opc_electronic_electron_2501', 0),
(5553, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_customerservice_mainimg_height', '200', 0),
(5552, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_customerservice_mainimg_width', '200', 0),
(4637, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(4636, 0, 'theme_default', 'theme_default_status', '1', 0),
(5551, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_categoryslider_img_height', '100', 0),
(5550, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_categoryslider_img_width', '100', 0),
(5549, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_categoryslider_mainimg_height', '200', 0),
(5548, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_categoryslider_mainimg_width', '200', 0),
(5547, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsblogfeasin_img_height', '1000', 0),
(5546, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsblogfeasin_img_width', '956', 0),
(5545, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsblogfeaall_img_height', '1000', 0),
(5544, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsblogfeaall_img_width', '956', 0),
(5543, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsblogfeahome_img_height', '1000', 0),
(5542, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsblogfeahome_img_width', '956', 0),
(5541, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsbloggallsin_img_height', '1000', 0),
(5540, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsbloggallsin_img_width', '956', 0),
(5539, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsbloggallall_img_height', '1000', 0),
(5538, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsbloggallall_img_width', '956', 0),
(5537, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsbloggallhome_img_height', '1000', 0),
(5536, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_tvcmsbloggallhome_img_width', '956', 0),
(5535, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_bestproduct_mainimg_height', '200', 0),
(5534, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_bestproduct_mainimg_width', '200', 0),
(5533, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_brand_img_height', '75', 0),
(5532, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_brand_img_width', '170', 0),
(5531, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_brand_mainimg_height', '200', 0),
(5530, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_brand_mainimg_width', '200', 0),
(5529, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_advanceblock_img_height', '72', 0),
(5528, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_advanceblock_img_width', '73', 0),
(5527, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_advanceblock_sub_mainimg_height', '350', 0),
(5526, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_advanceblock_sub_mainimg_width', '375', 0),
(5521, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_offerbannarmain', '{\"top\":1,\"left\":1}', 1),
(5522, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_appmain', '{\"status\":1}', 1),
(5523, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_custommain', '{\"copyrighttextstatus\":1,\"customtextstatus\":1}', 1),
(5524, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_advanceblock_mainimg_width', '200', 0),
(5525, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_advanceblock_mainimg_height', '200', 0),
(5519, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_status', '1', 0),
(5520, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_configuration', '{\"themeoption\":\"theme1\",\"customthemecolor\":\"Black\",\"boxlayout\":\"0\",\"backgroundtheme\":\"1\",\"backgroundthemecolor\":\"Black\",\"backgroundpattern\":\"pattern14.png\",\"themeoptionstatus\":\"1\",\"pageloader\":\"1\",\"wowjs\":\"1\",\"mousehoverimage\":\"1\",\"tabproductdoublerow\":\"1\",\"productcolor\":\"1\",\"mainmenustickystatus\":\"1\",\"bottomoption\":\"1\",\"doublerow\":\"1\",\"verticalmenuopen\":\"1\",\"bloglimit\":\"3\",\"searchlimit\":\"5\",\"htmlminify\":1,\"comparedisplay\":1,\"wishlistdisplay\":1}', 1),
(6083, 0, 'config', 'config_stock_warning', '0', 0),
(6082, 0, 'config', 'config_stock_display', '0', 0),
(6080, 0, 'config', 'config_fraud_status_id', '6', 0),
(6081, 0, 'config', 'config_api_id', '3', 0),
(6079, 0, 'config', 'config_complete_status', '[\"5\",\"3\"]', 1),
(6078, 0, 'config', 'config_processing_status', '[\"1\",\"2\",\"5\",\"3\"]', 1),
(6077, 0, 'config', 'config_order_status_id', '1', 0),
(6076, 0, 'config', 'config_checkout_id', '5', 0),
(6075, 0, 'config', 'config_checkout_guest', '1', 0),
(6074, 0, 'config', 'config_cart_weight', '1', 0),
(6073, 0, 'config', 'config_invoice_prefix', 'INV-2020-00', 0),
(6072, 0, 'config', 'config_account_id', '3', 0),
(6071, 0, 'config', 'config_login_attempts', '5', 0),
(6070, 0, 'config', 'config_customer_price', '0', 0),
(6069, 0, 'config', 'config_customer_group_display', '[\"1\"]', 1),
(6068, 0, 'config', 'config_customer_group_id', '1', 0),
(6067, 0, 'config', 'config_customer_search', '0', 0),
(6066, 0, 'config', 'config_customer_activity', '0', 0),
(5597, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_testimonial_mainimg_height', '200', 0),
(5598, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_testimonial_img_width', '50', 0),
(5599, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_testimonial_img_height', '50', 0),
(5600, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_testimonial_sing_img_width', '200', 0),
(5601, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_testimonial_sing_img_height', '200', 0),
(5602, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_app_img_width', '150', 0),
(5603, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_app_img_height', '44', 0),
(5604, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_offerbannarsub', '{\"lang_text\":{\"2\":{\"topimg\":\"catalog\\/themevolty\\/customsetting\\/demo_img_1.jpg\",\"leftimg\":\"catalog\\/themevolty\\/customsetting\\/demo_img_2.jpg\"},\"1\":{\"topimg\":\"catalog\\/themevolty\\/customsetting\\/demo_img_1.jpg\",\"leftimg\":\"catalog\\/themevolty\\/customsetting\\/demo_img_2.jpg\"},\"3\":{\"topimg\":\"catalog\\/themevolty\\/customsetting\\/demo_img_1.jpg\",\"leftimg\":\"catalog\\/themevolty\\/customsetting\\/demo_img_2.jpg\"}}}', 1),
(5605, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_appsub', '{\"lang_text\":{\"2\":{\"topimg1_width\":\"150\",\"topimg1_height\":\"44\",\"topimg1\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-1.png\",\"topimg2_width\":\"150\",\"topimg2_height\":\"44\",\"topimg2\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-2.png\",\"link\":\"#\",\"topimg3_width\":\"122\",\"topimg3_height\":\"44\",\"topimg3\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-3.png\",\"google\":\"#\",\"micro\":\"#\"},\"1\":{\"topimg1_width\":\"150\",\"topimg1_height\":\"44\",\"topimg1\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-1.png\",\"topimg2_width\":\"150\",\"topimg2_height\":\"44\",\"topimg2\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-2.png\",\"link\":\"#\",\"topimg3_width\":\"122\",\"topimg3_height\":\"44\",\"topimg3\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-3.png\",\"google\":\"#\",\"micro\":\"#\"},\"3\":{\"topimg1_width\":\"150\",\"topimg1_height\":\"44\",\"topimg1\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-1.png\",\"topimg2_width\":\"150\",\"topimg2_height\":\"44\",\"topimg2\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-2.png\",\"link\":\"#\",\"topimg3_width\":\"122\",\"topimg3_height\":\"44\",\"topimg3\":\"catalog\\/themevolty\\/applicationphoto\\/App-logo-3.png\",\"google\":\"#\",\"micro\":\"#\"}}}', 1),
(5606, 0, 'tvcmscustomsetting', 'tvcmscustomsetting_customsub', '{\"lang_text\":{\"2\":{\"text\":\"\\u00a9 2019 - Ecommerce software by Opencart\\u2122\",\"link\":\"#\",\"nopro\":\"10\",\"newslettertitle\":\"newsletter\",\"newslettersubtitle\":\"Register now to get updates on promotions and coupons.\",\"customtextstatus\":\"1\",\"customtext\":\"India\'s Fastest Online Shopping Destination\"},\"1\":{\"text\":\"\\u00a9 2019 - Ecommerce software by Opencart\\u2122\",\"link\":\"#\",\"nopro\":\"10\",\"newslettertitle\":\"newsletter\",\"newslettersubtitle\":\"Register now to get updates on promotions and coupons.\",\"customtextstatus\":\"1\",\"customtext\":\"India\'s Fastest Online Shopping Destination\"},\"3\":{\"text\":\"\\u00a9 2019 - Ecommerce software by Opencart\\u2122\",\"link\":\"#\",\"nopro\":\"10\",\"newslettertitle\":\"newsletter\",\"newslettersubtitle\":\"Register now to get updates on promotions and coupons.\",\"customtextstatus\":\"1\",\"customtext\":\"India\'s Fastest Online Shopping Destination\"}}}', 1),
(6065, 0, 'config', 'config_customer_online', '0', 0),
(6064, 0, 'config', 'config_tax_customer', 'shipping', 0),
(6063, 0, 'config', 'config_tax_default', 'shipping', 0),
(6062, 0, 'config', 'config_tax', '1', 0),
(6061, 0, 'config', 'config_voucher_max', '1000', 0),
(6060, 0, 'config', 'config_voucher_min', '1', 0),
(6059, 0, 'config', 'config_review_guest', '1', 0),
(6058, 0, 'config', 'config_review_status', '1', 0),
(6057, 0, 'config', 'config_limit_admin', '20', 0),
(6056, 0, 'config', 'config_product_count', '1', 0),
(6055, 0, 'config', 'config_length_class_id', '1', 0),
(6054, 0, 'config', 'config_currency_auto', '1', 0),
(6053, 0, 'config', 'config_currency', 'MGA', 0),
(6052, 0, 'config', 'config_admin_language', 'fr-FR', 0),
(6051, 0, 'config', 'config_language', 'fr-FR', 0),
(6050, 0, 'config', 'config_zone_id', '1938', 0),
(6049, 0, 'config', 'config_country_id', '127', 0),
(6048, 0, 'config', 'config_comment', '', 0),
(6046, 0, 'config', 'config_image', '', 0),
(6047, 0, 'config', 'config_open', '08:00 AM To 06:00 PM', 0),
(6045, 0, 'config', 'config_fax', '(012) 800 456 789', 0),
(6044, 0, 'config', 'config_telephone', '034 00 000 00', 0),
(6042, 0, 'config', 'config_geocode', '', 0),
(6043, 0, 'config', 'config_email', 'contact@laytsenabe.com', 0),
(6041, 0, 'config', 'config_address', '450 Boulevard ratsimandrava sonierana Antananarivo 101', 0),
(6040, 0, 'config', 'config_owner', 'Lay Tsena B.com', 0),
(6039, 0, 'config', 'config_name', 'Lay Tsena B.com', 0),
(6038, 0, 'config', 'config_layout_id', '4', 0),
(6037, 0, 'config', 'config_theme', 'default', 0),
(6036, 0, 'config', 'config_meta_keyword', '', 0),
(6035, 0, 'config', 'config_meta_description', 'My Store', 0),
(6034, 0, 'config', 'config_meta_title', 'Lay Tsena B.com', 0);

-- --------------------------------------------------------

--
-- Structure de la table `oc_shipping_courier`
--

DROP TABLE IF EXISTS `oc_shipping_courier`;
CREATE TABLE IF NOT EXISTS `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`shipping_courier_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_shipping_courier`
--

INSERT INTO `oc_shipping_courier` (`shipping_courier_id`, `shipping_courier_code`, `shipping_courier_name`) VALUES
(1, 'dhl', 'DHL'),
(2, 'fedex', 'Fedex'),
(3, 'ups', 'UPS'),
(4, 'royal-mail', 'Royal Mail'),
(5, 'usps', 'United States Postal Service'),
(6, 'auspost', 'Australia Post'),
(7, 'citylink', 'Citylink');

-- --------------------------------------------------------

--
-- Structure de la table `oc_simple_blog_category`
--

DROP TABLE IF EXISTS `oc_simple_blog_category`;
CREATE TABLE IF NOT EXISTS `oc_simple_blog_category` (
  `simple_blog_category_id` int(16) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `parent_id` int(16) NOT NULL,
  `top` tinyint(1) NOT NULL,
  `blog_category_column` int(16) NOT NULL,
  `external_link` text NOT NULL,
  `column` int(8) NOT NULL,
  `sort_order` int(8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`simple_blog_category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_simple_blog_category`
--

INSERT INTO `oc_simple_blog_category` (`simple_blog_category_id`, `image`, `parent_id`, `top`, `blog_category_column`, `external_link`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, '', 0, 0, 0, '', 10, 0, 1, '2017-08-25 14:18:26', '2017-11-16 16:18:41'),
(2, '', 0, 0, 0, '', 10, 0, 1, '2017-08-25 14:18:51', '2017-11-16 16:18:55'),
(3, '', 0, 0, 0, '', 10, 0, 1, '2017-08-25 14:19:12', '2017-11-16 16:19:10'),
(4, '', 0, 0, 0, '', 10, 0, 1, '2017-08-25 14:19:25', '2017-11-16 16:19:23'),
(5, '', 0, 0, 0, '', 10, 0, 1, '2017-08-25 14:19:52', '2017-11-16 16:19:34');

-- --------------------------------------------------------

--
-- Structure de la table `oc_simple_blog_category_description`
--

DROP TABLE IF EXISTS `oc_simple_blog_category_description`;
CREATE TABLE IF NOT EXISTS `oc_simple_blog_category_description` (
  `simple_blog_category_description_id` int(16) NOT NULL AUTO_INCREMENT,
  `simple_blog_category_id` int(16) NOT NULL,
  `language_id` int(16) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(256) NOT NULL,
  `meta_keyword` varchar(256) NOT NULL,
  PRIMARY KEY (`simple_blog_category_description_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_simple_blog_category_description`
--

INSERT INTO `oc_simple_blog_category_description` (`simple_blog_category_description_id`, `simple_blog_category_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`) VALUES
(16, 1, 1, 'Demo Category 1', '', '', ''),
(15, 1, 2, 'Demo Category 1 ', '', '', ''),
(18, 2, 1, 'Demo Category 2', '', '', ''),
(17, 2, 2, 'Demo Category 2', '', '', ''),
(20, 3, 1, 'Demo Category 3', '', '', ''),
(19, 3, 2, 'Demo Category 3', '', '', ''),
(22, 4, 1, 'Demo Category 4', '', '', ''),
(21, 4, 2, 'Demo Category 4', '', '', ''),
(24, 5, 1, 'Demo Category 5', '', '', ''),
(23, 5, 2, 'Demo Category 5', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `oc_soconfig`
--

DROP TABLE IF EXISTS `oc_soconfig`;
CREATE TABLE IF NOT EXISTS `oc_soconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `key` varchar(255) NOT NULL,
  `value` mediumtext NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=71321 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_soconfig`
--

INSERT INTO `oc_soconfig` (`id`, `store_id`, `key`, `value`, `serialized`) VALUES
(71317, 0, 'soconfig_pages_store', '{\"catalog_col_position\":\"outside\",\"catalog_col_type\":\"default\",\"catalog_sidebar_sticky\":\"disable\",\"product_catalog_refine\":\"2\",\"catalog_refine_number\":\"5\",\"lstimg_cate_status\":\"1\",\"product_catalog_mode\":\"grid-4\",\"discount_status\":\"1\",\"countdown_status\":\"1\",\"rating_status\":\"1\",\"product_order\":\"1\",\"card_gallery\":\"2\",\"placeholder_status\":\"1\",\"placeholder_img\":\"\",\"desktop_addcart_position\":\"left\",\"quick_status\":\"1\",\"desktop_addcart_status\":\"1\",\"desktop_wishlist_status\":\"1\",\"desktop_Compare_status\":\"1\",\"radio_style\":\"1\",\"thumbnails_position\":\"bottom\",\"product_enablezoom\":\"1\",\"product_enablesizechart\":\"0\",\"tabs_position\":\"2\",\"product_enableshowmore\":\"0\",\"product_enableshipping\":\"1\",\"product_contentshipping\":{\"2\":\"\",\"1\":\"\"},\"product_page_button\":\"1\",\"product_socialshare\":{\"2\":\"&lt;div class=&quot;title-share&quot;&gt;Share This&lt;\\/div&gt;\\r\\n&lt;div class=&quot;wrap-content&quot;&gt;&lt;div class=&quot;addthis_inline_share_toolbox&quot;&gt;&lt;\\/div&gt;&lt;\\/div&gt;\",\"1\":\"&lt;div class=&quot;title-share&quot;&gt;Share This&lt;\\/div&gt;\\r\\n&lt;div class=&quot;wrap-content&quot;&gt;&lt;div class=&quot;addthis_inline_share_toolbox&quot;&gt;&lt;\\/div&gt;&lt;\\/div&gt;\"},\"related_status\":\"1\",\"product_related_column_lg\":\"5\",\"product_related_column_md\":\"3\",\"product_related_column_sm\":\"3\",\"product_related_column_xs\":\"1\",\"comingsoon_imglogo\":\"\",\"comingsoon_title\":{\"2\":\"\",\"1\":\"\"},\"comingsoon_date\":\"\",\"comingsoon_content\":{\"2\":\"\",\"1\":\"\"}}', 1),
(71314, 0, 'soconfig_general_store', '{\"typelayout\":\"1\",\"themecolor\":\"orange\",\"toppanel_status\":\"1\",\"toppanel_type\":\"3\",\"phone_status\":\"1\",\"contact_number\":{\"2\":\"&lt;b&gt;Hotline:&lt;\\/b&gt;(801) 2345 - 6789\",\"1\":\"&lt;b&gt;Hotline:&lt;\\/b&gt;(801) 2345 - 6789\"},\"welcome_message_status\":\"1\",\"welcome_message\":{\"2\":\"&lt;div class=&quot;list-msg&quot;&gt; &lt;div class=&quot;list-msg--item&quot;&gt;&lt;label class=&quot;label-msg&quot;&gt;This week&lt;\\/label&gt; &lt;a href=&quot;#&quot;&gt;Sale special too good gear category&lt;\\/a&gt;&lt;\\/div&gt; &lt;div class=&quot;list-msg--item&quot;&gt;&lt;label class=&quot;label-msg&quot;&gt;Tomorrow&lt;\\/label&gt; &lt;a href=&quot;#&quot;&gt;Laten ipsum dolor sit amet.In gravida pellentesque&lt;\\/a&gt;&lt;\\/div&gt; &lt;\\/div&gt;\",\"1\":\"&lt;div class=&quot;list-msg&quot;&gt; &lt;div class=&quot;list-msg--item&quot;&gt;&lt;label class=&quot;label-msg&quot;&gt;This week&lt;\\/label&gt; &lt;a href=&quot;#&quot;&gt;Sale special too good gear category&lt;\\/a&gt;&lt;\\/div&gt; &lt;div class=&quot;list-msg--item&quot;&gt;&lt;label class=&quot;label-msg&quot;&gt;Tomorrow&lt;\\/label&gt; &lt;a href=&quot;#&quot;&gt;Laten ipsum dolor sit amet.In gravida pellentesque&lt;\\/a&gt;&lt;\\/div&gt; &lt;\\/div&gt;\"},\"checkout_status\":\"1\",\"lang_status\":\"1\",\"preloader\":\"1\",\"preloader_animation\":\"line\",\"backtop\":\"1\",\"imgpayment_status\":\"1\",\"imgpayment\":\"catalog\\/demo\\/payment\\/payment-4.png\",\"copyright\":\"So Ladaz \\u00a9 2016 - {year} Opencart Themes Demo Store. All Rights Reserved. Designed by &lt;a href=&quot;http:\\/\\/www.opencartworks.com\\/&quot; target=&quot;_blank&quot;&gt;OpenCartWorks.Com&lt;\\/a&gt;\",\"typeheader\":\"1\",\"typefooter\":\"1\",\"type_banner\":\"7\",\"img_sizechart\":\"\"}', 1),
(71318, 0, 'soconfig_fonts_store', '{\"body_status\":\"google\",\"normal_body\":\"Arial, Helvetica, sans-serif\",\"url_body\":\"https:\\/\\/fonts.googleapis.com\\/css?family=Roboto:300,400,400i,500,500i,700\",\"family_body\":\"\'Roboto\', sans-serif\",\"selector_body\":\"body, #wrapper\",\"menu_status\":\"standard\",\"normal_menu\":\"inherit\",\"url_menu\":\"https:\\/\\/fonts.googleapis.com\\/css?family=Roboto:300,400,500,700\",\"family_menu\":\"\'Roboto\', sans-serif\",\"selector_menu\":\"\",\"heading_status\":\"standard\",\"normal_heading\":\"inherit\",\"url_heading\":\"\",\"family_heading\":\"\",\"selector_heading\":\"\"}', 1),
(71319, 0, 'soconfig_social_store', '{\"social_sidebar\":\"0\",\"social_fb_status\":\"0\",\"facebook\":\"magentech\",\"social_twitter_status\":\"0\",\"twitter\":\"smartaddons\",\"social_custom_status\":\"0\",\"video_code\":{\"2\":\"&lt;h3&gt;Guide Create Social Wickgets&amp;nbsp;&lt;\\/h3&gt;\\r\\n&lt;p&gt;You please login admin, then select\\r\\n&lt;span style=&quot;font-weight: bold;&quot;&gt;&amp;nbsp;Extension tab &amp;gt; Modules &amp;gt; Theme Control Panel &amp;gt; Tab Social Wickgets:&lt;\\/span&gt;\\r\\nPlease enter data content&lt;\\/p&gt;\",\"1\":\"&lt;h3&gt;Guide Create Social Wickgets&amp;nbsp;&lt;\\/h3&gt;\\r\\n&lt;p&gt;You please login admin, then select\\r\\n&lt;span style=&quot;font-weight: bold;&quot;&gt;&amp;nbsp;Extension tab &amp;gt; Modules &amp;gt; Theme Control Panel &amp;gt; Tab Social Wickgets:&lt;\\/span&gt;\\r\\nPlease enter data content&lt;\\/p&gt;\"}}', 1),
(71320, 0, 'soconfig_custom_store', '{\"cssinput_status\":\"0\",\"cssinput_content\":\"\",\"cssfile_status\":\"0\",\"cssfile_url\":[\"catalog\\/view\\/theme\\/so-revo\\/css\\/custom.css\"],\"jsinput_status\":\"0\",\"jsinput_content\":\"\"}', 1),
(71315, 0, 'soconfig_advanced_store', '{\"name_color\":\"yellow\",\"theme_color\":\"#ff9600\",\"scsscompile\":\"0\",\"scssformat\":\"Nested\",\"compileMutiColor\":\"0\",\"cssminify\":\"0\",\"cssExclude\":[{\"namecss\":\"catalog\\/view\\/javascript\\/font-awesome\\/css\\/font-awesome.min.css\"}],\"jsminify\":\"0\"}', 1),
(71316, 0, 'soconfig_layout_store', '{\"layoutstyle\":\"full\",\"theme_bgcolor\":\"#7a167a\",\"pattern\":\"6\",\"contentbg\":\"\",\"content_bg_mode\":\"repeat\",\"content_attachment\":\"scroll\"}', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_statistics`
--

DROP TABLE IF EXISTS `oc_statistics`;
CREATE TABLE IF NOT EXISTS `oc_statistics` (
  `statistics_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL,
  PRIMARY KEY (`statistics_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '138875.0000'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Structure de la table `oc_stock_status`
--

DROP TABLE IF EXISTS `oc_stock_status`;
CREATE TABLE IF NOT EXISTS `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`stock_status_id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 3, 'Ao amin\'ny staoky'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 3, '2-3 Andro'),
(7, 1, 'In Stock'),
(6, 1, '2-3 Days'),
(7, 2, 'En Stock'),
(8, 2, 'Pré-commander'),
(5, 2, 'En rupture de stock'),
(6, 2, '2-3 Jours'),
(5, 3, 'Ritra'),
(8, 3, 'Kaomandy mialoha');

-- --------------------------------------------------------

--
-- Structure de la table `oc_store`
--

DROP TABLE IF EXISTS `oc_store`;
CREATE TABLE IF NOT EXISTS `oc_store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_tax_class`
--

DROP TABLE IF EXISTS `oc_tax_class`;
CREATE TABLE IF NOT EXISTS `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`tax_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Structure de la table `oc_tax_rate`
--

DROP TABLE IF EXISTS `oc_tax_rate`;
CREATE TABLE IF NOT EXISTS `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`tax_rate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Structure de la table `oc_tax_rate_to_customer_group`
--

DROP TABLE IF EXISTS `oc_tax_rate_to_customer_group`;
CREATE TABLE IF NOT EXISTS `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`tax_rate_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_tax_rule`
--

DROP TABLE IF EXISTS `oc_tax_rule`;
CREATE TABLE IF NOT EXISTS `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tax_rule_id`)
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Structure de la table `oc_theme`
--

DROP TABLE IF EXISTS `oc_theme`;
CREATE TABLE IF NOT EXISTS `oc_theme` (
  `theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`theme_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_translation`
--

DROP TABLE IF EXISTS `oc_translation`;
CREATE TABLE IF NOT EXISTS `oc_translation` (
  `translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmsbrandlist`
--

DROP TABLE IF EXISTS `oc_tvcmsbrandlist`;
CREATE TABLE IF NOT EXISTS `oc_tvcmsbrandlist` (
  `tvcmsbrandlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmsbrandlist_link` varchar(255) NOT NULL,
  `tvcmsbrandlist_img` varchar(255) NOT NULL,
  `tvcmsbrandlist_status` int(11) NOT NULL,
  `tvcmsbrandlist_pos` int(11) NOT NULL,
  `tvcmsbrandlist_lang` text NOT NULL,
  PRIMARY KEY (`tvcmsbrandlist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmsbrandlist`
--

INSERT INTO `oc_tvcmsbrandlist` (`tvcmsbrandlist_id`, `tvcmsbrandlist_link`, `tvcmsbrandlist_img`, `tvcmsbrandlist_status`, `tvcmsbrandlist_pos`, `tvcmsbrandlist_lang`) VALUES
(1, '#', 'catalog/demo/product/logo/logo dzama 2.jpg', 1, 1, 'null'),
(2, '#', 'catalog/demo/product/logo/logo big.jpg', 1, 2, 'null'),
(3, '#', 'catalog/demo/product/logo/logo salone.png', 1, 3, 'null'),
(4, '#', 'catalog/demo/product/logo/logo habibo groupe.jpg', 1, 4, 'null'),
(5, '#', 'catalog/demo/product/logo/logo savonerie tropicale.png', 1, 5, 'null'),
(6, '#', 'catalog/demo/product/logo/logo mellis.jpg', 1, 6, 'null');

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmscategoryproductmain`
--

DROP TABLE IF EXISTS `oc_tvcmscategoryproductmain`;
CREATE TABLE IF NOT EXISTS `oc_tvcmscategoryproductmain` (
  `tvcmscategoryproductmain_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmscategoryproductmain_pos` int(11) DEFAULT NULL,
  `tvcmscategoryproduct_status` int(11) DEFAULT NULL,
  `tvcmscategoryproduct_numberofproduct` int(11) DEFAULT NULL,
  `tvcmscategoryproduct_categoryselect` int(11) DEFAULT NULL,
  `tvcmscategoryproduct_img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tvcmscategoryproductmain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmscategoryproductmain`
--

INSERT INTO `oc_tvcmscategoryproductmain` (`tvcmscategoryproductmain_id`, `tvcmscategoryproductmain_pos`, `tvcmscategoryproduct_status`, `tvcmscategoryproduct_numberofproduct`, `tvcmscategoryproduct_categoryselect`, `tvcmscategoryproduct_img`) VALUES
(1, 1, 1, 10, 20, 'catalog/themevolty/categoryproduct/demo_img_1.png'),
(3, 3, 1, 10, 18, 'catalog/themevolty/categoryproduct/demo_img_3.png');

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmscategoryproductsub`
--

DROP TABLE IF EXISTS `oc_tvcmscategoryproductsub`;
CREATE TABLE IF NOT EXISTS `oc_tvcmscategoryproductsub` (
  `tvcmscategoryproductsub_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmscategoryproductmain_id` int(11) NOT NULL,
  `tvcmscategoryproductsublang_id` int(11) NOT NULL,
  `tvcmscategoryproduct_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tvcmscategoryproductsub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmscategoryproductsub`
--

INSERT INTO `oc_tvcmscategoryproductsub` (`tvcmscategoryproductsub_id`, `tvcmscategoryproductmain_id`, `tvcmscategoryproductsublang_id`, `tvcmscategoryproduct_title`) VALUES
(25, 3, 1, 'Clothes'),
(26, 3, 2, 'Clothes'),
(41, 1, 1, 'Electronic'),
(42, 1, 2, 'Beauty');

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmscategoryslidermain`
--

DROP TABLE IF EXISTS `oc_tvcmscategoryslidermain`;
CREATE TABLE IF NOT EXISTS `oc_tvcmscategoryslidermain` (
  `tvcmscategoryslidermain_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmscategoryslidermain_category_id` int(11) DEFAULT NULL,
  `tvcmscategoryslidermain_pos` int(11) DEFAULT NULL,
  `tvcmscategoryslidermain_image` varchar(100) DEFAULT NULL,
  `tvcmscategoryslidermain_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`tvcmscategoryslidermain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmscategoryslidermain`
--

INSERT INTO `oc_tvcmscategoryslidermain` (`tvcmscategoryslidermain_id`, `tvcmscategoryslidermain_category_id`, `tvcmscategoryslidermain_pos`, `tvcmscategoryslidermain_image`, `tvcmscategoryslidermain_status`) VALUES
(1, 20, 1, 'catalog/themevolty/categoryslider/BigCola.gif', 1),
(3, 18, 3, 'catalog/themevolty/categoryslider/600px-PILI_PILI_DOCK_004.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmscategoryslidersub`
--

DROP TABLE IF EXISTS `oc_tvcmscategoryslidersub`;
CREATE TABLE IF NOT EXISTS `oc_tvcmscategoryslidersub` (
  `tvcmscategoryslidersub_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmscategoryslidermain_id` int(11) NOT NULL,
  `tvcmscategoryslidersublang_id` int(11) NOT NULL,
  `tvcmscategoryslidersub_name` varchar(255) DEFAULT NULL,
  `tvcmscategoryslidersub_des` text,
  PRIMARY KEY (`tvcmscategoryslidersub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmscategoryslidersub`
--

INSERT INTO `oc_tvcmscategoryslidersub` (`tvcmscategoryslidersub_id`, `tvcmscategoryslidermain_id`, `tvcmscategoryslidersublang_id`, `tvcmscategoryslidersub_name`, `tvcmscategoryslidersub_des`) VALUES
(50, 1, 2, 'Boisson', ''),
(51, 1, 1, 'Boisson', ''),
(52, 1, 3, 'Zava-pisotro', ''),
(53, 3, 2, 'PPN', ''),
(54, 3, 1, 'PPN', ''),
(55, 3, 3, 'PPN', '');

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmspaymenticonmain`
--

DROP TABLE IF EXISTS `oc_tvcmspaymenticonmain`;
CREATE TABLE IF NOT EXISTS `oc_tvcmspaymenticonmain` (
  `tvcmspaymenticonmain_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmspaymenticonmain_pos` int(11) DEFAULT NULL,
  `tvcmspaymenticonmain_image` varchar(255) DEFAULT NULL,
  `tvcmspaymenticonmain_link` varchar(255) DEFAULT NULL,
  `tvcmspaymenticon_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`tvcmspaymenticonmain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmspaymenticonmain`
--

INSERT INTO `oc_tvcmspaymenticonmain` (`tvcmspaymenticonmain_id`, `tvcmspaymenticonmain_pos`, `tvcmspaymenticonmain_image`, `tvcmspaymenticonmain_link`, `tvcmspaymenticon_status`) VALUES
(1, 1, 'catalog/themevolty/paymenticon/téléchargement.png', '#', 1),
(2, 2, 'catalog/themevolty/paymenticon/unnamed.png', '#', 1),
(3, 3, 'catalog/themevolty/paymenticon/orange-money-600x450.jpg', '#', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmspaymenticonsub`
--

DROP TABLE IF EXISTS `oc_tvcmspaymenticonsub`;
CREATE TABLE IF NOT EXISTS `oc_tvcmspaymenticonsub` (
  `tvcmspaymenticonsub_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmspaymenticonmain_id` int(11) NOT NULL,
  `tvcmspaymenticonsublang_id` int(11) NOT NULL,
  `tvcmspaymenticonsub_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tvcmspaymenticonsub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmspaymenticonsub`
--

INSERT INTO `oc_tvcmspaymenticonsub` (`tvcmspaymenticonsub_id`, `tvcmspaymenticonmain_id`, `tvcmspaymenticonsublang_id`, `tvcmspaymenticonsub_title`) VALUES
(15, 1, 2, 'Mvola'),
(16, 1, 1, 'Mvola'),
(17, 1, 3, 'Mvola'),
(21, 2, 2, 'Airtel'),
(22, 2, 1, 'Airtel'),
(23, 2, 3, 'Airtel'),
(24, 3, 2, 'Orange'),
(25, 3, 1, 'Orange'),
(26, 3, 3, 'Orange');

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmssocialiconmain`
--

DROP TABLE IF EXISTS `oc_tvcmssocialiconmain`;
CREATE TABLE IF NOT EXISTS `oc_tvcmssocialiconmain` (
  `tvcmssocialiconmain_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmssocialiconmain_pos` int(11) DEFAULT NULL,
  `tvcmssocialiconmain_class_name` varchar(255) DEFAULT NULL,
  `tvcmssocialiconmain_link` varchar(255) DEFAULT NULL,
  `tvcmssocialicon_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`tvcmssocialiconmain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmssocialiconmain`
--

INSERT INTO `oc_tvcmssocialiconmain` (`tvcmssocialiconmain_id`, `tvcmssocialiconmain_pos`, `tvcmssocialiconmain_class_name`, `tvcmssocialiconmain_link`, `tvcmssocialicon_status`) VALUES
(1, 1, 'facebook', '#', 1),
(2, 2, 'twitter', '#', 1),
(3, 3, 'googleplus', '#', 1),
(4, 4, 'youtube', '#', 1),
(5, 5, 'vimeo', '#', 1),
(6, 6, 'pinterest', '#', 1),
(7, 7, 'rss', '#', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcmssocialiconsub`
--

DROP TABLE IF EXISTS `oc_tvcmssocialiconsub`;
CREATE TABLE IF NOT EXISTS `oc_tvcmssocialiconsub` (
  `tvcmssocialiconsub_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcmssocialiconmain_id` int(11) NOT NULL,
  `tvcmssocialiconsublang_id` int(11) NOT NULL,
  `tvcmssocialiconsub_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tvcmssocialiconsub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `oc_tvcmssocialiconsub`
--

INSERT INTO `oc_tvcmssocialiconsub` (`tvcmssocialiconsub_id`, `tvcmssocialiconmain_id`, `tvcmssocialiconsublang_id`, `tvcmssocialiconsub_title`) VALUES
(15, 1, 1, 'Title1'),
(16, 1, 2, 'Title1'),
(17, 2, 1, 'Title2'),
(18, 2, 2, 'Title2'),
(19, 3, 1, 'Title3'),
(20, 3, 2, 'Title3'),
(21, 4, 1, 'Title4'),
(22, 4, 2, 'Title4'),
(23, 5, 1, 'Title5'),
(24, 5, 2, 'Title5'),
(25, 6, 1, 'Title6'),
(26, 6, 2, 'Title6'),
(27, 7, 1, 'Title7'),
(28, 7, 2, 'Title7');

-- --------------------------------------------------------

--
-- Structure de la table `oc_tvcustomlink`
--

DROP TABLE IF EXISTS `oc_tvcustomlink`;
CREATE TABLE IF NOT EXISTS `oc_tvcustomlink` (
  `tvcustomlink_id` int(11) NOT NULL AUTO_INCREMENT,
  `tvcustomlink_title` varchar(100) DEFAULT NULL,
  `tvcustomlink_link` varchar(100) DEFAULT NULL,
  `tvcustomlink_pos` int(11) DEFAULT NULL,
  `tvcustomlink_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`tvcustomlink_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_upload`
--

DROP TABLE IF EXISTS `oc_upload`;
CREATE TABLE IF NOT EXISTS `oc_upload` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`upload_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_user`
--

DROP TABLE IF EXISTS `oc_user`;
CREATE TABLE IF NOT EXISTS `oc_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', '50fc303db4dd5a034f8f481a810f34a3a3266e47', 'bn2Y31GqO', 'John', 'Doe', 'syraxrakotonarivo@gmail.com', '', '', '::1', 1, '2020-05-12 09:02:57'),
(2, 10, 'pao', '45ae64377459d7396aacceec2769bff1b3702e28', 'bfyqXmYbq', 'Dimbisoa Jisia', 'RAMANANTOANINA', 'dimbisoajisia@gmail.com', '', '', '::1', 1, '2020-05-14 09:56:06'),
(3, 11, 'gestion', '5e023dce493a89d06ba999f8a3a83cd0366c7c66', '2S2MbwTUc', 'Rinah', 'RAZAKA', 'rinah@gmail.com', '', '', '::1', 1, '2020-05-14 09:58:43');

-- --------------------------------------------------------

--
-- Structure de la table `oc_user_group`
--

DROP TABLE IF EXISTS `oc_user_group`;
CREATE TABLE IF NOT EXISTS `oc_user_group` (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/tvcmsadvanceblock\",\"extension\\/module\\/tvcmsbanners\",\"extension\\/module\\/tvcmsblog\",\"extension\\/module\\/tvcmsblogcategory\",\"extension\\/module\\/tvcmsbrandlist\",\"extension\\/module\\/tvcmscategoryslider\",\"extension\\/module\\/tvcmscommentlist\",\"extension\\/module\\/tvcmscustomerservices\",\"extension\\/module\\/tvcmscustomlink\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmscustomtext\",\"extension\\/module\\/tvcmsfooterlogo\",\"extension\\/module\\/tvcmsfooterproduct\",\"extension\\/module\\/tvcmsimagegallery\",\"extension\\/module\\/tvcmsimageslider\",\"extension\\/module\\/tvcmsleftbanner\",\"extension\\/module\\/tvcmsleftproduct\",\"extension\\/module\\/tvcmslefttestimonial\",\"extension\\/module\\/tvcmsmap\",\"extension\\/module\\/tvcmsmultibanner\",\"extension\\/module\\/tvcmsnewsletter\",\"extension\\/module\\/tvcmsnewsletterlist\",\"extension\\/module\\/tvcmsnewsletterpopup\",\"extension\\/module\\/tvcmspaymenticon\",\"extension\\/module\\/tvcmssingleblock\",\"extension\\/module\\/tvcmssocialicon\",\"extension\\/module\\/tvcmsspecialproduct\",\"extension\\/module\\/tvcmsstoretime\",\"extension\\/module\\/tvcmstabproducts\",\"extension\\/module\\/tvcmstags\",\"extension\\/module\\/tvcmstestimonial\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/module\\/tvcmsleftcustomerservices\",\"extension\\/module\\/tvcmsimageslider\",\"extension\\/module\\/tvcmstabproducts\",\"extension\\/module\\/tvcmssingleblock\",\"extension\\/module\\/tvcmssingleblock\",\"extension\\/module\\/tvcmssingleblock\",\"extension\\/module\\/tvcmsspecialproduct\",\"extension\\/module\\/tvcmsmultibanner\",\"extension\\/module\\/tvcmsbanners\",\"extension\\/module\\/tvcmsbanners\",\"extension\\/module\\/tvcmscategoryslider\",\"extension\\/module\\/tvcmscategoryslider\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/module\\/tvcmsleftbanner\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/module\\/tvcmsfooterproduct\",\"extension\\/module\\/tvcmsbrandlist\",\"extension\\/module\\/tvcmscustomerservices\",\"extension\\/module\\/tvcmsfooterlogo\",\"extension\\/module\\/tvcmsnewsletterpopup\",\"extension\\/module\\/tvcmsnewsletter\",\"extension\\/module\\/tvcmsnewsletterlist\",\"extension\\/module\\/tvcmspaymenticon\",\"extension\\/module\\/tvcmsleftproduct\",\"extension\\/module\\/tvcmsleftbanner\",\"extension\\/module\\/tvcmslefttestimonial\",\"extension\\/module\\/tvcmsleftblogpost\",\"extension\\/module\\/tvcmstestimonial\",\"extension\\/module\\/tvcmsblogcategory\",\"extension\\/module\\/tvcmsblog\",\"extension\\/module\\/tvcmscommentlist\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmssocialicon\",\"extension\\/module\\/tvcmssliderofferbanner\",\"extension\\/module\\/tvcmssliderofferbanner\",\"extension\\/module\\/tvcmssliderofferbanner\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmsadvanceblock\",\"extension\\/module\\/tvcmsapplication\",\"extension\\/module\\/tvcmsapplication\",\"extension\\/module\\/tvcmsapplication\",\"extension\\/module\\/tvcmsapplication\",\"extension\\/module\\/information\",\"extension\\/module\\/tvcmstags\",\"extension\\/module\\/tvcmsinstagramslider\",\"extension\\/module\\/tvcmscategoryproduct\",\"extension\\/module\\/tvcmsleftcustomerservices\",\"extension\\/module\\/tvcmsverticalmenu\",\"extension\\/module\\/tvcmsverticalmenu\",\"extension\\/module\\/tvcmsinstagramslider\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/module\\/tvcmsimageslider\",\"extension\\/module\\/tvcmsmultibanner\",\"extension\\/module\\/tvcmssliderofferbanner\",\"extension\\/module\\/tvcmsleftbanner\",\"extension\\/module\\/responsive_slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmsnewsletterpopup\",\"extension\\/module\\/tvcmsnewsletter\",\"extension\\/module\\/tvcmsnewsletterpopup\",\"extension\\/module\\/tvcmsnewsletter\",\"extension\\/module\\/smartsupp\",\"extension\\/module\\/onwebchat\",\"extension\\/module\\/revechat\"],\"modify\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/tvcmsadvanceblock\",\"extension\\/module\\/tvcmsbanners\",\"extension\\/module\\/tvcmsblog\",\"extension\\/module\\/tvcmsblogcategory\",\"extension\\/module\\/tvcmsbrandlist\",\"extension\\/module\\/tvcmscategoryslider\",\"extension\\/module\\/tvcmscommentlist\",\"extension\\/module\\/tvcmscustomerservices\",\"extension\\/module\\/tvcmscustomlink\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmscustomtext\",\"extension\\/module\\/tvcmsfooterlogo\",\"extension\\/module\\/tvcmsfooterproduct\",\"extension\\/module\\/tvcmsimagegallery\",\"extension\\/module\\/tvcmsimageslider\",\"extension\\/module\\/tvcmsleftbanner\",\"extension\\/module\\/tvcmsleftproduct\",\"extension\\/module\\/tvcmslefttestimonial\",\"extension\\/module\\/tvcmsmap\",\"extension\\/module\\/tvcmsmultibanner\",\"extension\\/module\\/tvcmsnewsletter\",\"extension\\/module\\/tvcmsnewsletterlist\",\"extension\\/module\\/tvcmsnewsletterpopup\",\"extension\\/module\\/tvcmspaymenticon\",\"extension\\/module\\/tvcmssingleblock\",\"extension\\/module\\/tvcmssocialicon\",\"extension\\/module\\/tvcmsspecialproduct\",\"extension\\/module\\/tvcmsstoretime\",\"extension\\/module\\/tvcmstabproducts\",\"extension\\/module\\/tvcmstags\",\"extension\\/module\\/tvcmstestimonial\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/module\\/tvcmsleftcustomerservices\",\"extension\\/module\\/tvcmsimageslider\",\"extension\\/module\\/tvcmstabproducts\",\"extension\\/module\\/tvcmssingleblock\",\"extension\\/module\\/tvcmssingleblock\",\"extension\\/module\\/tvcmssingleblock\",\"extension\\/module\\/tvcmsspecialproduct\",\"extension\\/module\\/tvcmsmultibanner\",\"extension\\/module\\/tvcmsbanners\",\"extension\\/module\\/tvcmsbanners\",\"extension\\/module\\/tvcmscategoryslider\",\"extension\\/module\\/tvcmscategoryslider\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/module\\/tvcmsleftbanner\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/module\\/tvcmsfooterproduct\",\"extension\\/module\\/tvcmsbrandlist\",\"extension\\/module\\/tvcmscustomerservices\",\"extension\\/module\\/tvcmsfooterlogo\",\"extension\\/module\\/tvcmsnewsletterpopup\",\"extension\\/module\\/tvcmsnewsletter\",\"extension\\/module\\/tvcmsnewsletterlist\",\"extension\\/module\\/tvcmspaymenticon\",\"extension\\/module\\/tvcmsleftproduct\",\"extension\\/module\\/tvcmsleftbanner\",\"extension\\/module\\/tvcmslefttestimonial\",\"extension\\/module\\/tvcmsleftblogpost\",\"extension\\/module\\/tvcmstestimonial\",\"extension\\/module\\/tvcmsblogcategory\",\"extension\\/module\\/tvcmsblog\",\"extension\\/module\\/tvcmscommentlist\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmssocialicon\",\"extension\\/module\\/tvcmssliderofferbanner\",\"extension\\/module\\/tvcmssliderofferbanner\",\"extension\\/module\\/tvcmssliderofferbanner\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmsadvanceblock\",\"extension\\/module\\/tvcmsapplication\",\"extension\\/module\\/tvcmsapplication\",\"extension\\/module\\/tvcmsapplication\",\"extension\\/module\\/tvcmsapplication\",\"extension\\/module\\/information\",\"extension\\/module\\/tvcmstags\",\"extension\\/module\\/tvcmsinstagramslider\",\"extension\\/module\\/tvcmscategoryproduct\",\"extension\\/module\\/tvcmsleftcustomerservices\",\"extension\\/module\\/tvcmsverticalmenu\",\"extension\\/module\\/tvcmsverticalmenu\",\"extension\\/module\\/tvcmsinstagramslider\",\"extension\\/module\\/tvcmstwoofferbanner\",\"extension\\/module\\/tvcmsimageslider\",\"extension\\/module\\/tvcmsmultibanner\",\"extension\\/module\\/tvcmssliderofferbanner\",\"extension\\/module\\/tvcmsleftbanner\",\"extension\\/module\\/responsive_slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/tvcmscustomsetting\",\"extension\\/module\\/tvcmsnewsletterpopup\",\"extension\\/module\\/tvcmsnewsletter\",\"extension\\/module\\/tvcmsnewsletterpopup\",\"extension\\/module\\/tvcmsnewsletter\",\"extension\\/module\\/smartsupp\",\"extension\\/module\\/onwebchat\",\"extension\\/module\\/revechat\"]}'),
(10, 'PAO-iste', '{\"access\":[\"sale\\/order\"],\"modify\":[\"sale\\/order\"]}'),
(11, 'Gestion Stock', '{\"access\":[\"catalog\\/category\",\"catalog\\/filter\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\"],\"modify\":[\"catalog\\/category\",\"catalog\\/filter\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\"]}');

-- --------------------------------------------------------

--
-- Structure de la table `oc_voucher`
--

DROP TABLE IF EXISTS `oc_voucher`;
CREATE TABLE IF NOT EXISTS `oc_voucher` (
  `voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`voucher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_voucher_history`
--

DROP TABLE IF EXISTS `oc_voucher_history`;
CREATE TABLE IF NOT EXISTS `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`voucher_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `oc_voucher_theme`
--

DROP TABLE IF EXISTS `oc_voucher_theme`;
CREATE TABLE IF NOT EXISTS `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`voucher_theme_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `oc_voucher_theme_description`
--

DROP TABLE IF EXISTS `oc_voucher_theme_description`;
CREATE TABLE IF NOT EXISTS `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`voucher_theme_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General'),
(6, 3, 'Christmas'),
(7, 3, 'Birthday'),
(8, 3, 'General');

-- --------------------------------------------------------

--
-- Structure de la table `oc_weight_class`
--

DROP TABLE IF EXISTS `oc_weight_class`;
CREATE TABLE IF NOT EXISTS `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`weight_class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Structure de la table `oc_weight_class_description`
--

DROP TABLE IF EXISTS `oc_weight_class_description`;
CREATE TABLE IF NOT EXISTS `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  PRIMARY KEY (`weight_class_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound', 'lb'),
(6, 1, 'Ounce', 'oz'),
(1, 3, 'Kilogram', 'kg'),
(2, 3, 'Gram', 'g'),
(5, 3, 'Pound', 'lb'),
(6, 3, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Structure de la table `oc_zone`
--

DROP TABLE IF EXISTS `oc_zone`;
CREATE TABLE IF NOT EXISTS `oc_zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4239 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark\'unik\'', 'GEG', 1),
(184, 11, 'Kotayk\'', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik\'', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots\' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'K', 'KAR', 1),
(201, 14, 'Nieder', 'NOS', 1),
(202, 14, 'Ober', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel\'skaya (Homyel\')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Li', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amap', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Cear', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Esp', 'ES', 1),
(448, 30, 'Goi', 'GO', 1),
(449, 30, 'Maranh', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Par', 'PA', 1),
(454, 30, 'Para', 'PB', 1),
(455, 30, 'Paran', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piau', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rond', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'S', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-Kade', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrova?ko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlova?ka', 'KA', 1),
(852, 53, 'Koprivni?ko-kri', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Li?ko-senjska', 'LS', 1),
(855, 53, 'Me?imurska', 'ME', 1),
(856, 53, 'Osje?ko-baranjska', 'OB', 1),
(857, 53, 'Po', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, '', 'SK', 1),
(860, 53, 'Sisa?ko-moslava?ka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Vara', 'VA', 1),
(864, 53, 'Viroviti?ko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagreba?ka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, '', 'U', 1),
(890, 56, 'Jiho?esk', 'C', 1),
(891, 56, 'Jihomoravsk', 'B', 1),
(892, 56, 'Karlovarsk', 'K', 1),
(893, 56, 'Kr', 'H', 1),
(894, 56, 'Libereck', 'L', 1),
(895, 56, 'Moravskoslezsk', 'T', 1),
(896, 56, 'Olomouck', 'M', 1),
(897, 56, 'Pardubick', 'E', 1),
(898, 56, 'Plze?sk', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'St?edo?esk', 'S', 1),
(901, 56, 'Vyso?ina', 'J', 1),
(902, 56, 'Zl', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan l', 'AL', 1),
(1086, 72, 'Etel', 'ES', 1),
(1087, 72, 'It', 'IS', 1),
(1088, 72, 'L', 'LS', 1),
(1089, 72, 'Lapin l', 'LA', 1),
(1090, 72, 'Oulun l', 'OU', 1),
(1114, 74, 'Ain', '1', 1),
(1115, 74, 'Aisne', '2', 1),
(1116, 74, 'Allier', '3', 1),
(1117, 74, 'Alpes de Haute Provence', '4', 1),
(1118, 74, 'Hautes-Alpes', '5', 1),
(1119, 74, 'Alpes Maritimes', '6', 1),
(1120, 74, 'Ard&egrave;che', '7', 1),
(1121, 74, 'Ardennes', '8', 1),
(1122, 74, 'Ari&egrave;ge', '9', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'Kalimantan Utara', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '35', 1),
(2198, 142, 'Darhan uul', '37', 1),
(2199, 142, 'Hentiy', '39', 1),
(2200, 142, 'Hovsgol', '41', 1),
(2201, 142, 'Hovd', '43', 1),
(2202, 142, 'Uvs', '46', 1),
(2203, 142, 'Tov', '47', 1),
(2204, 142, 'Selenge', '49', 1),
(2205, 142, 'Suhbaatar', '51', 1),
(2206, 142, 'Omnogovi', '53', 1),
(2207, 142, 'Ovorhangay', '55', 1),
(2208, 142, 'Dzavhan', '57', 1),
(2209, 142, 'DundgovL', '59', 1),
(2210, 142, 'Dornod', '61', 1),
(2211, 142, 'Dornogov', '63', 1),
(2212, 142, 'Govi-Sumber', '64', 1),
(2213, 142, 'Govi-Altay', '65', 1),
(2214, 142, 'Bulgan', '67', 1),
(2215, 142, 'Bayanhongor', '69', 1),
(2216, 142, 'Bayan-Olgiy', '71', 1),
(2217, 142, 'Arhangay', '73', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord-Brabant', 'NB', 1),
(2336, 150, 'Noord-Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid-Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul\'yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust\'-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrick', 'BA', 1),
(2926, 189, 'Bratislavsk', 'BR', 1),
(2927, 189, 'Ko', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Pre', 'PR', 1),
(2930, 189, 'Tren?iansky', 'TC', 1),
(2931, 189, 'Trnavsk', 'TV', 1),
(2932, 189, '', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J', 'Z', 1),
(3080, 203, 'J', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '', 'T', 1),
(3085, 203, '', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V', 'S', 1),
(3091, 203, 'V', 'AC', 1),
(3092, 203, 'V', 'Y', 1),
(3093, 203, 'V', 'U', 1),
(3094, 203, 'V', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graub', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Ad?yaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'A?r?', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Ayd?n', 'AYI', 1),
(3326, 215, 'Bal?kesir', 'BAL', 1),
(3327, 215, 'Bart?n', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bing', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, '', 'CKL', 1),
(3337, 215, '', 'CKR', 1),
(3338, 215, '', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbak?r', 'DIY', 1),
(3341, 215, 'D', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elaz??', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eski?ehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'G', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'I?d?r', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, '?stanbul', 'IST', 1),
(3355, 215, '?zmir', 'IZM', 1),
(3356, 215, 'Kahramanmara?', 'KAH', 1),
(3357, 215, 'Karab', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'K?r?kkale', 'KRK', 1),
(3364, 215, 'K?rklareli', 'KLR', 1),
(3365, 215, 'K?r?ehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'K', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Mu?la', 'MUG', 1),
(3374, 215, 'Mu?', 'MUS', 1),
(3375, 215, 'Nev?ehir', 'NEV', 1),
(3376, 215, 'Ni?de', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, '?anl?urfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, '??rnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirda?', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'U?ak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkas\'ka Oblast\'', '71', 1),
(3481, 220, 'Chernihivs\'ka Oblast\'', '74', 1),
(3482, 220, 'Chernivets\'ka Oblast\'', '77', 1),
(3483, 220, 'Crimea', '43', 1),
(3484, 220, 'Dnipropetrovs\'ka Oblast\'', '12', 1),
(3485, 220, 'Donets\'ka Oblast\'', '14', 1),
(3486, 220, 'Ivano-Frankivs\'ka Oblast\'', '26', 1),
(3487, 220, 'Khersons\'ka Oblast\'', '65', 1),
(3488, 220, 'Khmel\'nyts\'ka Oblast\'', '68', 1),
(3489, 220, 'Kirovohrads\'ka Oblast\'', '35', 1),
(3490, 220, 'Kyiv', '30', 1),
(3491, 220, 'Kyivs\'ka Oblast\'', '32', 1),
(3492, 220, 'Luhans\'ka Oblast\'', '9', 1),
(3493, 220, 'L\'vivs\'ka Oblast\'', '46', 1),
(3494, 220, 'Mykolayivs\'ka Oblast\'', '48', 1),
(3495, 220, 'Odes\'ka Oblast\'', '51', 1),
(3496, 220, 'Poltavs\'ka Oblast\'', '53', 1),
(3497, 220, 'Rivnens\'ka Oblast\'', '56', 1),
(3498, 220, 'Sevastopol\'', '40', 1),
(3499, 220, 'Sums\'ka Oblast\'', '59', 1),
(3500, 220, 'Ternopil\'s\'ka Oblast\'', '61', 1),
(3501, 220, 'Vinnyts\'ka Oblast\'', '5', 1),
(3502, 220, 'Volyns\'ka Oblast\'', '7', 1),
(3503, 220, 'Zakarpats\'ka Oblast\'', '21', 1),
(3504, 220, 'Zaporiz\'ka Oblast\'', '23', 1),
(3505, 220, 'Zhytomyrs\'ka oblast\'', '18', 1),
(3506, 221, 'Abu Dhabi', 'ADH', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koro', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kra', '10', 1),
(3966, 190, 'Gori', '11', 1),
(3967, 190, 'Obalno-kra', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '1', 1),
(3974, 242, 'Bar', '2', 1),
(3975, 242, 'Berane', '3', 1),
(3976, 242, 'Bijelo Polje', '4', 1),
(3977, 242, 'Budva', '5', 1),
(3978, 242, 'Cetinje', '6', 1),
(3979, 242, 'Danilovgrad', '7', 1),
(3980, 242, 'Herceg-Novi', '8', 1),
(3981, 242, 'Kola', '9', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nik', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plu', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Ro', '17', 1),
(3990, 242, '', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, '', '21', 1),
(3994, 243, 'Belgrade', '0', 1),
(3995, 243, 'North Ba?ka', '1', 1),
(3996, 243, 'Central Banat', '2', 1),
(3997, 243, 'North Banat', '3', 1),
(3998, 243, 'South Banat', '4', 1),
(3999, 243, 'West Ba?ka', '5', 1),
(4000, 243, 'South Ba?ka', '6', 1),
(4001, 243, 'Srem', '7', 1),
(4002, 243, 'Ma?va', '8', 1),
(4003, 243, 'Kolubara', '9', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Brani?evo', '11', 1),
(4006, 243, '', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaje?ar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Ra', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Ni', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'P?inja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Aina', '661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '320201', 1),
(4038, 117, 'Aizkraukles novads', '320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '640605', 1),
(4040, 117, 'Aizputes novads', '640600', 1),
(4041, 117, 'Akn?ste, Akn?stes novads', '560805', 1),
(4042, 117, 'Akn?stes novads', '560800', 1),
(4043, 117, 'Aloja, Alojas novads', '661007', 1),
(4044, 117, 'Alojas novads', '661000', 1),
(4045, 117, 'Alsungas novads', '624200', 1),
(4046, 117, 'Al?ksne, Al?ksnes novads', '360201', 1),
(4047, 117, 'Al?ksnes novads', '360200', 1),
(4048, 117, 'Amatas novads', '424701', 1),
(4049, 117, 'Ape, Apes novads', '360805', 1),
(4050, 117, 'Apes novads', '360800', 1),
(4051, 117, 'Auce, Auces novads', '460805', 1),
(4052, 117, 'Auces novads', '460800', 1),
(4053, 117, '?da', '804400', 1),
(4054, 117, 'Bab?tes novads', '804900', 1),
(4055, 117, 'Baldone, Baldones novads', '800605', 1),
(4056, 117, 'Baldones novads', '800600', 1),
(4057, 117, 'Balo', '800807', 1),
(4058, 117, 'Baltinavas novads', '384400', 1),
(4059, 117, 'Balvi, Balvu novads', '380201', 1),
(4060, 117, 'Balvu novads', '380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '400201', 1),
(4062, 117, 'Bauskas novads', '400200', 1),
(4063, 117, 'Bever?nas novads', '964700', 1),
(4064, 117, 'Broc?ni, Broc?nu novads', '840605', 1),
(4065, 117, 'Broc?nu novads', '840601', 1),
(4066, 117, 'Burtnieku novads', '967101', 1),
(4067, 117, 'Carnikavas novads', '805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '700807', 1),
(4069, 117, 'Cesvaines novads', '700800', 1),
(4070, 117, 'C?sis, C?su novads', '420201', 1),
(4071, 117, 'C?su novads', '420200', 1),
(4072, 117, 'Ciblas novads', '684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '601009', 1),
(4074, 117, 'Dagdas novads', '601000', 1),
(4075, 117, 'Daugavpils', '50000', 1),
(4076, 117, 'Daugavpils novads', '440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '460201', 1),
(4078, 117, 'Dobeles novads', '460200', 1),
(4079, 117, 'Dundagas novads', '885100', 1),
(4080, 117, 'Durbe, Durbes novads', '640807', 1),
(4081, 117, 'Durbes novads', '640801', 1),
(4082, 117, 'Engures novads', '905100', 1),
(4083, 117, '?rg?u novads', '705500', 1),
(4084, 117, 'Garkalnes novads', '806000', 1),
(4085, 117, 'Grobi?a, Grobi?as novads', '641009', 1),
(4086, 117, 'Grobi?as novads', '641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '500201', 1),
(4088, 117, 'Gulbenes novads', '500200', 1),
(4089, 117, 'Iecavas novads', '406400', 1),
(4090, 117, 'Ik', '740605', 1),
(4091, 117, 'Ik', '740600', 1),
(4092, 117, 'Il?kste, Il?kstes novads', '440807', 1),
(4093, 117, 'Il?kstes novads', '440801', 1),
(4094, 117, 'In?ukalna novads', '801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '321007', 1),
(4096, 117, 'Jaunjelgavas novads', '321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '425700', 1),
(4098, 117, 'Jaunpils novads', '905700', 1),
(4099, 117, 'Jelgava', '90000', 1),
(4100, 117, 'Jelgavas novads', '540200', 1),
(4101, 117, 'J?kabpils', '110000', 1),
(4102, 117, 'J?kabpils novads', '560200', 1),
(4103, 117, 'J?rmala', '130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '901211', 1),
(4106, 117, 'Kandavas novads', '901201', 1),
(4107, 117, 'K?rsava, K?rsavas novads', '681009', 1),
(4108, 117, 'K?rsavas novads', '681000', 1),
(4109, 117, 'Koc?nu novads ,bij. Valmieras)', '960200', 1),
(4110, 117, 'Kokneses novads', '326100', 1),
(4111, 117, 'Kr?slava, Kr?slavas novads', '600201', 1),
(4112, 117, 'Kr?slavas novads', '600202', 1),
(4113, 117, 'Krimuldas novads', '806900', 1),
(4114, 117, 'Krustpils novads', '566900', 1),
(4115, 117, 'Kuld?ga, Kuld?gas novads', '620201', 1),
(4116, 117, 'Kuld?gas novads', '620200', 1),
(4117, 117, '?eguma novads', '741001', 1),
(4118, 117, '?egums, ?eguma novads', '741009', 1),
(4119, 117, '?ekavas novads', '800800', 1),
(4120, 117, 'Lielv?rde, Lielv?rdes novads', '741413', 1),
(4121, 117, 'Lielv?rdes novads', '741401', 1),
(4122, 117, 'Liep?ja', '170000', 1),
(4123, 117, 'Limba', '660201', 1),
(4124, 117, 'Limba', '660200', 1),
(4125, 117, 'L?gatne, L?gatnes novads', '421211', 1),
(4126, 117, 'L?gatnes novads', '421200', 1),
(4127, 117, 'L?v?ni, L?v?nu novads', '761211', 1),
(4128, 117, 'L?v?nu novads', '761201', 1),
(4129, 117, 'Lub?na, Lub?nas novads', '701413', 1),
(4130, 117, 'Lub?nas novads', '701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '680201', 1),
(4132, 117, 'Ludzas novads', '680200', 1),
(4133, 117, 'Madona, Madonas novads', '700201', 1),
(4134, 117, 'Madonas novads', '700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '961011', 1),
(4136, 117, 'Mazsalacas novads', '961000', 1),
(4137, 117, 'M?lpils novads', '807400', 1),
(4138, 117, 'M?rupes novads', '807600', 1),
(4139, 117, 'M?rsraga novads', '887600', 1),
(4140, 117, 'Nauk', '967300', 1),
(4141, 117, 'Neretas novads', '327100', 1),
(4142, 117, 'N?cas novads', '647900', 1),
(4143, 117, 'Ogre, Ogres novads', '740201', 1),
(4144, 117, 'Ogres novads', '740202', 1),
(4145, 117, 'Olaine, Olaines novads', '801009', 1),
(4146, 117, 'Olaines novads', '801000', 1),
(4147, 117, 'Ozolnieku novads', '546701', 1),
(4148, 117, 'P?rgaujas novads', '427500', 1),
(4149, 117, 'P?vilosta, P?vilostas novads', '641413', 1),
(4150, 117, 'P?vilostas novads', '641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '980213', 1),
(4152, 117, 'P?avi?as, P?avi?u novads', '321413', 1),
(4153, 117, 'P?avi?u novads', '321400', 1),
(4154, 117, 'Prei?i, Prei?u novads', '760201', 1),
(4155, 117, 'Prei?u novads', '760202', 1),
(4156, 117, 'Priekule, Priekules novads', '641615', 1),
(4157, 117, 'Priekules novads', '641600', 1),
(4158, 117, 'Prieku?u novads', '427300', 1),
(4159, 117, 'Raunas novads', '427700', 1),
(4160, 117, 'R?zekne', '210000', 1),
(4161, 117, 'R?zeknes novads', '780200', 1),
(4162, 117, 'Riebi?u novads', '766300', 1),
(4163, 117, 'R?ga', '10000', 1),
(4164, 117, 'Rojas novads', '888300', 1),
(4165, 117, 'Ropa', '808400', 1),
(4166, 117, 'Rucavas novads', '648500', 1),
(4167, 117, 'Rug?ju novads', '387500', 1),
(4168, 117, 'Rund?les novads', '407700', 1),
(4169, 117, 'R?jiena, R?jienas novads', '961615', 1),
(4170, 117, 'R?jienas novads', '961600', 1),
(4171, 117, 'Sabile, Talsu novads', '880213', 1),
(4172, 117, 'Salacgr?va, Salacgr?vas novads', '661415', 1),
(4173, 117, 'Salacgr?vas novads', '661400', 1),
(4174, 117, 'Salas novads', '568700', 1),
(4175, 117, 'Salaspils novads', '801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '801211', 1),
(4177, 117, 'Saldus novads', '840200', 1),
(4178, 117, 'Saldus, Saldus novads', '840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '801413', 1),
(4180, 117, 'Saulkrastu novads', '801400', 1),
(4181, 117, 'Seda, Stren?u novads', '941813', 1),
(4182, 117, 'S?jas novads', '809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '801615', 1),
(4184, 117, 'Siguldas novads', '801601', 1),
(4185, 117, 'Skr?veru novads', '328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '621209', 1),
(4187, 117, 'Skrundas novads', '621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '941615', 1),
(4189, 117, 'Smiltenes novads', '941600', 1),
(4190, 117, 'Staicele, Alojas novads', '661017', 1),
(4191, 117, 'Stende, Talsu novads', '880215', 1),
(4192, 117, 'Stopi?u novads', '809600', 1),
(4193, 117, 'Stren?i, Stren?u novads', '941817', 1),
(4194, 117, 'Stren?u novads', '941800', 1),
(4195, 117, 'Subate, Il?kstes novads', '440815', 1),
(4196, 117, 'Talsi, Talsu novads', '880201', 1),
(4197, 117, 'Talsu novads', '880200', 1),
(4198, 117, 'T?rvetes novads', '468900', 1),
(4199, 117, 'Tukuma novads', '900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '900201', 1),
(4201, 117, 'Vai?odes novads', '649300', 1),
(4202, 117, 'Valdem?rpils, Talsu novads', '880217', 1),
(4203, 117, 'Valka, Valkas novads', '940201', 1),
(4204, 117, 'Valkas novads', '940200', 1),
(4205, 117, 'Valmiera', '250000', 1),
(4206, 117, 'Vanga', '801817', 1),
(4207, 117, 'Varak??ni, Varak??nu novads', '701817', 1),
(4208, 117, 'Varak??nu novads', '701800', 1),
(4209, 117, 'V?rkavas novads', '769101', 1),
(4210, 117, 'Vecpiebalgas novads', '429300', 1),
(4211, 117, 'Vecumnieku novads', '409500', 1),
(4212, 117, 'Ventspils', '270000', 1),
(4213, 117, 'Ventspils novads', '980200', 1),
(4214, 117, 'Vies?te, Vies?tes novads', '561815', 1),
(4215, 117, 'Vies?tes novads', '561800', 1),
(4216, 117, 'Vi?aka, Vi?akas novads', '381615', 1),
(4217, 117, 'Vi?akas novads', '381600', 1),
(4218, 117, 'Vi??ni, Vi??nu novads', '781817', 1),
(4219, 117, 'Vi??nu novads', '781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '681817', 1),
(4221, 117, 'Zilupes novads', '681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Kharkivs\'ka Oblast\'', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1),
(4232, 44, 'Qinghai', 'QH', 1),
(4233, 100, 'Papua Barat', 'PB', 1),
(4234, 100, 'Sulawesi Barat', 'SR', 1),
(4235, 100, 'Kepulauan Riau', 'KR', 1),
(4236, 105, 'Barletta-Andria-Trani', 'BT', 1),
(4237, 105, 'Fermo', 'FM', 1),
(4238, 105, 'Monza Brianza', 'MB', 1);

-- --------------------------------------------------------

--
-- Structure de la table `oc_zone_to_geo_zone`
--

DROP TABLE IF EXISTS `oc_zone_to_geo_zone`;
CREATE TABLE IF NOT EXISTS `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`zone_to_geo_zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
