<?php
// Text
$_['text_tax']      				= 'Ex Impôt:';

//product
$_['text_tax']      				= 'Ex Impôt:';
$_['text_quantity']					= 'Qté:';
$_['tv_lang_new_label']				= 'Nouveau';
$_['tv_lang_sale_label']			= 'Vente';
$_['tv_lang_login_label']      		= 'Connexion ou Inscription';
$_['tv_lang_search_label']      	= 'Cherher categories';
$_['tv_lang_search_text_label']     = 'Chercher';
$_['tv_lang_removecart_label']     	= 'retirer du panier';
$_['tv_lang_subtottal_label'] 		= 'Total';
$_['tv_lang_shipping_label'] 		= 'livraison';
$_['tv_lang_tax_label'] 			= 'Impôt';
$_['tv_lang_total_label'] 			= 'Total';
$_['tv_lang_checkout_label'] 		= 'Processus de vérification';
$_['tv_lang_no_add_pro_label'] 		= 'Aucun produit dans le panier';
$_['tv_lang_account']      			= 'Mon compte';

/* webvolty */
$_['tv_lang_store_label']			= 'Stocker des informations';
$_['tv_lang_call_label']			= 'Appelez-nous:';
$_['tv_lang_email_label']			= 'Envoyez-nous un email:';
$_['text_wishlist']					= 'Liste de souhaits';
$_['text_compare']					= 'Comparer';
$_['text_mycart']					= 'Mon panier';
$_['text_myaccount']				= 'Mon compte';
$_['text_scrolltop']				= 'Faire défiler vers le haut';
$_['text_productcompater']			= 'Comparaison de produits';
$_['text_storeinfo']    			= 'INFORMATIONS SUR LE MAGASIN';
$_['text_youraccount']    			= 'COMPTE';
$_['text_ourcompany']    			= 'NOTRE COMPAGNIE';
$_['text_support']    			    = 'Assistance 24X7';
$_['text_seemap']   				= 'Voir la carte';
$_['text_address']   				= 'Adresse:';
$_['text_account_address']			= 'Adresse';
$_['text_phone']   					= 'Phone:';
$_['text_email']  					= 'Email:';
$_['text_product_empty']        	= 'Il ya aucun produit correspondant aux critères de recherche.';
$_['entry_product_search']      	= 'Critères de recherche';
$_['entry_product_description'] 	= 'Rechercher dans les descriptions de produits';
$_['text_product_search']      		= 'Produits répondant aux critères de recherche';
$_['text_product_empty']        	= 'Il ya aucun produit correspondant aux critères de recherche.';
$_['text_storetimemonfri']    		= 'Lun - Ven:';
$_['text_storetimesat']   	     	= 'Sam:';
$_['text_storetimesun']     	   	= 'Dim:';
$_['text_gotop']     	   			= 'Aller en haut';
$_['text_quickview']  	   			= 'Aperçu rapide';
$_['text_viewleftcolumn']  	   		= 'Afficher la colonne de gauche';
$_['text_viewrightcolumn']  	   	= 'Afficher la colonne de droite';
$_['text_grid']  	   				= 'la grille';
$_['text_grid2']  	   				= 'Grille 2';
$_['text_list']  	   				= 'liste';
$_['text_list2']  	   				= 'Liste-2';
$_['text_catalog']  	   			= 'catalogue';
$_['text_contact_us']  	   			= 'Adresse';
$_['text_information']  	   			= 'information';
$_['text_cust_ser']  	   			= 'BESOIN D AIDE ?';
$_['text_tracking']  	   			= 'Veuillez entrer votre numéro de suivi d expédition ci-dessous:';

$_['text_user_title']           	= ' Utilisateur démo';
$_['text_readmore']  	   			= 'Lire la suite';
$_['text_comment']  	   			= 'Commentaire';
$_['text_by']		  	   			= 'Par';
$_['text_allblogs']	  	   			= 'Tous les blogs';
$_['text_blogspagetitle']	  	   	= 'Titre du Blog';
$_['text_blogcountinue']	  	   	= 'Continuer';
$_['text_blogpostby']	  	   		= 'posté par';
$_['text_pagination']       		= 'Affichage de %d à %d de %d (%d pages)';
$_['text_allcomments']       		= 'Tous les commentaires';

$_['text_comment_name']       		= '*votre nom';
$_['text_comment_email']       		= '*Votre email';
$_['text_comment_url']       		= 'URL du site Web (option)';
$_['text_comment_subject']       	= 'Sujet (option)';
$_['text_comment_comment']       	= '*Commentaire';
$_['button_submit']       			= 'Bouton de soumission';

$_['error_name']       				= 'Votre nom doit comprendre entre 3 et 32 ​​caractères!';
$_['error_email']       			= 'L adresse e-mail ne semble pas être valide!';
$_['error_enquiry']       			= 'Le commentaire doit comprendre entre 10 et 3000 caractères!';
$_['button_submit']       			= 'Bouton de soumission';
$_['text_success']       			= 'Ajouter un commentaire avec succès';

$_['text_newsletterbtn']      		= 'Souscrire!';
$_['text_newpopupshow']      		= 'Ne plus afficher ce popup';

$_['text_enter_email']      		= '<div class="ttv-newssubmit-error">
										<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
										<span class="ttv-newssubmit-error-text">Veuillez saisir l e-mail

. </span>
										<span id="closenewsletter">x</span>
										</div>';

$_['text_repeat_email']      		= '<div class="ttv-newssubmit-error">
										<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
										<span class="ttv-newssubmit-error-text">cette adresse email est déjà enregistrée.</span>
										<span id="closenewsletter">x</span>
										</div>';

$_['text_success_email']      		= '<div class="ttv-newssubmit-success">
											<i class="fa fa-check-circle"></i>
											<span class="ttv-newssubmit-success-text">Vous avez souscrit à cette newsletter avec succès. </span>
											<span id="closenewsletter">x</span>
										</div>';

$_['text_error_email']      		= '<div class="ttv-newssubmit-error">
										<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
										<span class="ttv-newssubmit-error-text">Adresse e-mail invalide

.</span>
										<span id="closenewsletter">x</span>
										</div>';
$_['text_place_enter']          	= 'Entrez votre mail...';
$_['text_leave']          			= 'Laisser une réponse';

$_['text_themeoption']      		= 'Option de thème';

$_['text_theme'] 					= 'Thème';
$_['text_custom'] 					= 'Custom';
$_['text_custome_color_1'] 			= 'Couleur personnalisée 1';
$_['text_box_layout'] 				= 'Disposition de la boîte';
$_['text_background_pattern'] 		= 'Motif de fond';

$_['text_bgadmin'] 					= 'Expérience client également disponible en admin.';
$_['text_bgcolor'] 					= 'Couleur de l arrière plan';
$_['text_menusticky'] 				= 'Menu collant';
$_['text_reset'] 					= 'Réinitialiser';
$_['text_menu'] 					= 'Menu';
$_['text_call_us'] 					= 'Phone:';
$_['text_email_us']					= 'Email:';
$_['text_home'] 					= 'Maison';
$_['text_close']					= 'Fermer';
$_['text_moreresult']				= 'Plus de résultats';
$_['text_noproduct']				= 'Aucun produit trouvé';
$_['text_buttonSubscribe']			= 'Souscrire';
$_['text_buttonok']					= 'Ok';
$_['text_newslettercall']					= 'APPEL À SOUTIEN';
$_['text_todayoffer']					= 'L offre du jour';
$_['text_fax']					= 'Impot:';
$_['text_opentime']					= 'Temps ouvert:';

