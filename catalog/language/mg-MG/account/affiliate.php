<?php
// Heading
$_['heading_title']             = 'Kaonty mpiara-miombon&rsquo; antoka amiko';

// Text
$_['text_account']              = 'kaonty';
$_['text_affiliate']            = 'Mpikambana';
$_['text_my_affiliate']         = 'Kaonty mpiara-miombon&rsquo; antoka amiko';
$_['text_payment']              = 'Fomba fandoavam-bola';
$_['text_cheque']               = 'Chèque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Famindram-bola amin&rsquo; ny banky';
$_['text_success']              = 'Fahombiazana: nohavaozina ny kaontinao';
$_['text_agree']                = 'Namaky sy nanaiky ny <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_company']             = 'orinasa';
$_['entry_website']             = 'Adiresy tranonkala';
$_['entry_tax']                 = 'Famantarana ny hetra';
$_['entry_payment']             = 'Fomba fandoavam-bola';
$_['entry_cheque']              = 'Hamarino ny anaran&rsquo; ny mpampiasa tombontsoa';
$_['entry_paypal']              = 'E-mail ny kaonty PayPal';
$_['entry_bank_name']           = 'Banky anarany';
$_['entry_bank_branch_number']  = 'Code ABA/BSB (numéro de succursale)';
$_['entry_bank_swift_code']     = 'Code SWIFT';
$_['entry_bank_account_name']   = 'Anaran&rsquo; ny kaonty';
$_['entry_bank_account_number'] = 'Isan&rsquo; ny kaonty';

// Error
$_['error_agree']               = 'Aza adino: tokony manaiky ianao %s';
$_['error_cheque']              = 'Hamarino ny anaran&rsquo; ny mpampiasa tombontsoa!';
$_['error_paypal']              = 'Ny adiresy mailaka PayPal dia toa tsy manan-kery!';
$_['error_bank_account_name']   = 'Banky anarany';
$_['error_bank_account_number'] = 'Isan&rsquo; ny kaonty takiana											
									!';
$_['error_custom_field']        = '%s ilaina!';
