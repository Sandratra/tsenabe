<?php
// Heading
$_['heading_title']      = 'Ireo teboka tsy mivadika aminao';

// Column
$_['column_date_added']  = 'Daty nidirana';
$_['column_description'] = 'famaritana';
$_['column_points']      = 'Hevitra';

// Text
$_['text_account']       = 'Kaonty';
$_['text_reward']        = 'Hevitra mahatoky';
$_['text_total']         = 'Ny laharam-pankatoavanao tanteraka : ';
$_['text_empty']         = 'Tsy manana teboka tsy mivadika ianao !';
