<?php
// Heading
$_['heading_title']       = 'Kaontiko';

// Text
$_['text_account']        = 'Kaonty';
$_['text_my_account']     = 'Kaontiko';
$_['text_my_orders']      = 'Mes commandes';
$_['text_my_affiliate']   = 'kaonty mpiara-miombon antoka amiko';
$_['text_my_newsletter']  = 'Taratasim-baovao';
$_['text_edit']           = 'Hanova ny fampahalalana kaonty';
$_['text_password']       = 'Ovaina ny teny miafina';
$_['text_address']        = 'Hanova ny fidirana amin ny takelaka';
$_['text_credit_card']    = 'Mitantana karatra fitehirizana voatahiry';
$_['text_wishlist']       = 'Hanova ny lisitry ny fanirianao';
$_['text_order']          = 'jereo ny tantaran d&rsquo; ny filaminana';
$_['text_download']       = 'Téléchargements';
$_['text_reward']         = 'Teboka tsy mivadika';
$_['text_return']         = 'Jereo ny baikonao miverina';
$_['text_transaction']    = 'Ny asanao';
$_['text_newsletter']     = 'Mitantana famandrihana vaovao';
$_['text_recurring']      = 'Ny fandoavana vola';
$_['text_transactions']   = 'Varotra';
$_['text_affiliate_add']  = 'Misoratra anarana amin d&rsquo; ny kaonty miombona antoka';
$_['text_affiliate_edit'] = 'Hanova ny fampahalalana momba anao';
$_['text_tracking']       = 'kaody famantarana manokana';
