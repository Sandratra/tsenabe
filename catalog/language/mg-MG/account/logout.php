<?php
// Heading
$_['heading_title'] = 'Fialana amin&rsquo; ny Kaonty';

// Text
$_['text_message']  = 'Efa nivoaka avy amin&rsquo; ny kaontinao ianao. Azonao atao ny miala am-pelatananao tsy misy atahorana ny fampiasana ny kaontinao. Voavonjy ny haronao, azo jerena ireo vokatra hita ao rehefa miditra amin&rsquo; ny kaontinao ianao .';
$_['text_account']  = 'Kaonty';
$_['text_logout']   = 'Fialana';
