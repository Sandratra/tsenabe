<?php
// Heading
$_['heading_title'] 	 = 'Famerenana ny vokatra';

// Text
$_['text_account']       = 'Kaonty';
$_['text_return']        = 'Miverina ny fampahalalana';
$_['text_return_detail'] = 'Avereno antsipirihany';
$_['text_description']   = 'Azafady mameno ny taratasy eto ambany mba hahazoana isa miverina';
$_['text_order']         = 'Manafatra fampahalalana';
$_['text_product']       = 'Fampahalalana momba ny vokatra';
$_['text_reason']        = 'Antony hiverenana';
$_['text_message']       = '<p>Misaotra anao tamin&rsquo; ny fandefasanao ny fangatahana niverina.Nalefa tany 							amin&rsquo; ireo olona voakasika io mba hokarakaina.</p><p>hahazoana 									 fampandrenesana hatrany ny mombamomba ny fangatahanao ianao amin&rsquo; ny 							 mailaka.</p>';
$_['text_return_id']     = 'Isa hiverenana : ';
$_['text_order_id']      = 'ID de la commande : ';
$_['text_date_ordered']  = 'Datin&rsquo; ny filaminana : ';
$_['text_status']        = 'Toetra : ';
$_['text_date_added']    = 'Daty nanampiana : ';
$_['text_comment']       = 'Hevitra momba ny fiverenana';
$_['text_history']       = 'Tantaran&rsquo; fiverenana';
$_['text_empty']         = 'Mbola tsy niverina ianao teo aloha';
$_['text_agree']         = 'Namaky sy nanaiky ny <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'ID ny fiverenana';
$_['column_order_id']    = 'Commande no.';
$_['column_status']      = 'Sata';
$_['column_date_added']  = 'Daty nanampiana';
$_['column_customer']    = 'Mpanjifa';
$_['column_product']     = 'Anaran&rsquo; ny vokatra';
$_['column_model']       = 'Référence';
$_['column_quantity']    = 'Habetsahana';
$_['column_price']       = 'Vidiny';
$_['column_opened']      = 'Misokatra';
$_['column_comment']     = 'Fanehoan-kevitra';
$_['column_reason']      = 'Antony hiverenana';
$_['column_action']      = 'Hetsika';

// Entry
$_['entry_order_id']     = 'Commande no.';
$_['entry_date_ordered'] = 'Daty nanaovana commande';
$_['entry_firstname']    = 'Anarana voalohany';
$_['entry_lastname']     = 'Anarana farany';
$_['entry_email']        = 'E-mail';
$_['entry_telephone']    = 'Téléfaonina';
$_['entry_product']      = 'Anaran&rsquo; ny vokatra';
$_['entry_model']        = 'Référence';
$_['entry_quantity']     = 'Habetsahana';
$_['entry_reason']       = 'Antony hiverenana';
$_['entry_opened']       = 'Vokatra ';
$_['entry_fault_detail'] = 'Tsininy';

// Error
$_['text_error']         = 'Tsy hita ny fiverenanao !';
$_['error_order_id']     = 'Ilaina ny kaody ID !';
$_['error_firstname']    = 'Ny anarana voalohany dia tsy maintsy misy eo anelanelan&rsquo; ny 1 sy 32 isa !';
$_['error_lastname']     = 'Ny anarana farany dia tokony eo anelanelan&rsquo; ny 1 sy 32 litera !';
$_['error_email']        = 'Ny adiresy mailaka dia toa tsy misy mitombina !';
$_['error_telephone']    = 'Ny laharana an-tariby dia tsy maintsy eo anelanelan&rsquo; ny 3 sy 32 									endri-tsoratra !';
$_['error_product']      = 'Ny anaran&rsquo; ny vokatra dia tokony eo amin&rsquo; ny litera 3 sy 255 !';
$_['error_model']        = 'Ny fanondroana ny vokatra dia tsy maintsy eo anelanelan&rsquo;ny litera 3 sy 64 !' 							;
$_['error_reason']       = 'Tokony hisafidy antony hiverenanao ianao !';
$_['error_agree']        = 'Aza adino: tokony manaiky ianao %s';
