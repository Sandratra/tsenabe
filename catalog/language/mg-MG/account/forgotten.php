<?php
// Heading
$_['heading_title']   = 'Hadinonao ny teny miafinao ?';

// Text
$_['text_account']    = 'Kaonty';
$_['text_forgotten']  = 'Hadino ny tenimiafina';
$_['text_your_email'] = 'E-mail';
$_['text_email']      = 'Ampidiro ny adiresy mailaka mifandraika amin&rsquo; ny kaontinao. Tsindrio alefaso 					hampiasa ny mailakao aminao.';
$_['text_success']    = 'Fahombiazana: ny tenimiafina vaovao dia nalefa any amin&rsquo; ny adiresy mailaka 						anao.';

// Entry
$_['entry_email']     = 'E-mail';
$_['entry_password']  = 'Tenimiafina vaovao';
$_['entry_confirm']   = 'Manamarina';

// Error
$_['error_email']     = 'Azafady azafady: ity adiresy mailaka ity dia tsy hita tao amin&rsquo; ireo 							 antontan-tsahanay, avereno manandrana indray!';
$_['error_approved']  = 'Aza hadino: ny kaontinao dia mitaky fankatoavana alohan&rsquo; ny ahafahanao miditra.						';
$_['error_password']  = 'Ny tenimiafina dia tokony eo anelanelan&rsquo; ny 4 sy 20 ny endriny!';
$_['error_confirm']   = 'Tsy mifanaraka ny teny sy ny fanamafisana !';
