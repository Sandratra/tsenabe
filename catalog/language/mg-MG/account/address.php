<?php
// Heading
$_['heading_title']      = 'Boky adiresy';

// Text
$_['text_account']       = 'Kaonty';
$_['text_address_book']  = 'Fidirana ny boky adiresy';
$_['text_address_add']   = 'Fanampiana adiresy';
$_['text_address_edit']  = 'Fanovana adiresy';
$_['text_add']           = 'Tafiditra soamatsara ny adiresinao';
$_['text_edit']          = 'Voahova soamatsara ny adiresinao';
$_['text_delete']        = 'Voafafa soamatsara ny adiresinao';
$_['text_empty']         = 'tsy nisy tao amin ny kaontinao ny adiresy.';

// Entry
$_['entry_firstname']    = 'Fanampin&rsquo; anarana';
$_['entry_lastname']     = 'Anrana';
$_['entry_company']      = 'Orinasa';
$_['entry_address_1']    = 'Adiresy 1';
$_['entry_address_2']    = 'Adiresy 2';
$_['entry_postcode']     = 'kaody paositra';
$_['entry_city']         = 'tanàna';
$_['entry_country']      = 'Firenena';
$_['entry_zone']         = 'sampan-draharaha';
$_['entry_default']      = 'Adiresy';

// Error
$_['error_delete']       = 'Mariho azafady: tsy maintsy manana adiresy iray farafahakeliny ianao !';
$_['error_default']      = 'Azafady azafady: tsy afaka mamafa ny adiresinao ianao  !';
$_['error_firstname']    = 'Aza hadino fa: ny anarana voalohany dia tokony ho eo amin&rsquo; ny litera 1 sy  						 32 !';
$_['error_lastname']     = 'Aza hadino fa: ny anaram-baovao dia tokony ho eo amin&rsquo; ny litera 1 sy 32 !';
$_['error_address_1']    = 'Aza adino: ny adiresy dia tsy maintsy misy eo amin&rsquo; ny 3 sy 128 ny litera!';
$_['error_postcode']     = 'Azafady: ny kaody paositra dia tsy maintsy eo anelanelan&rsquo; ny 2 sy 10 ny 						    endri-tsoratra!';
$_['error_city']         = 'Marihina: ny anaran&rsquo;ny tanàna dia tsy maintsy eo anelanelan&rsquo; ny 2 sy 	                     128 ny endri-tsoratra !';
$_['error_country']      = 'Aza adino: tokony misafidy firenena ianao!';
$_['error_zone']         = 'Aza adino: tokony misafidy departemanta ianao !';
$_['error_custom_field'] = '%s dia takiana !';
