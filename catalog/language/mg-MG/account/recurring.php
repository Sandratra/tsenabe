<?php
// Heading
$_['heading_title'] 			   = 'Ny fandoavam-bola miverimberina';

// Text
$_['text_account']                         = 'Kaonty';
$_['text_recurring']                       = 'Ny fandoavam-bola miverimberina';
$_['text_recurring_detail']                = 'Ny antsipiriany momba ny fandoavam-bola';
$_['text_order_recurring_id']              = 'Ny fandoavana vola ID: ';
$_['text_date_added']                      = 'Daty fananganana:';
$_['text_status']                          = 'sata : ';
$_['text_payment_method']                  = 'Fomba fandoavam-bola : ';
$_['text_order_id']                        = 'Commande : ';
$_['text_product']                         = 'Vokatra: ';
$_['text_quantity']                        = 'Habetsahana : ';
$_['text_description']                     = 'Famaritana';
$_['text_reference']                       = 'Référence';
$_['text_transaction']                     = 'Varotra';
$_['text_status_1']                        = 'Mavitrika';
$_['text_status_2']                        = 'tsy mavitrika';
$_['text_status_3']                        = 'Nofoanana ';
$_['text_status_4']                        = 'Naato';
$_['text_status_5']                        = 'Fetra fampiasana';
$_['text_status_6']                        = 'Mbola miandry';
$_['text_transaction_date_added']          = 'Daty fananganana';
$_['text_transaction_payment']             = 'fandoavam-bola';
$_['text_transaction_outstanding_payment'] = 'Paiement en suspens';
$_['text_transaction_skipped']             = 'Nesorina ny fandoavam-bola';
$_['text_transaction_failed']              = 'Tsy nahomby ny fandoavam-bola';
$_['text_transaction_cancelled']           = 'Nofoanana';
$_['text_transaction_suspended']           = 'Naato';
$_['text_transaction_suspended_failed']    = 'Nahemotra noho ny tsy fahombiazan&rsquo; ny fandoavana';
$_['text_transaction_outstanding_failed']  = 'Miandry ny fandoavana';
$_['text_transaction_expired']             = 'Fetra fampiasana';
$_['text_empty']                           = 'Tsy hita ny fandoavam-bola miverimberina';
$_['text_error']                           = 'Tsy hita ny fandoavana miverimberina nangatahina!';
$_['text_cancelled']                       = 'Nofoanana ny fandoa vola miverimberina';

// Column
$_['column_date_added']                    = 'Daty fananganana';
$_['column_type']                          = 'karazana';
$_['column_amount']                        = 'Vola';
$_['column_status']                        = 'Sata';
$_['column_product']                       = 'Vokatra';
$_['column_order_recurring_id']            = 'ID de profil';

// Error
$_['error_not_cancelled']                  = 'Fahadisoana: %s';
$_['error_not_found']                      = 'Aza adino: tsy misy vola miverimberina hofoanana';

// Button
$_['button_return']                        = 'Miverina';
