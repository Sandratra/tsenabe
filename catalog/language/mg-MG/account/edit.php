<?php
// Heading
$_['heading_title']      = 'Ny mombamomba ny kaontiko';

// Text
$_['text_account']       = 'Kaonty';
$_['text_edit']          = 'Hanova ny vaovao';
$_['text_your_details']  = 'Fampahalalana manokana';
$_['text_success']       = 'Fahombiazana: novana ny kaontinao marina.';

// Entry
$_['entry_firstname']    = 'anarana voalohany';
$_['entry_lastname']     = 'Anarana farany';
$_['entry_email']        = 'E-mail';
$_['entry_telephone']    = 'telefaonina';

// Error
$_['error_exists']       = 'Azafady: efa nampiasaina io mailaka io !';
$_['error_firstname']    = 'Ny anarana voalohany dia tsy maintsy misy eo anelanelan&rsquo; ny 1 sy 32 isa !';
$_['error_lastname']     = 'Ny anarana farany dia tokony eo anelanelan&rsquo ny 1 sy 32 litera!';
$_['error_email']        = 'Ny adiresy mailaka dia toa tsy misy mitombina !';
$_['error_telephone']    = 'Ny laharana an-tariby dia tsy maintsy eo anelanelan&rsquo ny 3 sy 32        							endri-tsoratra!';
$_['error_custom_field'] = '%s dia takiana !';
