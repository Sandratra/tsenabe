<?php
// Heading
$_['heading_title']                = 'Fidirana amin&rsquo; ny kaonty';

// Text
$_['text_account']                 = 'Kaonty';
$_['text_login']                   = 'Sokafy ny fivoriana iray';
$_['text_new_customer']            = 'Mpanjifa vaovao';
$_['text_register']                = 'Mamorona kaonty';
$_['text_register_account']        = 'Amin&rsquo; ny alàlan&rsquo; ny famoronana kaonty dia hahafahanao manao 									haingana kokoa ny fividiananao ianao, fantaro ny toe-piainan&rsquo; ny 									  baikonao ary araho ny zavatra novidinao teo aloha.';
$_['text_returning_customer']      = 'Efa mpanjifa';
$_['text_i_am_returning_customer'] = 'Efa mpanjifa tokoa aho';
$_['text_forgotten']               = 'Hadino ny tenimiafina';

// Entry
$_['entry_email']                  = 'E-mail';
$_['entry_password']               = 'teny miafina';

// Error
$_['error_login']                  = 'Azafady: tsy nisy lalao nifanesy hita teo amin&rsquo; ny anaran&rsquo; 									ny mpampiasa sy / na ity tenimiafina ity.';
$_['error_attempts']               = 'Aza adino ity: ny kaontinao dia mihoatra ny fahazoan-dàlana mifandraika 									amin&rsquo;ny fifandraisana. Manandrama azafady raha afaka 1 ora.';
$_['error_approved']               = 'Aza adino: ny kaontinao dia tokony hankatoavina alohan&rsquo;ny 											ahafahanao miditra..';
