<?php
// Heading
$_['heading_title']  = 'Ovay ny teny miafina';

// Text
$_['text_account']   = 'Kaonty';
$_['text_password']  = 'Ny teny miafinao';
$_['text_success']   = 'Fahombiazana: nohavaozina ny tenimiafinao.';

// Entry
$_['entry_password'] = 'Teny miafina';
$_['entry_confirm']  = 'Hamarino ny teny miafina ity';

// Error
$_['error_password'] = 'Ny tenimiafina dia tokony misy eo amin&rsquo;ny 4 ka hatramin&rsquo; ny 20 !';
$_['error_confirm']  = 'Tsy mifanaraka ny teny sy ny fanamafisana !';
