<?php
// Heading
$_['heading_title'] = 'Tafiditra soamatsara ny kaontinao !';

// Text
$_['text_message']  = '<p>Arahabaina,Tafiditra soamatsara ny kaontinao!</p><p>Raha manana fanontaniana momba ny fomba fiasan&rsquo; ny rafi-miasa ianao dia mifandraisa anao amin&rsquo; ny tompon&rsquo;ny magazay.

.</p><p>Nisy ny fanamafisana dia nalefa tany amin&rsquo; ny adiresy mailaka izay nomenao. Raha mbola tsy nahazo na inona na inona ianao amin&rsquo; ny ora manaraka, dia misaotra anao
 <a href="%s">mifandraisa</a>.</p>';
$_['text_approval'] = '<p>Misaotra anao nisoratra anarana
 %s!</p><p>Hampahafantarina anao amin&rsquo; ny mailaka ny nanakantohan&rsquo; ny kaontin&rsquo; ny tompon&rsquo; ilay tranombarotra ny kaontinao.</p><p>Raha manana fanontaniana momba ny fampandehanana ny fivarotana an-tserasera ianao, azafady

 <a href="%s">mifandraisa amin&rsquo; ny tompon&rsquo;ny magazay</a>.</p>';
$_['text_account']  = 'Kaonty';
$_['text_success']  = 'Fahombiazana';
