<?php
// Heading
$_['heading_title']  = 'Ovay ny teny miafina';

// Text
$_['text_account']   = 'Kaonty';
$_['text_password']  = 'Ny teny miafinao';
$_['text_success']   = 'Nohavaozina tsara ny <p>teny miafinao</p>.';

// Entry
$_['entry_password'] = 'Teny miafina';
$_['entry_confirm']  = 'hanamarina ny teny miafinao';

// Error
$_['error_password'] = 'Ny tenimiafina dia tokony misy eo amin&rsquo; ny 4 ka hatramin&rsquo; ny 20 !';
$_['error_confirm']  = 'Tsy mifanaraka ny teny sy ny fanamafisana !';
$_['error_code']     = 'Teny miafina vaovao tsy mety na nampiasaina teo aloha!';
