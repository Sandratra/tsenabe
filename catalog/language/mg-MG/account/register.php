<?php
// Heading
$_['heading_title'] 	   = 'Fisoratana anarana';

// Text
$_['text_account']         = 'Kaonty';
$_['text_register']        = 'fisoratana anarana';
$_['text_account_already'] = 'Raha efa manana kaonty ianao dia azafady <a href="%s">midira</a>.';
$_['text_your_details']    = 'Ny mombamomba anao manokana';
$_['text_newsletter']      = 'Taratasy momban&rsquo; ny vaovao';
$_['text_your_password']   = 'ny teny miafinao';
$_['text_agree']           = 'Namaky sy nanaiky ny <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Vondrona mpanjifa';
$_['entry_firstname']      = 'Anarana voalohany';
$_['entry_lastname']       = 'Anarana farany';
$_['entry_email']          = 'E-mail';
$_['entry_telephone']      = 'Telefaonina';
$_['entry_newsletter']     = 'Famandrihana';
$_['entry_password']       = 'Teny miafina';
$_['entry_confirm']        = 'Hamarino ny teny miafina';

// Error
$_['error_exists']         = 'Aza adino ity: efa misoratra anarana ity e-mail ity !';
$_['error_firstname']      = 'Ny anarana voalohany dia tsy maintsy misy eo anelanelan&rsquo; ny 1 sy 32 isa !';
$_['error_lastname']       = 'Ny anarana farany dia tokony eo anelanelan&rsquo; ny 1 sy 32 endri-tsoratra!';
$_['error_email']          = 'Ny adiresy mailaka dia toa tsy misy mitombina !';
$_['error_telephone']      = 'Ny laharana an-tariby dia tsy maintsy eo anelanelan&rsquo; ny 3 sy 32 								  endri-tsoratra !';
$_['error_custom_field']   = '%s dia takiana!';
$_['error_password']       = 'Ny tenimiafina dia tokony misy eo amin&rsquo;ny 4 ka hatramin&rsquo; ny 20 !';
$_['error_confirm']        = 'Tsy mifanaraka ny teny sy ny fanamafisana !';
$_['error_agree']          = 'Aza adino: tokony manaiky ianao%s';
