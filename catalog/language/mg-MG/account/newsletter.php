<?php
// Heading
$_['heading_title']    = 'Misoratra anarana amin&rsquo; ny foibe';

// Text
$_['text_account']     = 'Kaonty';
$_['text_newsletter']  = 'Taratasy & rsquo; vaovao';
$_['text_success']     = 'Fahombiazana: novaina ny famandrihana nataonao ao amin& rsquo;ny newsletter !';

// Entry
$_['entry_newsletter'] = 'famandrihana';
