<?php
// Heading
$_['heading_title']         = 'Mandahatra ny tantara';

// Text
$_['text_account']          = 'Kaonty';
$_['text_order']            = 'Manafatra fampahalalana';
$_['text_order_detail']     = 'Mandahatra antsipiriany';
$_['text_invoice_no']       = 'volavolan-dalàna n°: ';
$_['text_order_id']         = 'ID de la commande : ';
$_['text_date_added']       = 'Nampiana daty : ';
$_['text_shipping_address'] = 'Adiresy fandefasana';
$_['text_shipping_method']  = 'Fomba fandefasana : ';
$_['text_payment_address']  = 'Adiresy fandoavam-bola';
$_['text_payment_method']   = 'Fomba fandoavam-bola : ';
$_['text_comment']          = 'fanehoan-kevitra';
$_['text_history']          = 'Mandahatra ny tantara';
$_['text_success']          = 'Fahombiazana: nanampy anao <a href="%s"> %s</a> ho anao <a href="%s"> 									sarety</a> !';
$_['text_empty']            = 'Tsy nametraka baiko teo aloha ianao';
$_['text_error']            = 'Tsy hita ny baiko nangatahinao !';

// Column
$_['column_order_id']       = 'Commande no.';
$_['column_customer']       = 'Mpanjifa';
$_['column_product']        = 'Isan&rsquo; ny vokatra';
$_['column_name']           = 'Anaran&rsquo; ny vokatra';
$_['column_model']          = 'Famantarana';
$_['column_quantity']       = 'habetsahana';
$_['column_price']          = 'Vidiny';
$_['column_total']          = 'Fitambarany';
$_['column_action']         = 'Hetsika';
$_['column_date_added']     = 'Nampiana daty';
$_['column_status']         = 'Sokajin-kafatra';
$_['column_comment']        = 'Fanehoan-kevitra';

// Error
$_['error_reorder']         = '%s tsy azo ampiasaina izao mba hanoroana azy.';
