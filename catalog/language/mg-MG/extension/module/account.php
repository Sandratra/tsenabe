<?php
// Heading
$_['heading_title']    = 'Kaonty';

// Text
$_['text_register']    = 'Fisoratana anarana';
$_['text_login']       = 'Sokafy ny fivoriana iray';
$_['text_logout']      = 'Hivoaka';
$_['text_forgotten']   = 'Hadino ny teny miafina';
$_['text_account']     = 'Kaontiko';
$_['text_edit']        = 'Hanova ny kaontiko';
$_['text_password']    = 'Teny miafina';
$_['text_address']     = 'Lisitry ny adiresy';
$_['text_wishlist']    = 'Lisitry ny faniriana';
$_['text_order']       = 'Historique des commandes';
$_['text_download']    = 'Téléchargements';
$_['text_reward']      = 'Hevitra mahatoky';
$_['text_return']      = 'Fiverenana';
$_['text_transaction'] = 'Varotra';
$_['text_newsletter']  = 'Taratasim-baovao';
$_['text_recurring']   = 'Ny fandoavam-bola miverimberina';
