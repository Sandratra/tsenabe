<?php
// Text
$_['text_tax']      				= 'Ex hetra:';

//product
$_['text_tax']      				= 'Ex hetra:';
$_['text_quantity']					= 'Isa tavela:';
$_['tv_lang_new_label']				= 'Vaovao';
$_['tv_lang_sale_label']			= 'Fivarotana';
$_['tv_lang_login_label']      		= 'Fifandraisana na Fisoratana anarana';
$_['tv_lang_search_label']      	= 'Fikarohana Sokajy';
$_['tv_lang_search_text_label']     = 'Fikarohana';
$_['tv_lang_removecart_label']     	= 'Esory amin ny harona';
$_['tv_lang_subtottal_label'] 		= 'Fitambarany';
$_['tv_lang_shipping_label'] 		= 'Fanaterana';
$_['tv_lang_tax_label'] 			= 'Hetra';
$_['tv_lang_total_label'] 			= 'Fitambarany';
$_['tv_lang_checkout_label'] 		= 'Dingana fanamarinana';
$_['tv_lang_no_add_pro_label'] 		= 'Tsisy vokatra ao amin ny harona';
$_['tv_lang_account']      			= 'Kaontiko';

/* webvolty */
$_['tv_lang_store_label']			= 'Mitahiry fampahalalana';
$_['tv_lang_call_label']			= 'Antsoy izahay:';
$_['tv_lang_email_label']			= 'Mandefasa mailaka aminay:';
$_['text_wishlist']					= 'Lisitra faniriana';
$_['text_compare']					= 'Ampitahao';
$_['text_mycart']					= 'Ny harako';
$_['text_myaccount']				= 'Kaontiko';
$_['text_scrolltop']				= 'Horonana mankany ambony';
$_['text_productcompater']			= 'Fampitahana vokatra';
$_['text_storeinfo']    			= 'MOMBAMOMBAN NY FIVAROTANA';
$_['text_youraccount']    			= 'KAONTY';
$_['text_ourcompany']    			= 'NY ORINASANTSIKA';
$_['text_support']    			    = 'Fanampiana 24X7';
$_['text_seemap']   				= 'Jereo ny sari-tany';
$_['text_address']   				= 'Adiresy:';
$_['text_account_address']			= 'Adiresy';
$_['text_phone']   					= 'Telefaonina:';
$_['text_email']  					= 'Email:';
$_['text_product_empty']        	= 'Tsy misy vokatra mifanaraka amin ny fitsipiky ny fikarohana.';
$_['entry_product_search']      	= 'Fitsipi-pitadiavana';
$_['entry_product_description'] 	= 'Karohy ao amin ny famaritana vokatra';
$_['text_product_search']      		= 'Ny vokatra mifanaraka amin ny fitsipiky ny fikarohana';
$_['text_product_empty']        	= 'Tsy misy vokatra mifanaraka amin ny fitsipiky ny fikarohana.';
$_['text_storetimemonfri']    		= 'Ala - Zom:';
$_['text_storetimesat']   	     	= 'Sab:';
$_['text_storetimesun']     	   	= 'Alh:';
$_['text_gotop']     	   			= 'Miakatra';
$_['text_quickview']  	   			= 'Famintinana haingana';
$_['text_viewleftcolumn']  	   		= 'Asehoy ny tsanganana ankavia';
$_['text_viewrightcolumn']  	   	= 'Asehoy ny tsanganana ankavanana';
$_['text_grid']  	   				= 'Fefy';
$_['text_grid2']  	   				= 'Fefy 2';
$_['text_list']  	   				= 'Lisitra';
$_['text_list2']  	   				= 'Lisitra-2';
$_['text_catalog']  	   			= 'katalaogy';
$_['text_contact_us']  	   			= 'Adiresy';
$_['text_information']  	   			= 'Fampahalalam-baovao';
$_['text_cust_ser']  	   			= 'MILA VONJY ?';
$_['text_tracking']  	   			= 'Azafady ampidiro eto ny nomerao fanaraha-maso mailaka eto ambany:';

$_['text_user_title']           	= ' Mpampiasa demo';
$_['text_readmore']  	   			= 'Hamaky bebe kokoa';
$_['text_comment']  	   			= 'Fanehoan-kevitra';
$_['text_by']		  	   			= 'Ny';
$_['text_allblogs']	  	   			= 'Ny bilaogy rehetra';
$_['text_blogspagetitle']	  	   	= '	Lohateny ny bilaogy';
$_['text_blogcountinue']	  	   	= 'Hanohy';
$_['text_blogpostby']	  	   		= 'Navoakan i';
$_['text_pagination']       		= 'Mampiseho %d ka %d amin ny %d (%d pejy)';
$_['text_allcomments']       		= 'Ny fanehoan-kevitra rehetra';

$_['text_comment_name']       		= '*Ny anaranao';
$_['text_comment_email']       		= '*Ny mailakao';
$_['text_comment_url']       		= 'URL amin ny tranokala (safidy)';
$_['text_comment_subject']       	= 'Lohahevitra (safidy)';
$_['text_comment_comment']       	= '*Fanehoan-kevitra';
$_['button_submit']       			= 'Kitiho ny bokotra';

$_['error_name']       				= 'Ny anaranao dia tokony ho eo amin ny litera 3 ka hatramin ny 32!';
$_['error_email']       			= 'Ny adiresy mailaka dia toa tsy manan-kery!';
$_['error_enquiry']       			= 'Tokony ho eo anelanelan ny 10 sy 3000 ireo fanehoan-kevitra!';
$_['button_submit']       			= 'Kitiho ny bokotra';
$_['text_success']       			= 'Manampy fanamarihana mahomby';

$_['text_newsletterbtn']      		= 'Famandrihana!';
$_['text_newpopupshow']      		= 'Aza aseho intsony ity fampidirana ity';

$_['text_enter_email']      		= '<div class="ttv-newssubmit-error">
										<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
										<span class="ttv-newssubmit-error-text">Ampidiro azafady ilay mailaka

. </span>
										<span id="closenewsletter">x</span>
										</div>';

$_['text_repeat_email']      		= '<div class="ttv-newssubmit-error">
										<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
										<span class="ttv-newssubmit-error-text">ity adiresy mailaka ity dia efa nisoratra anarana.</span>
										<span id="closenewsletter">x</span>
										</div>';

$_['text_success_email']      		= '<div class="ttv-newssubmit-success">
											<i class="fa fa-check-circle"></i>
											<span class="ttv-newssubmit-success-text">Nahasoratra anarana tamim-pahombiazana anao ity takelaka ity. </span>
											<span id="closenewsletter">x</span>
										</div>';

$_['text_error_email']      		= '<div class="ttv-newssubmit-error">
										<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
										<span class="ttv-newssubmit-error-text">Adiresy mailaka tsy marina.</span>
										<span id="closenewsletter">x</span>
										</div>';
$_['text_place_enter']          	= 'Ampidiro ny mailakao...';
$_['text_leave']          			= 'Mametraha valiny';

$_['text_themeoption']      		= 'Safidy lohahevitra';

$_['text_theme'] 					= 'Lohahevitra';
$_['text_custom'] 					= 'Custom';
$_['text_custome_color_1'] 			= 'Loko Custom 1';
$_['text_box_layout'] 				= 'Fametahana boaty';
$_['text_background_pattern'] 		= 'Modely amin ny sehatra';

$_['text_bgadmin'] 					= 'Ny traikefa ho an ny mpanjifa dia misy amin ny admin.';
$_['text_bgcolor'] 					= 'Loko eo ambadika';
$_['text_menusticky'] 				= 'Menio miaraikitra';
$_['text_reset'] 					= 'Mamerina';
$_['text_menu'] 					= 'Menio';
$_['text_call_us'] 					= 'telefaonina:';
$_['text_email_us']					= 'Email:';
$_['text_home'] 					= 'Trano';
$_['text_close']					= 'Hidio';
$_['text_moreresult']				= 'Valiny bebe kokoa';
$_['text_noproduct']				= 'Tsy hita ny vokatra';
$_['text_buttonSubscribe']			= 'Famandrihana';
$_['text_buttonok']					= 'Eny';
$_['text_newslettercall']					= 'HANOKO fanampiana';
$_['text_todayoffer']					= 'Tolotra anio';
$_['text_fax']					= 'Hetra:';
$_['text_opentime']					= 'Fotoana misokatra:';

