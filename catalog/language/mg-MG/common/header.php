<?php
// Text
$_['text_home']          = 'Tonga soa';
$_['text_wishlist']      = 'Lisitra ny faniriana (%s)';
$_['text_shopping_cart'] = 'Sarety';
$_['text_category']      = 'Sokajy';
$_['text_account']       = 'Kaontiko';
$_['text_register']      = 'Fisoratana anarana';
$_['text_login']         = 'Sokafy ny fivoriana iray';
$_['text_order']         = 'Tantara momba ny commande';
$_['text_transaction']   = 'Varotra';
$_['text_download']      = 'Téléchargements';
$_['text_logout']        = 'Fialana';
$_['text_checkout']      = 'Fanamafisana ny commande';
$_['text_search']        = 'Karohy';
$_['text_all']           = 'Jereo daholo';
