<?php
// Text
$_['text_information']  = 'Vaovao';
$_['text_service']      = 'Serivisy mpanjifa';
$_['text_extra']        = 'Fanampiny';
$_['text_contact']      = 'Fifandraisana';
$_['text_return']       = 'Miverina';
$_['text_sitemap']      = 'Drafitra ny site';
$_['text_manufacturer'] = 'Mpanamboatra';
$_['text_voucher']      = 'Taratasim-bola fanomezana';
$_['text_affiliate']    = 'Mpiray';
$_['text_special']      = 'Fisondrotana';
$_['text_account']      = 'Kaontiko';
$_['text_order']        = 'tantara ny commande';
$_['text_wishlist']     = 'Lisitra ny faniriana';
$_['text_newsletter']   = 'Taratasim-baovao';
