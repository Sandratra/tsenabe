<?php
// Text
$_['text_upload']    = 'Navoaka soa aman-tsara ny rakitrao !';

// Error
$_['error_filename'] = 'Ny anaran&rsquo; ny rakitra dia tsy maintsy eo anelanelan&rsquo; ny 3 sy 128 !';
$_['error_filetype'] = 'Karazana rakitra tsy mety !';
$_['error_upload']   = 'Téléchargement nécessaire !';
