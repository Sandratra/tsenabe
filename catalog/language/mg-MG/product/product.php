<?php
// Text
$_['text_search']              = 'Karohy';
$_['text_brand']               = 'Marika';
$_['text_manufacturer']        = 'Mpanamboatra : ';
$_['text_model']               = 'Famantarana : ';
$_['text_reward']              = 'Hevitra mahatoky : ';
$_['text_points']              = 'Vidiny amin ny laharam-pahatokisana : ';
$_['text_stock']               = 'Fisiny : ';
$_['text_instock']             = 'Disponible';
$_['text_tax']                 = 'Vidiny tsy misy hetra : ';
$_['text_discount']            = ' na mihoatra ';
$_['text_option']              = 'Safidy azo ampiasaina';
$_['text_minimum']             = 'Ity vokatra ity dia manana habetsany kely kokoa amin ny %s';
$_['text_reviews']             = '%s fanombanana';
$_['text_write']               = 'Omeo ny hevitrao';
$_['text_login']               = 'Miasaotra anareo<a href="%s"> Niditra</a> na <a href="%s">Nisoratra anarana</a> manombatombana';
$_['text_no_reviews']          = 'Tsy misy hevitra momba an io vokatra io.';
$_['text_note']                = '<span class="text-danger"> Mitandrema :</span> le HTML n&rsquo;est pas transcrit !';
$_['text_success']             = 'Misaotra tamin ny famerenanao. Efa naterina tamin ny tranokala webmaster izany hahazoana fankatoavana

.';
$_['text_related']             = 'Vokatra mifandraika';
$_['text_tags']                = 'Teny mifandraika : ';
$_['text_error']               = 'Vokatra tsy hita !';
$_['text_payment_recurring']   = 'Ny mombamomba ny fandoavana';
$_['text_trial_description']   = '%s isaky %s(s) %d ny %d handoa vola avy ep';
$_['text_payment_description'] = '%s isaky %s(s) %d ny %d handoa vola';
$_['text_payment_cancel']      = '%s isaky %s(s) %d hatramin&rsquo; ny fanajanonana';
$_['text_day']                 = 'Andro';
$_['text_week']                = 'herinandro';
$_['text_semi_month']          = '15 andro';
$_['text_month']               = 'Volana';
$_['text_year']                = 'Taona';

// Entry
$_['entry_qty']                = 'habetsany';
$_['entry_name']               = 'Ny anaranao';
$_['entry_review']             = 'Ny fanombanana anao';
$_['entry_rating']             = 'Naoty';
$_['entry_good']               = 'Tsara';
$_['entry_bad']                = 'Ratsy';

// Tabs
$_['tab_description']          = 'Famaritana';
$_['tab_astuce']          	   = 'Torohevitra';
$_['tab_attribute']            = 'Fihavahany';
$_['tab_review']               = 'Evaluations (%s)';

// Error
$_['error_name']               = 'Aza hadino fa: ny anaran&rsquo; ny fanombanana dia tokony ho eo amin&rsquo; 								ny litera 3 sy 25 !';
$_['error_text']               = 'Tsiahivina: ny lahatsoratra eo amin&rsquo; ny fanombanana dia tsy maintsy 							 eo anelanelan&rsquo; ny 25 sy 1000 ny tarehimarika !';
$_['error_rating']             = 'Zava-dehibe: azafady, fidio naoty !';
