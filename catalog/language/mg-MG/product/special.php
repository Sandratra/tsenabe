<?php
// Heading
$_['heading_title']     = 'Fisondrotana';

// Text
$_['text_empty']        = 'Tsy misy ny fampiroboroboana mifanentana.';
$_['text_quantity']     = 'Habetsahany : ';
$_['text_manufacturer'] = 'Mpanamboatra : ';
$_['text_model']        = 'Famantarana : ';
$_['text_points']       = 'Hevitra mahatoky : ';
$_['text_price']        = 'Vidiny : ';
$_['text_tax']          = 'Vidiny tsy misy hetra : ';
$_['text_compare']      = 'Ny vokatra ampitahaina ';
$_['text_sort']         = 'Sivanin i : ';
$_['text_default']      = 'Par défaut';
$_['text_name_asc']     = 'Anarana (A - Z)';
$_['text_name_desc']    = 'Anarana (Z - A)';
$_['text_price_asc']    = 'Manomboka amin ny mora indrindra ka hatramin ny lafo indrindra';
$_['text_price_desc']   = 'Manomboka amin ny lafo indrindra ka hatramin ny mora indrindra';
$_['text_rating_asc']   = 'Naoty (Ambany indrinra)';
$_['text_rating_desc']  = 'Naoty (Ambony indrindra)';
$_['text_model_asc']    = 'Famantarana (A - Z)';
$_['text_model_desc']   = 'Famantarana (Z - A)';
$_['text_limit']        = 'Jerena : ';
