<?php
// Text
$_['text_refine']       = 'Mamerimberina fikarohana';
$_['text_product']      = 'Vokatra';
$_['text_error']        = 'Tsy hita ny sokajy !';
$_['text_empty']        = 'Tsy misy vokatra azo amin ity sokajy ity.';
$_['text_quantity']     = 'Habetsahana : ';
$_['text_manufacturer'] = 'Mpanamboatra : ';
$_['text_model']        = 'Famantarana : ';
$_['text_points']       = 'Hevitra mahatoky : ';
$_['text_price']        = 'Vidiny : ';
$_['text_tax']          = 'Vidiny tsy misy hetra: ';
$_['text_compare']      = 'Ny vokatra ampitahaina (%s)';
$_['text_sort']         = 'Sivanin i : ';
$_['text_default']      = 'Par défaut';
$_['text_name_asc']     = 'Anarana (A - Z)';
$_['text_name_desc']    = 'Anarana (Z - A)';
$_['text_price_asc']    = 'Manomboka amin ny kely indrindra ka hatramin ny lafo indrindra';
$_['text_price_desc']   = 'Manomboka amin ny lafo indrindra ka hatramin ny mora indrindra';
$_['text_rating_asc']   = 'Noty (Ambany indrindra)';
$_['text_rating_desc']  = 'Noty (Ambony indrindra)';
$_['text_model_asc']    = 'Famantarana (A - Z)';
$_['text_model_desc']   = 'Famantarana (Z - A)';
$_['text_limit']        = 'Jerena : ';
