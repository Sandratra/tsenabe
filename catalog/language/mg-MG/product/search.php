<?php
// Heading
$_['heading_title']     = 'Karohy';
$_['heading_tag']       = 'Tag - ';

// Text
$_['text_search']       = 'Ny vokatra mifanaraka amin ny fitsipiky ny fikarohana';
$_['text_keyword']      = 'Teny mifanaraka';
$_['text_category']     = 'Sokajy rehetra';
$_['text_sub_category'] = 'Hikaroka ao amin ny zana-tsokajy';
$_['text_empty']        = 'Tsy misy vokatra mifanaraka amin ny fitsipiky ny fikarohana.';
$_['text_quantity']     = 'Habetsahany : ';
$_['text_manufacturer'] = 'Mpanamboatra : ';
$_['text_model']        = 'Famantarana : ';
$_['text_points']       = 'Hevitra mahatoky : ';
$_['text_price']        = 'Vidiny : ';
$_['text_tax']          = 'Vidiny tsy misy hetra : ';
$_['text_reviews']      = 'Miorina amin ny salan ny.';
$_['text_compare']      = 'Ny vokatra ampitahaina ';
$_['text_sort']         = 'Sivanin i : ';
$_['text_default']      = 'Par défaut';
$_['text_name_asc']     = 'Anarana (A - Z)';
$_['text_name_desc']    = 'Anarana (Z - A)';
$_['text_price_asc']    = 'Manomboka amin ny mora indrindra ka hatramin ny lafo indrindra';
$_['text_price_desc']   = 'Manomboka amin ny lafo indrindra ka hatramin ny mora indrindra';
$_['text_rating_asc']   = 'Naoty (Ambany indrindra)';
$_['text_rating_desc']  = 'Naoty (Ambony indrindra)';
$_['text_model_asc']    = 'Famantarana (A - Z)';
$_['text_model_desc']   = 'Famantarana (Z - A)';
$_['text_limit']        = 'Jerena : ';

// Entry
$_['entry_search']      = 'Fitsipi-pitadiavana';
$_['entry_description'] = 'Karohy ao amin ny famaritana vokatra';
