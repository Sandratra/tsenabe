<?php
// Heading
$_['heading_title']     = 'Fampitahana vokatra';

// Text
$_['text_product']      = 'Mombamomba ny vokatra';
$_['text_name']         = 'Vokatra';
$_['text_image']        = 'Sary';
$_['text_price']        = 'Vita';
$_['text_model']        = 'Famantarana';
$_['text_manufacturer'] = 'Mpanamboatra';
$_['text_availability'] = 'Fisiany';
$_['text_instock']      = 'Disponible';
$_['text_rating']       = 'Noty';
$_['text_reviews']      = 'Basé sur %s évaluation(s).';
$_['text_summary']      = 'Famintinana';
$_['text_weight']       = 'Lanjany';
$_['text_dimension']    = 'Habe (L × l × H)';
$_['text_compare']      = 'Ny vokatra ampitahaina (%s)';
$_['text_success']      = 'Fahombiazana: nanampy anao <a href="%s"> %s</a> ho anao <a href="%s"> ny 								fampitahana ny vokatra</a> !';
$_['text_remove']       = 'Fahombiazana: nanova ny fampitahanao vokatrao ianao !';
$_['text_empty']        = 'Tsy nifidy vokatra ianao raha hampitaha.';
