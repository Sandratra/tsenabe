<?php
// Heading
$_['heading_title']  = 'Mifandraisa aminay';

// Text
$_['text_location']  = 'Adiresy fivarotana';
$_['text_store']     = 'Fivarotana';
$_['text_contact']   = 'Teny fifandraisana';
$_['text_address']   = 'Adiresy';
$_['text_telephone'] = 'Telefaonina';
$_['text_fax']       = 'Télécopie';
$_['text_open']      = 'Ora fisokafana';
$_['text_comment']   = 'fanehoan-kevitra';
$_['text_success']   = '<p>Tanteraka soamatsara ny fangatahanao fampahalalana.</p>';

// Entry
$_['entry_name']     = 'Anarana';
$_['entry_email']    = 'E-mail';
$_['entry_enquiry']  = 'Fangatahana fampahalalana';

// Email
$_['email_subject']  = 'Fangatahana fampahalalana: %s';

// Errors
$_['error_name']     = 'Aza hadino fa: ny anarana dia tokony ho eo amin ny litera 3 sy 32 !';
$_['error_email']    = 'Azafady: ny adiresy mailaka dia tsy hita fa tsy mitombina!';
$_['error_enquiry']  = 'Aza adino: ny fangatahana fampahalalana dia tsy maintsy misy eo amin ny litera 10 sy 3000!';
