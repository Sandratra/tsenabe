<?php
// Heading
$_['heading_title']                 = 'Fandraisana anjara fandaharana';

// Text
$_['text_account']                  = 'Kaonty';
$_['text_login']                    = 'Sokafy ny fivoriana iray';
$_['text_description']              = '<p>%s maimaimpoana ny programam-pifandraisana ary mamela ireo mpikambana hamokatra fidiram-bola amin ny alàlan ny fametrahana rohy iray na maromaro amin ny tranonkalany mba hampandrosoana ny% s na ny vokatra. Ny varotra rehetra nataon ireo mpanjifa izay nanindry ireo rohy ireo dia miteraka vaomiera. Ny tahan ny komisiona mahazatra amin izao fotoana izao
 %s.</p><p>Raha mila fanazavana fanampiny, tsidiho ny FAQ eto na manadihady ny fepetra maha-mpikambana.</p>';
$_['text_new_affiliate']            = 'Fifandraisana vaovao';
$_['text_register_account']         = '<p>Mbola tsy mifandray aho .</p><p>Tsindrio hanohy eto ambany mba hamoronana kaonty fampifandraisana. Aza hadino fa tsy misy ifandraisany amin ny kaontin ny mpanjifanao ny kaonty iraisana.</p>';
$_['text_returning_affiliate']      = 'Fifandraisana mifandray';
$_['text_i_am_returning_affiliate'] = 'Efa mifandray aho';
$_['text_forgotten']                = 'Hadino ny teny miafina';

// Entry
$_['entry_email']                   = 'Mailaka ampifandraisina';
$_['entry_password']                = 'Teny miafina';

// Error
$_['error_login']                   = 'Attention: aucune correspondance n&rsquo;a été trouvée entre ce nom d&rsquo;utilisateur et/ou ce mot de passe.';
$_['error_attempts']                = 'Attention: votre compte a dépassé le nombre autorisé de tentatives de connexion. Merci d&rsquo;essayer à nouveau dans 1 heure.';
$_['error_approved']                = 'Attention: votre compte nécessite d&rsquo;avoir été approuvé avant que vous puissiez vous connecter.';
