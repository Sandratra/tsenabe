<?php
// Heading
$_['heading_title'] = 'Tsy nahomby ny fandoavam-bola !';

// Text
$_['text_basket']   = 'Sarety';
$_['text_checkout'] = 'Hamarino ny fakana entana';
$_['text_failure']  = 'Tsy nahomby ny fandoavam-bola';
$_['text_message']  = '<p>Nisy olana  nandritra ny fanodinana ny fandoavanao ary tsy notanterahina ny fakana entana.</p>

<p>Ny antony mety:</p>
<ul>
  <li>Vola tsy ampy</li>
  <li>Tsy nahomby ny fanamarinana</li>
</ul>

<p>Azafady manandrana misafidy indray hampiasa fomba fandoavam-bola hafa.</p>

<p>Raha mbola mitohy ny olana, azafady <a href="%s"> mifandraisa aminay </a> miaraka amin ny antsipiriany ny baiko tadiavinao halefa.</p>';
