<?php
// Heading
$_['heading_title']        = 'Efa voaray an-tanana soamatsara ireo entana nosafidianao !';

// Text
$_['text_basket']          = 'Sarety';
$_['text_checkout']        = 'Hamarino ny fakana entana';
$_['text_success']         = 'Mandeha fahombiazana';
$_['text_customer']        = '<p> Voarindra soa aman-tsara ny baikonao! </p> <p> Afaka mandinika ny tantaran ny baiko ianao amin ny fandehanana any amin ny <a href="%s"> kaontinao </a> ary tsindrio ny <a href="%s"> tantara </a>. </p> <p> Raha ny fividiananao dia misy rindranasa mifandraika, dia afaka mandeha any amin ny <a href="%s"> download < / a> raha hijery azy ireo. ny fividiananao ao amin ny tranokalanay! </p> '

;
$_['text_guest']           = '<p>Votre commande a été traitée avec succès!</p><p>Veuillez adresser vos éventuelles questions au <a href="%s">propriétaire de la boutique</a>.</p><p>Merci d&rsquo;avoir effectué vos achats sur notre site!</p>';
