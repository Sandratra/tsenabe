<?php
// Heading
$_['heading_title']                  = 'Fankatoavana ireo entana vidina';

// Text
$_['text_cart']                      = 'Sarety';
$_['text_checkout_option']           = 'Dingana %s: Mombamomba ireo entana vidina';
$_['text_checkout_account']          = 'Dingana %s: Kaonty sy antsipirian ny faktiora';
$_['text_checkout_payment_address']  = 'Dingana %s: Antsipiriany momba ny fandoavam-bola';
$_['text_checkout_shipping_address'] = 'Dingana %s: Antsipiriany momba ny fandefasana entana';
$_['text_checkout_shipping_method']  = 'Dingana %s: Fomba fanaterana entana';
$_['text_checkout_payment_method']   = 'Dingana %s: Fomba fandoavam-bola';
$_['text_checkout_confirm']          = 'Dingana %s: Fanamafafisana ny fividianana';
$_['text_modify']                    = 'Hanova';
$_['text_new_customer']              = 'Mpanjifa vaovao';
$_['text_returning_customer']        = 'Efa mpanjifa';
$_['text_checkout']                  = 'Safidy Fankatoavana : ';
$_['text_i_am_returning_customer']   = 'Izaho dia efa mpanjifa';
$_['text_register']                  = 'Fisoratana anarana';
$_['text_guest']                     = 'fividianana raha tsy misy fisoratana anarana';
$_['text_register_account']          = 'Amin ny alàlan ny famoronana kaonty azonao atao haingana kokoa ny fividiananao, dia mahalalaha ny toeran ny baikonao ary araho hatrany ny fividiananao teo aloha.';
$_['text_forgotten']                 = 'Hadino ny teny miafina';
$_['text_your_details']              = 'Ny mombamomba anao manokana';
$_['text_your_address']              = 'Ny adiresinao';
$_['text_your_password']             = 'Ny teny miafinao';
$_['text_agree']                     = 'Namaky sy nanaiky ny <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Mampiasà adiresy vaovao';
$_['text_address_existing']          = 'Mampiasà adiresy misy';
$_['text_shipping_method']           = 'Azafady fidio ny fomba fandefasana an io fanafarana io.';
$_['text_payment_method']            = 'Azafady fidio ny fomba fandoavam-bola ho an ity fanafarana ity.';
$_['text_comments']                  = 'Manampia fanehoan-kevitra momba ny fanafarananao';
$_['text_recurring_item']            = 'Entana miverimberina';
$_['text_payment_recurring']         = 'Mombamomba ny fandoavam-bola';
$_['text_trial_description']         = '%s isaky ny %d %s  ho an ny% d fandoa  na';
$_['text_payment_description']       = '%s isaky ny %d %s  ho an ny% d fandoavam-bola';
$_['text_payment_cancel']            = '%s isaky ny %d %s  mandra-pahatongany';
$_['text_day']                       = 'Andro';
$_['text_week']                      = 'herinandro';
$_['text_semi_month']                = '15 Andro';
$_['text_month']                     = 'Volana';
$_['text_year']                      = 'Taona';

// Column
$_['column_name']                    = 'Anaran ny vokatra';
$_['column_model']                   = 'Famatarana';
$_['column_quantity']                = 'Habetsahany';
$_['column_price']                   = 'Vidiny an-tokantrano';
$_['column_total']                   = 'Fitambarany';

// Entry
$_['entry_email_address']            = 'E-mail';
$_['entry_email']                    = 'E-mail';
$_['entry_password']                 = 'Teny miafina';
$_['entry_confirm']                  = 'Hamarino ny teny miafina';
$_['entry_firstname']                = 'Anarana voalohany';
$_['entry_lastname']                 = 'Anarana farany';
$_['entry_telephone']                = 'telefaonina';
$_['entry_address']                  = 'Adiresy';
$_['entry_company']                  = 'orinasa';
$_['entry_customer_group']           = 'Vondrona mpanjifa';
$_['entry_address_1']                = 'Adiresy 1';
$_['entry_address_2']                = 'Adiresy 2';
$_['entry_postcode']                 = 'Kaody paositra';
$_['entry_city']                     = 'tanàna';
$_['entry_country']                  = 'Firenena';
$_['entry_zone']                     = 'sampan-draharaha';
$_['entry_newsletter']               = 'Te hisoratra anarana amin ny sy %s aho.';
$_['entry_shipping']                 = 'Mitovitovy ny adiresy ny fandefasana sy ny fandoavam-bola nataoko.';

// Error
$_['error_warning']                  = 'Nisy lesoka nitranga teo am-pamokarana ny baikonao! Raha mitohy ny olana dia andramo atao hisafidy fomba fandoavam-bola hafa na hifandraisa amin ny tompon ilay tranombarotra
 <a href="%s"> Trindrio eto</a>.';
$_['error_login']                    = 'Azafady : tsy nisy fifandraisana hita teo amin ny anaran ny mpampiasa sy / na ity teny miafina ity..';
$_['error_attempts']                 = 'Aza adino ity: ny kaontinao dia mihoatra ny fahazoan-dàlana mifandraika amin ny fifandraisana. Manandrama azafady raha afaka 1 ora. ';
$_['error_approved']                 = 'Aza adino: ny kaontinao dia tokony efa nankatoavina mialoha alohan ny hahafahanao miditra.';
$_['error_exists']                   = 'Aza adino ity: efa misoratra anarana ity e-mail ity!';
$_['error_firstname']                = 'Ny anarana voalohany dia tsy maintsy misy eo anelanelan ny 1 sy 32 isa!';
$_['error_lastname']                 = 'Ny anarana farany dia tokony eo anelanelan ny 1 sy 32 litera !';
$_['error_email']                    = 'Ny adiresy mailaka dia toa tsy  mitombina !';
$_['error_telephone']                = 'Ny laharana an-tariby dia tsy maintsy eo anelanelan ny 3 sy 32 endri-tsoratra !';
$_['error_password']                 = 'Ny teny miafina dia tokony misy eo amin ny 4 ka hatramin ny 20 !';
$_['error_confirm']                  = 'Tsy mifanaraka ny teny sy ny fanamafisana !';
$_['error_address_1']                = 'Ny adiresy dia tsy maintsy eo anelanelan ny 3 sy 128 tarehimarika !';
$_['error_city']                     = 'Ny anaran ny tanàna dia tokony ho eo anelanelan ny 2 sy 128 endri-tsoratra !';
$_['error_postcode']                 = 'Azafady: ny kaody paositra dia tsy maintsy eo anelanelan ny 2 sy 10 ny endri-tsoratra !';
$_['error_country']                  = 'Mifidiana firenena iray !';
$_['error_zone']                     = 'Mifidiana departemanta iray azafady !';
$_['error_agree']                    = 'Aza adino: tokony manaiky ianao%s';
$_['error_address']                  = 'Aza adino: tokony misafidy ny adiresy ianao !';
$_['error_shipping']                 = 'Aza adino: ilaina ny fomba fandefasana !';
$_['error_no_shipping']              = 'Aza hadino fa tsy misy safidy fandefasana entana. Azafady, azafady ny <a href="%s"> fanohanan ny contact </a>!';
$_['error_payment']                  = 'Aza adino: ilaina ny fomba fandoavam-bola !';
$_['error_no_payment']               = 'Azafady: tsy misy safidy fandoavam-bola. Azafady ny <a href="%s"> fanohanan ny contact </a> !';
$_['error_custom_field']             = '%s dia takiana !';
