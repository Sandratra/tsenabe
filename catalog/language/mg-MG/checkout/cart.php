<?php
// Heading
$_['heading_title']            = 'Sarety';

// Text
$_['text_success']             = 'Fahombiazana: Nananmpy <a href="%s"> %s</a> tao <a href="%s"> anaty harona ihanao</a> !';
$_['text_remove']              = 'Fahombiazana: nanamboatra ny tapakolom-barotrao tsara ianao !';
$_['text_login']               = '	Aza adino: mila <a href="%s">miditra</a> ou <a href="%s">manokatra kaonty</a> raha toa ka hijery ny vidiny!';
$_['text_items']               = '%s lahatsoratra - %s';
$_['text_points']              = 'Hevitra mahatoky: %s';
$_['text_next']                = 'Inona tianao hatao manaraka ?';
$_['text_next_choice']         = 'Safidio na te hampiasa fihenam-bidy amin ny fihenam-bidy ianao na voucher mba hanombanana ny vidin ny fandefasana.';
$_['text_empty']               = 'Foana ny entana entinao !';
$_['text_day']                 = 'Andro';
$_['text_week']                = 'Herinandro';
$_['text_semi_month']          = '15 andro';
$_['text_month']               = 'Volana';
$_['text_year']                = 'Taona';
$_['text_trial']               = '% s samy% s% s ho an'ny saran'ny% s ';
$_['text_recurring']           = '%s isaky %s %s';
$_['text_payment_cancel']      = 'Mandra-panafoanana';
$_['text_recurring_item']      = 'Entana miverimberina';
$_['text_payment_recurring']   = 'Mombamomba ny fandoavam-bola';
$_['text_trial_description']   = '% s isaky ny% d% s (s) ho an ny% d fandoa (s) na';
$_['text_payment_description'] = '% s isaky ny% d% s (s) ho an ny% d fandoavam-bola';
$_['text_payment_cancel']      = '% s isaky ny% d% s (s) mandra-pahatongany';

// Column
$_['column_image']             = 'Sary';
$_['column_name']              = 'Anaran ny vokatra';
$_['column_model']             = 'Famatarana';
$_['column_quantity']          = 'Habetsahany';
$_['column_price']             = 'Vidiny an-tokantrano';
$_['column_total']             = 'Fitambarany';

// Error
$_['error_stock']              = 'Ny vokatra voatondro miaraka amin ny *** dia tsy hita ao amin ny habe maniry na tsy amin ny tahiry amin izao fotoana izao !';
$_['error_minimum']            = 'Ny sanda farany ambany indrindra ho an ny% s dia% s !';
$_['error_required']           = '%s dia takiana !';
$_['error_product']            = 'Aza adino: tsy misy vokatra ao anaty harona !';
$_['error_recurring_required'] = 'Merci de sélectionner un paiement récurrent !';
